-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 16, 2020 at 01:27 PM
-- Server version: 10.2.31-MariaDB-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `giamgiah_data`
--

-- --------------------------------------------------------

--
-- Table structure for table `ad_spaces`
--

CREATE TABLE `ad_spaces` (
  `id` int(11) NOT NULL,
  `ad_space` text DEFAULT NULL,
  `ad_code_728` text DEFAULT NULL,
  `ad_code_468` text DEFAULT NULL,
  `ad_code_300` text DEFAULT NULL,
  `ad_code_234` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ad_spaces`
--

INSERT INTO `ad_spaces` (`id`, `ad_space`, `ad_code_728`, `ad_code_468`, `ad_code_300`, `ad_code_234`) VALUES
(1, 'index_top', '', '', NULL, ''),
(2, 'index_bottom', NULL, NULL, NULL, NULL),
(3, 'post_top', NULL, NULL, NULL, NULL),
(4, 'post_bottom', NULL, NULL, NULL, NULL),
(5, 'category_top', NULL, NULL, NULL, NULL),
(6, 'category_bottom', NULL, NULL, NULL, NULL),
(7, 'tag_top', NULL, NULL, NULL, NULL),
(8, 'tag_bottom', NULL, NULL, NULL, NULL),
(9, 'search_top', NULL, NULL, NULL, NULL),
(10, 'search_bottom', NULL, NULL, NULL, NULL),
(11, 'profile_top', NULL, NULL, NULL, NULL),
(12, 'profile_bottom', NULL, NULL, NULL, NULL),
(13, 'reading_list_top', NULL, NULL, NULL, NULL),
(14, 'reading_list_bottom', NULL, NULL, NULL, NULL),
(15, 'sidebar_top', NULL, NULL, NULL, NULL),
(16, 'sidebar_bottom', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `lang_id` tinyint(4) DEFAULT 1,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `keywords` varchar(500) DEFAULT NULL,
  `parent_id` int(11) DEFAULT 0,
  `category_order` smallint(6) DEFAULT NULL,
  `show_on_menu` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `lang_id`, `name`, `slug`, `description`, `keywords`, `parent_id`, `category_order`, `show_on_menu`, `created_at`) VALUES
(13, 2, 'Khuyến mại khác', 'khuyen-mai-khac', 'Các chương trình khuyến mại khác của Giảm giá hằng ngày', 'mã giảm giá shopee, mã giảm giá tiki, mã giảm giá sendo, coupon giảm giá 2019, coupon giảm giá 2020', 0, 255, 1, '2019-12-16 17:05:04'),
(14, 2, 'Mỹ phẩm Thái Lan - Beauty Buffet', 'beauty-buffet-my-pham-so-1-thai-lan', 'BEAUTY BUFFET là hình thức kết hợp đặc biệt giữa “tất cả các mỹ phẩm làm đẹp” và các cửa hàng bán lẻ để tạo nên sự độc đáo ấn tượng. Hãy truy cập blog Giảm giá hằng ngày để săn khuyến mại khủng từ Beauty Buffet', 'mã khuyến mại mỹ phẩm, mã coupon beauty buffet, coupon beauty buffet 2019, beauty buffet tết 2020', 13, NULL, 0, '2019-12-16 17:12:12'),
(15, 2, 'Sản phẩm khuyến mại', 'san-pham-khuyen-mai', 'Tất cả sản phẩm khuyến mại được tổng hợp bởi Giamgiahangngay.com', 'Tất cả sản phẩm khuyến mại được tổng hợp bởi Giamgiahangngay.com', 0, 250, 0, '2019-12-16 17:29:32'),
(16, 2, 'EC - Vua bia', 'vua-bia', 'Giamgiahangngay.com - Vua Bia là trang thương mại điện tử chuyên biệt về bia số một Việt Nam', 'vuabia, vuabia tết canh tý, vuabia tet 2020, vuabia khuyen mai, vuabia giamgia', 0, 255, 0, '2019-12-17 15:59:22'),
(17, 2, 'Mã giảm giá Shopee', 'ma-giam-gia-shopee', 'Mã giảm giá Shopee, giảm giá shopee, coupon shopee, shopee tết 2020', 'Mã giảm giá Shopee, giảm giá shopee, coupon shopee, shopee tết 2020', 0, 1, 1, '2019-12-19 10:10:49'),
(18, 2, 'Mã giảm giá Tiki', 'ma-giam-gia-tiki', 'Mã giảm giá Tiki, giảm giá tiki, coupon tiki, tiki tết 2020', 'Mã giảm giá Tiki, giảm giá tiki, coupon tiki, tiki tết 2020', 0, 1, 1, '2019-12-19 10:11:24'),
(19, 2, 'Mã giảm giá Sendo', 'ma-giam-gia-sendo', 'Giảm giá hằng ngày luôn cập nhật mã giảm giá Sendo sớm nhất, coupon sendo, sendo mã khuyến mại khủng 2020', 'mã giảm giá sendo 2019, mã giảm giá sendo tết, sendo tết, sendo tết 2020, sendo coupon', 0, 1, 1, '2019-12-19 14:37:08'),
(20, 2, 'Tất cả ngành hàng - Sendo', 'tat-ca-nganh-hang', 'Mã giảm giá tất cả ngành hàng Sendo', 'sendo coupon, giảm giá sendo, mã giảm giá hằng ngày', 19, NULL, 0, '2019-12-19 14:52:53'),
(21, 2, 'Khách sạn & Du lịch - Reddoorz', 'reddoorz', 'mã giảm giá Reddoorz khuyến mại được cập nhật hằng ngày', 'mã giảm giá Reddoorz, coupon Reddoorz, Reddoorz coupon 2019, mã giảm giá Reddoorz 2020', 0, 255, 0, '2019-12-19 16:16:50'),
(22, 2, 'Mã giảm giá Sách - Tiki', 'ma-giam-gia-sach', 'Mã giảm giá sách Tiki cập nhật liên tục hằng ngày', 'tiki sach coupon, giam gia sach tiki, giảm giá sách tiki, sach tiki', 18, NULL, 1, '2019-12-19 17:10:29'),
(23, 2, 'Hầu hết các ngành hàng - Shopee', 'hau-het-cac-nganh-hang', 'Mã giảm giá Shopee hầu hết các ngành hàng cập nhật hằng ngày', 'mã giảm giá shopee, mã giảm giá tết shopee, coupon shopee tết, shopee giảm giá tết', 0, 255, 0, '2019-12-19 17:19:56'),
(24, 2, 'Mã giảm giá Lazada', 'ma-giam-gia-lazada', 'Mã giảm giá Lazada', 'Mã giảm giá Lazada', 0, 1, 0, '2020-01-02 04:53:11'),
(25, 2, 'Tết Canh Tý - Lazada', 'tet-canh-ty', 'Khuyến mại Lazada tết Canh Tý', 'Khuyến mại Lazada tết Canh Tý', 24, NULL, 0, '2020-01-02 04:53:41'),
(26, 2, 'Mã giảm giá Yes24', 'ma-giam-gia-yes24', 'Blog giảm giá hằng ngày luôn cập nhật các mã giảm giá Yes24 nhanh nhất, các chương trình khuyến mại đặc biệt của Yes24', 'yes24 giảm giá, yes24 voucher thời trang, yes24 voucher giày dép, yes24 coupon, khuyến mại yes24', 0, 1, 0, '2020-01-04 10:16:08'),
(27, 2, 'Giày dép - Yes24', 'giay-dep-yes24', 'Khuyến mại giày dép Yes24 cập nhật liên tục và chính xác nhất', 'yes24 giảm giá, yes24 voucher thời trang, yes24 voucher giày dép, yes24 coupon, khuyến mại yes24', 26, NULL, 0, '2020-01-04 10:17:04'),
(28, 2, 'Đồ gia dụng - Yes24', 'do-gia-dung-yes24', 'Đồ gia dụng chính hãng giá rẻ tại Yes24', 'đồ gia dụng yes24, sunhouse chính hãng giá rẻ', 26, NULL, 0, '2020-01-04 10:28:06'),
(29, 2, 'Thời trang - Yes24', 'thoi-trang-yes24', 'Mã giảm giá, mã khuyến mại Yes24 cập nhật hằng ngày', 'mã giảm giá yes24, voucher yes24, voucher thời trang yes24, voucher đồ gia dụng yes24', 26, NULL, 0, '2020-01-10 06:59:56'),
(30, 2, 'Tất cả ngành hàng - Lazada', 'tat-ca-nganh-hang-lazada', 'Giamgiahangngay.com cung cấp mã giảm giá lazada uy tín nhất', 'mã giảm giá lazada, blog giảm giá lazada, voucher giảm giá lazada, voucher giam gia lazada, ma giam gia lazada', 24, NULL, 0, '2020-01-10 09:17:35'),
(31, 2, 'Newshop - Siêu thị sách Online', 'newshop-sieu-thi-sach-online', 'Mã giảm giá sách Newshop cập nhật hằng ngày', 'mã giảm giá newshop, giảm giá sách newshop, voucher sách newshop', 13, NULL, 0, '2020-01-10 09:23:01'),
(35, 2, 'Tài chính - Ngân hàng', 'tai-chinh-ngan-hang', 'Dịch vụ vay tín chấp, vay thế chấp, mở thẻ tín dụng uy tín nhất', 'dich vu vay tin chap, dịch vụ vay tiền tín chấp, vay tin chap shinhanbank, vay tiền shinhanbank, mở thẻ tín dụng online, khuyến mại ngân hàng, khuyến mại vay vốn tín chấp', 0, 1, 0, '2020-01-10 09:31:49'),
(36, 2, 'Vay tiền ngân hàng (tín chấp, thế chấp)', 'vay-tien-ngan-hang-tin-chap-the-chap', 'Ưu đãi vay vốn tín chấp, vay vốn thế chấp chỉ có tại Giảm giá hằng ngày', 'vay tin chap online, vay tin chap qua luong, vay tin chap ngan hang online, vay tin chap lai suat thap', 35, NULL, 0, '2020-01-10 09:34:47'),
(37, 2, 'Con cưng - Siêu thị mẹ bầu và em bé', 'con-cung-sieu-thi-me-bau-va-em-be', 'Con cưng - Siêu thị mẹ bầu và em bé', 'mã giảm giá con cưng, voucher con cưng, voucher giam gia con cung, voucher giam gia me bau', 13, NULL, 0, '2020-01-11 02:07:31'),
(38, 2, 'Hành lý du lịch - Lug', 'hanh-ly-du-lich-lug', 'Mã giảm giá hành lý du lịch lug', 'mã giảm giá hành lý du lịch, ma giam gia du lich, ma giam gia lug, voucher giam gia lug.vn, giam gia lug.vn', 13, NULL, 0, '2020-01-11 02:14:21'),
(39, 2, 'Du lịch & Vé máy bay - BambooAirways', 'du-lich-ve-may-bay-bambooairways', 'Mã giảm giá vé máy bay bambooairways, khuyến mại bambooairways', 'voucher bambooairways, ma giam gia bambooairways, chuong trinh uu dai bambooairways', 13, NULL, 0, '2020-01-11 02:17:52'),
(40, 2, 'Hệ thống đồng hồ chính hãng - Xwatch', 'he-thong-dong-ho-chinh-hang-xwatch', 'Mã giảm giá đồng hồ chính hãng xwatch', 'voucher xwatch, voucher giảm giá đồng hồ', 13, NULL, 0, '2020-01-11 03:08:24'),
(41, 2, 'Du lịch & Tham quan - Klookvn', 'du-lich-tham-quan-klookvn', 'Mã giảm giá du lịch tự túc, vé tham quan klookvn', 'mã giảm giá klook, mã giảm giá du lịch, mã giảm giá vé tham quan, voucher klookvn', 13, NULL, 0, '2020-01-11 03:11:44'),
(42, 2, 'Nghỉ dưỡng & Vui chơi - Vinpearl', 'nghi-duong-vui-choi-vinpearl', 'Ưu đãi các kỳ nghỉ dưỡng và vui chơi tại Vinpearl', 'voucher vinpearl, voucher giảm giá vinpearl, voucher giam gia vinpearl, khuyen mai vinpearl, khuyến mại vinpearl', 13, NULL, 0, '2020-01-11 03:48:32'),
(43, 2, 'Điện thoại, Laptop, máy tính bảng', 'dien-thoai-laptop-may-tinh-bang', 'Voucher giảm giá hoanghamobile, voucher hoanghamobile', 'Mã giảm giá điện thoại, mã giảm giá laptop, mã giảm giá máy tính bảng, voucher giam gia dien thoai', 13, NULL, 0, '2020-01-11 03:54:14'),
(44, 2, 'Tất cả ngành hàng - VnExpress', 'tat-ca-nganh-hang-vnexpress', 'Shop VnExpress - Sàn TMĐT trực thuộc báo VnExpress - Mã giảm giá mua sắm Shop VnExpress online tin cậy', 'mua hàng Online, mua sam online, mua hang truc tuyen, mua ban online, mua sam truc tuyen, mua hang qua mang, Shop vnexpress, ma giam gia vnexpress, blog giam gia', 13, NULL, 0, '2020-01-13 03:26:09'),
(45, 2, 'Siêu thị điện máy, điện lạng - Nguyễn Kim', 'sieu-thi-dien-may-dien-lang-nguyen-kim', 'Mã giảm giá hàng ngàn sản phẩm điện máy, điện tử, điện lạnh tại siêu thị Nguyễn Kim', 'ma giam gia nguyenkimvn, ma giam gia nguyen kim, voucher nguyen kim, voucher giam gia nguyen kim', 13, NULL, 0, '2020-01-13 03:37:37'),
(46, 2, 'Hầu hết các ngành hàng', 'hau-het-cac-nganh-hang', '', '', 13, NULL, 0, '2020-02-13 11:05:59');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT 0,
  `post_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `comment` varchar(5000) DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `message` varchar(5000) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `file_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `file_name`) VALUES
(1, 'accesstrade-shopee-31-12-2019.xlsx'),
(2, 'accesstrade-lazada-02-01-2020.xlsx');

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

CREATE TABLE `followers` (
  `id` int(11) NOT NULL,
  `following_id` int(11) DEFAULT NULL,
  `follower_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_albums`
--

CREATE TABLE `gallery_albums` (
  `id` int(11) NOT NULL,
  `lang_id` tinyint(4) DEFAULT 1,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_categories`
--

CREATE TABLE `gallery_categories` (
  `id` int(11) NOT NULL,
  `lang_id` tinyint(4) DEFAULT 1,
  `name` varchar(255) DEFAULT NULL,
  `album_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE `general_settings` (
  `id` int(11) NOT NULL,
  `site_lang` tinyint(4) NOT NULL DEFAULT 1,
  `layout` varchar(30) DEFAULT 'layout_1',
  `dark_mode` tinyint(1) DEFAULT 0,
  `timezone` varchar(255) DEFAULT 'America/New_York',
  `slider_active` tinyint(1) DEFAULT 1,
  `site_color` varchar(30) DEFAULT 'default',
  `show_pageviews` tinyint(1) DEFAULT 1,
  `show_rss` tinyint(1) DEFAULT 1,
  `logo_path` varchar(255) DEFAULT NULL,
  `mobile_logo_path` varchar(255) DEFAULT NULL,
  `favicon_path` varchar(255) DEFAULT NULL,
  `facebook_app_id` varchar(500) DEFAULT NULL,
  `facebook_app_secret` varchar(500) DEFAULT NULL,
  `google_client_id` varchar(500) DEFAULT NULL,
  `google_client_secret` varchar(500) DEFAULT NULL,
  `google_analytics` text DEFAULT NULL,
  `google_adsense_code` text DEFAULT NULL,
  `mail_library` varchar(100) DEFAULT 'swift',
  `mail_protocol` varchar(100) DEFAULT 'smtp',
  `mail_host` varchar(255) DEFAULT NULL,
  `mail_port` varchar(255) DEFAULT '587',
  `mail_username` varchar(255) DEFAULT NULL,
  `mail_password` varchar(255) DEFAULT NULL,
  `mail_title` varchar(255) DEFAULT NULL,
  `send_email_contact_messages` tinyint(1) DEFAULT 0,
  `mail_options_account` varchar(255) DEFAULT NULL,
  `primary_font` varchar(255) DEFAULT 'open_sans',
  `secondary_font` varchar(255) DEFAULT 'roboto',
  `facebook_comment` text DEFAULT NULL,
  `pagination_per_page` tinyint(4) DEFAULT 6,
  `menu_limit` tinyint(4) DEFAULT 5,
  `multilingual_system` tinyint(1) DEFAULT 1,
  `registration_system` tinyint(1) DEFAULT 1,
  `comment_system` tinyint(1) DEFAULT 1,
  `comment_approval_system` tinyint(1) DEFAULT 1,
  `approve_posts_before_publishing` tinyint(1) DEFAULT 1,
  `emoji_reactions` tinyint(1) DEFAULT 1,
  `emoji_reactions_type` varchar(10) DEFAULT 'gif',
  `head_code` text DEFAULT NULL,
  `inf_key` varchar(500) NOT NULL,
  `purchase_code` varchar(500) NOT NULL,
  `recaptcha_site_key` varchar(500) DEFAULT NULL,
  `recaptcha_secret_key` varchar(500) DEFAULT NULL,
  `recaptcha_lang` varchar(30) DEFAULT NULL,
  `cache_system` tinyint(1) DEFAULT 0,
  `cache_refresh_time` int(11) DEFAULT 1800,
  `refresh_cache_database_changes` tinyint(1) DEFAULT 0,
  `text_editor_lang` varchar(10) DEFAULT 'en'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `general_settings`
--

INSERT INTO `general_settings` (`id`, `site_lang`, `layout`, `dark_mode`, `timezone`, `slider_active`, `site_color`, `show_pageviews`, `show_rss`, `logo_path`, `mobile_logo_path`, `favicon_path`, `facebook_app_id`, `facebook_app_secret`, `google_client_id`, `google_client_secret`, `google_analytics`, `google_adsense_code`, `mail_library`, `mail_protocol`, `mail_host`, `mail_port`, `mail_username`, `mail_password`, `mail_title`, `send_email_contact_messages`, `mail_options_account`, `primary_font`, `secondary_font`, `facebook_comment`, `pagination_per_page`, `menu_limit`, `multilingual_system`, `registration_system`, `comment_system`, `comment_approval_system`, `approve_posts_before_publishing`, `emoji_reactions`, `emoji_reactions_type`, `head_code`, `inf_key`, `purchase_code`, `recaptcha_site_key`, `recaptcha_secret_key`, `recaptcha_lang`, `cache_system`, `cache_refresh_time`, `refresh_cache_database_changes`, `text_editor_lang`) VALUES
(1, 2, 'layout_2', 0, 'Asia/Ho_Chi_Minh', 1, 'dev', 0, 1, 'uploads/logo/logo_5df27411bd2d8.png', 'uploads/logo/logo_5df27411bd2d81.png', 'uploads/logo/logo_5df2751e97e7d.png', NULL, NULL, NULL, NULL, '<!-- Global site tag (gtag.js) - Google Analytics -->\r\n<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-154581397-1\"></script>\r\n<script>\r\n  window.dataLayer = window.dataLayer || [];\r\n  function gtag(){dataLayer.push(arguments);}\r\n  gtag(\'js\', new Date());\r\n  gtag(\'config\', \'UA-154581397-1\');\r\n</script>', NULL, 'swift', 'smtp', NULL, '587', NULL, NULL, 'DEVNGUYENCMS', 0, '', 'roboto', 'roboto', '<div id=\"fb-root\"></div>\r\n<script async defer src=\"https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6\"></script>', 15, 7, 0, 1, 1, 0, 1, 0, 'png', '<meta name=\"google-site-verification\" content=\"-czYrK8BxWoMsDikwO0TllmJREi8eeTIyVeUf9Ev31g\" />', 'valid', 'devnguyen', NULL, NULL, 'en', 0, 1800, 0, 'vi');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `image_big` varchar(255) DEFAULT NULL,
  `image_mid` varchar(255) DEFAULT NULL,
  `image_small` varchar(255) DEFAULT NULL,
  `image_slider` varchar(255) DEFAULT NULL,
  `image_mime` varchar(30) DEFAULT 'jpg',
  `file_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `image_big`, `image_mid`, `image_small`, `image_slider`, `image_mime`, `file_name`) VALUES
(4, 'uploads/images/image_750x_5df7ba1c74893.jpg', 'uploads/images/image_750x415_5df7ba1c95933.jpg', 'uploads/images/image_100x75_5df7ba1cc7b51.jpg', 'uploads/images/image_650x433_5df7ba1cd7322.jpg', 'jpg', 'red_1'),
(5, 'uploads/images/image_750x_5df7bbd07f516.jpg', 'uploads/images/image_750x415_5df7bbd19ca52.jpg', 'uploads/images/image_100x75_5df7bbd2ad22f.jpg', 'uploads/images/image_650x433_5df7bbd3a7350.jpg', 'jpg', '1570603474_logo'),
(6, 'uploads/images/image_750x_5df8fde8a42b2.jpg', 'uploads/images/image_750x415_5df8fde8f0967.jpg', 'uploads/images/image_100x75_5df8fde959331.jpg', 'uploads/images/image_650x433_5df8fde987446.jpg', 'jpg', 'vua-bia-khuyen-mai-tet-canh-ty-2020-tet-sang-khoai-xuan-ron-ra'),
(7, 'uploads/images/image_750x_5df8ff99da3e4.jpg', 'uploads/images/image_750x415_5df8ff9a35229.jpg', 'uploads/images/image_100x75_5df8ff9a8c87a.jpg', 'uploads/images/image_650x433_5df8ff9abf872.jpg', 'jpg', 'vua-bia-khuyen-mai-tet-canh-ty-2020-tet-tri-an-xuan-tinh-nghia'),
(8, 'uploads/images/image_750x_5df9004c8958f.jpg', 'uploads/images/image_750x415_5df9004cd5825.jpg', 'uploads/images/image_100x75_5df9004d34938.jpg', 'uploads/images/image_650x433_5df9004d65fb9.jpg', 'jpg', 'vua-bia-khuyen-mai-tet-canh-ty-2020-tet-dam-dang-xuan-am-cung'),
(9, 'uploads/images/image_750x_5dfb8c4e30310.jpg', 'uploads/images/image_750x415_5dfb8c4ec0e54.jpg', 'uploads/images/image_100x75_5dfb8c4f4eaa4.jpg', 'uploads/images/image_650x433_5dfb8c4f964bd.jpg', 'jpg', 'sendo-sale-mung-giam-sinh-2019_giamgiahangngay_com'),
(10, 'uploads/images/image_750x_5dfba22cbe134.jpg', 'uploads/images/image_750x415_5dfba22d152c5.jpg', 'uploads/images/image_100x75_5dfba22d5ba07.jpg', 'uploads/images/image_650x433_5dfba22d7be5c.jpg', 'jpg', 'reddoozr-khuyen-mai-giang-sinh_giamgiahangngay_com'),
(11, 'uploads/images/image_750x_5dfbb050bb90c.jpg', 'uploads/images/image_750x415_5dfbb050e17c8.jpg', 'uploads/images/image_100x75_5dfbb05120bfd.jpg', 'uploads/images/image_650x433_5dfbb0513259b.jpg', 'jpg', 'giam-gia-sach-tiki_giamgiahangngay_com'),
(12, 'uploads/images/image_750x_5dfbb48f96eb6.jpg', 'uploads/images/image_750x415_5dfbb48fd1023.jpg', 'uploads/images/image_100x75_5dfbb49018565.jpg', 'uploads/images/image_650x433_5dfbb490319ab.jpg', 'jpg', 'shopee-giam-gia-cuoi-nam-2019_giamgiahangngay'),
(13, 'uploads/images/image_750x_5e1063d3a0990.jpg', 'uploads/images/image_750x415_5e1063d3e055c.jpg', 'uploads/images/image_100x75_5e1063d428333.jpg', 'uploads/images/image_650x433_5e1063d4461ce.jpg', 'jpg', 'lazada_30_12_3'),
(14, 'uploads/images/image_750x_5e10651c0a15d.jpg', 'uploads/images/image_750x415_5e10651c36453.jpg', 'uploads/images/image_100x75_5e10651c6c9f4.jpg', 'uploads/images/image_650x433_5e10651c8088a.jpg', 'jpg', 'yes24-giam-gia-giay'),
(15, 'uploads/images/image_750x_5e1066df388d4.jpg', 'uploads/images/image_750x415_5e1066df7715c.jpg', 'uploads/images/image_100x75_5e1066dfb52ac.jpg', 'uploads/images/image_650x433_5e1066dfd1b09.jpg', 'jpg', 'reddoorz_03_01'),
(16, 'uploads/images/image_750x_5e10686fb46f3.jpg', 'uploads/images/image_750x415_5e10687049264.jpg', 'uploads/images/image_100x75_5e106870c1828.jpg', 'uploads/images/image_650x433_5e106871265a3.jpg', 'jpg', 'yes24_27_12_3'),
(17, 'uploads/images/image_750x_5e106aabc8f46.jpg', 'uploads/images/image_750x415_5e106aabeaa25.jpg', 'uploads/images/image_100x75_5e106aac1f2c1.jpg', 'uploads/images/image_650x433_5e106aac2ea10.jpg', 'jpg', 'sendovn-ngay-hoi-toan-dan-sam-tet');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` tinyint(4) NOT NULL,
  `name` varchar(100) NOT NULL,
  `short_form` varchar(30) NOT NULL,
  `language_code` varchar(30) NOT NULL,
  `folder_name` varchar(120) NOT NULL,
  `text_direction` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `language_order` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `short_form`, `language_code`, `folder_name`, `text_direction`, `status`, `language_order`) VALUES
(1, 'English', 'en', 'en-US', 'default', 'ltr', 1, 1),
(2, 'Vietnamese', 'vi', 'vi-VN', 'vietnamese', 'ltr', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `lang_id` tinyint(4) DEFAULT 1,
  `title` varchar(500) DEFAULT NULL,
  `page_description` varchar(500) DEFAULT NULL,
  `page_keywords` varchar(500) DEFAULT NULL,
  `slug` varchar(500) DEFAULT NULL,
  `is_custom` tinyint(1) DEFAULT 1,
  `page_content` longtext DEFAULT NULL,
  `page_order` int(11) DEFAULT 5,
  `page_active` tinyint(1) DEFAULT 1,
  `title_active` tinyint(1) DEFAULT 1,
  `breadcrumb_active` tinyint(1) DEFAULT 1,
  `right_column_active` tinyint(1) DEFAULT 1,
  `need_auth` tinyint(1) DEFAULT 0,
  `location` varchar(255) DEFAULT 'header',
  `link` varchar(1000) DEFAULT NULL,
  `parent_id` int(11) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(11) NOT NULL,
  `lang_id` tinyint(4) DEFAULT 1,
  `title` varchar(500) DEFAULT NULL,
  `album_id` int(11) DEFAULT 1,
  `category_id` int(11) DEFAULT NULL,
  `path_big` varchar(255) DEFAULT NULL,
  `path_small` varchar(255) DEFAULT NULL,
  `is_album_cover` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `polls`
--

CREATE TABLE `polls` (
  `id` int(11) NOT NULL,
  `lang_id` tinyint(4) DEFAULT 1,
  `question` text DEFAULT NULL,
  `option1` text DEFAULT NULL,
  `option2` text DEFAULT NULL,
  `option3` text DEFAULT NULL,
  `option4` text DEFAULT NULL,
  `option5` text DEFAULT NULL,
  `option6` text DEFAULT NULL,
  `option7` text DEFAULT NULL,
  `option8` text DEFAULT NULL,
  `option9` text DEFAULT NULL,
  `option10` text DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `poll_votes`
--

CREATE TABLE `poll_votes` (
  `id` int(11) NOT NULL,
  `poll_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `vote` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `lang_id` tinyint(4) DEFAULT 1,
  `title` varchar(500) DEFAULT NULL,
  `title_slug` varchar(500) DEFAULT NULL,
  `summary` varchar(1000) DEFAULT NULL,
  `content` longtext DEFAULT NULL,
  `coupon_options` text DEFAULT NULL,
  `keywords` varchar(500) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `image_big` varchar(255) DEFAULT NULL,
  `image_mid` varchar(255) DEFAULT NULL,
  `image_small` varchar(255) DEFAULT NULL,
  `image_slider` varchar(255) DEFAULT NULL,
  `image_mime` varchar(100) DEFAULT 'jpg',
  `is_slider` tinyint(1) DEFAULT 0,
  `is_picked` tinyint(1) DEFAULT 0,
  `hit` int(11) DEFAULT 0,
  `slider_order` tinyint(4) DEFAULT 0,
  `optional_url` varchar(1000) DEFAULT NULL,
  `post_type` varchar(30) DEFAULT 'post',
  `video_url` varchar(1000) DEFAULT NULL,
  `video_embed_code` varchar(1000) DEFAULT NULL,
  `image_url` varchar(1000) DEFAULT NULL,
  `need_auth` tinyint(1) DEFAULT 0,
  `visibility` tinyint(1) DEFAULT 1,
  `status` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `lang_id`, `title`, `title_slug`, `summary`, `content`, `coupon_options`, `keywords`, `user_id`, `category_id`, `image_big`, `image_mid`, `image_small`, `image_slider`, `image_mime`, `is_slider`, `is_picked`, `hit`, `slider_order`, `optional_url`, `post_type`, `video_url`, `video_embed_code`, `image_url`, `need_auth`, `visibility`, `status`, `created_at`) VALUES
(10, 2, '[REDDOORZ] - 9 NGÀY SALE KHỦNG', 'reddoorz-9-ngay-sale-khung', NULL, '<p class=\"fontSize20 colorBlack\"><strong>ĐỒNG GI&Aacute; 109K - 139K - 149K!</strong></p>\r\n<p><strong><span>ĐỒNG GI&Aacute; 109K - 139K - 149K/Đ&Ecirc;M&nbsp;| &Aacute;p dụng cho kh&aacute;ch sạn thuộc chương tr&igrave;nh</span></strong></p>\r\n<p align=\"left\"><span><strong>ĐIỀU KIỆN CHƯƠNG TR&Igrave;NH:</strong></span></p>\r\n<ul align=\"left\" class=\"deal_page_1\">\r\n<li><span>Thời gian đặt ph&ograve;ng: 11-20/12/2019</span></li>\r\n<li><span>Thời gian nhận ph&ograve;ng: Đến hết 20/12/2019</span><span>&nbsp;</span></li>\r\n<li><span>Kh&ocirc;ng &aacute;p dụng chung với m&atilde; giảm gi&aacute; kh&aacute;c. VẪN GIẢM TH&Ecirc;M 12% cho Hội Vi&ecirc;n RedClub&nbsp;</span></li>\r\n</ul>\r\n<p><span><strong>Đ&Agrave; NẴNG: ĐỒNG GI&Aacute; 109K/Đ&Ecirc;M C&Aacute;C KH&Aacute;CH SẠN</strong></span></p>\r\n<p><span>RedDoorz @ Cach Mang Thang Tam Danang<br />RedDoorz @ Ho Xuan Huong Street<br />RedDoorz near Korean Town<br />RedDoorz near Cau Song Han<br />RedDoorz near Nguyen Van Thoai Street<br />RedDoorz near My Khe Beach 3<br />RedDoorz near Cau Song Han 2<br />RedDoorz near Han Market</span></p>\r\n<p><span><strong>VŨNG T&Agrave;U: ĐỒNG GI&Aacute; 139K/Đ&Ecirc;M C&Aacute;C KH&Aacute;CH SẠN</strong></span></p>\r\n<p><span>RedDoorz near Bai Sau 3<br />RedDoorz near Bai Sau 2<br />RedDoorz near Vung Tau Lighthouse</span></p>\r\n<p><span><strong>NHA TRANG:&nbsp;ĐỒNG GI&Aacute; 149K/Đ&Ecirc;M C&Aacute;C KH&Aacute;CH SẠN</strong></span></p>\r\n<p><span>Reddoorz near Bai Sau Nha Trang<br />RedDoorz near Bai Sau Nha Trang 2<br />Reddoorz near Tran Phu Street Nha Trang<br />Reddoorz near Hon Chong</span></p>\r\n<p></p>', 'a:4:{s:11:\"coupon_code\";s:16:\"KHONG_CAN_COUPON\";s:11:\"coupon_date\";s:10:\"20/12/2019\";s:11:\"coupon_note\";s:138:\"Đồng giá phòng 109k - 139k - 149k ở 3 thành phố Đà Nẵng, Vũng Tàu, Nha Trang Có trên website, app RedDoorz. ????????????\";s:14:\"affiliate_link\";s:122:\"https://go.isclix.com/deep_link/5229553935641796647?url=https://www.reddoorz.com/vi-vn/deal_page/169?id=169&page=deal_page\";}', 'mã giảm giá RedDoorz, mã khuyến mại RedDoorz, coupn RedDoorz 2019, coupon RedDoorz 2020', 1, 13, 'uploads/images/image_750x_5df7ba1c74893.jpg', 'uploads/images/image_750x415_5df7ba1c95933.jpg', 'uploads/images/image_100x75_5df7ba1cc7b51.jpg', 'uploads/images/image_650x433_5df7ba1cd7322.jpg', 'jpg', 0, 0, 76, 0, '', 'post', '', '', '', 0, 1, 1, '2019-12-16 17:09:52'),
(11, 2, '[Beauty Buffet] GIẢM 10% CHO ĐƠN HÀNG TỪ 99K', 'giam-10-cho-don-hang-tu-99k', NULL, '', 'a:4:{s:11:\"coupon_code\";s:13:\"ACCESSTRADE10\";s:11:\"coupon_date\";s:10:\"31/12/2019\";s:11:\"coupon_note\";s:35:\"GIẢM 10% CHO ĐƠN HÀNG TỪ 99K\";s:14:\"affiliate_link\";s:83:\"https://go.isclix.com/deep_link/5229553935641796647?url=https://beautybuffetvn.com/\";}', 'BEAUTY BUFFET giảm giá, coupon beauty buffet, coupon tết beauty buffet', 1, 14, 'uploads/images/image_750x_5df7bbd07f516.jpg', 'uploads/images/image_750x415_5df7bbd19ca52.jpg', 'uploads/images/image_100x75_5df7bbd2ad22f.jpg', 'uploads/images/image_650x433_5df7bbd3a7350.jpg', 'jpg', 0, 0, 41, 0, '', 'post', '', '', '', 0, 1, 1, '2019-12-16 17:18:40'),
(12, 2, '[Beauty Buffet] GIẢM 50K CHO ĐƠN HÀNG TỪ 399K', 'giam-50k-cho-don-hang-tu-399k', NULL, '', 'a:4:{s:11:\"coupon_code\";s:13:\"ACCESSTRADE50\";s:11:\"coupon_date\";s:10:\"31/12/2019\";s:11:\"coupon_note\";s:36:\"GIẢM 50K CHO ĐƠN HÀNG TỪ 399K\";s:14:\"affiliate_link\";s:83:\"https://go.isclix.com/deep_link/5229553935641796647?url=https://beautybuffetvn.com/\";}', 'beauty buffet coupon 2019, beauty buffet coupon 50k, giam gia beauty buffet', 1, 14, 'uploads/images/image_750x_5df7bbd07f516.jpg', 'uploads/images/image_750x415_5df7bbd19ca52.jpg', 'uploads/images/image_100x75_5df7bbd2ad22f.jpg', 'uploads/images/image_650x433_5df7bbd3a7350.jpg', 'jpg', 0, 0, 45, 0, '', 'post', '', '', '', 0, 1, 1, '2019-12-16 17:21:52'),
(13, 2, '[Vuabia] Vui cùng Vuabia - Tết sảng khoái, xuân rộn rã', 'tet-canh-ty-vui-cung-vuabia-tet-sang-khoai-xuan-ron-ra', NULL, '<p>Tết đến nơi rồi, uống g&igrave; b&acirc;y giờ nhỉ? tất nhi&ecirc;n l&agrave; bia rồi. <strong>Tết Canh T&yacute; 2020</strong> Vua Bia c&oacute; nhiều ưu đ&atilde;i d&agrave;nh cho kh&aacute;ch h&agrave;ng v&ocirc; c&ugrave;ng hấp dẫn. <strong>Vuabia</strong> tung ra chương tr&igrave;nh <a href=\"https://giamgiahangngay.com/\" target=\"_blank\" rel=\"noopener\"><strong>khuyến m&atilde;i Tết cực khủng</strong></a>, mua ngay vuabia c&ugrave;ng <a href=\"https://giamgiahangngay.com/\">Giảm gi&aacute; hằng ng&agrave;y</a> n&agrave;o</p>\r\n<p>&bull; Giảm ngay 140K khi mua <a href=\"https://giamgiahangngay.com/tag/vuabia-khuyen-mai\"><strong>combo 1 th&ugrave;ng Budweiser</strong></a> lon 330ml + 1 th&ugrave;ng CoCa 12 lon 330ml<br />&bull; Giảm ngay 170K khi mua <a href=\"https://giamgiahangngay.com/tag/vuabia\"><strong>combo 2 th&ugrave;ng Budweiser</strong></a> lon 330ml + 1 th&ugrave;ng CoCa 24 lon 250ml</p>\r\n<p>&bull; Giảm ngay 170K khi mua <a href=\"https://giamgiahangngay.com/tag/vuabia-khuyen-mai\"><strong>combo 1 th&ugrave;ng bia Bỉ</strong></a> bất k&igrave; + 1 th&ugrave;ng CoCa 24 lon 250ml<br />&bull; Giảm ngay 170K khi mua <a href=\"https://giamgiahangngay.com/tag/vuabia-khuyen-mai\"><strong>combo 2 th&ugrave;ng Budweiser</strong></a> lon 330ml + 1 th&ugrave;ng Sprite 24 lon 250ml</p>\r\n<p>&bull;Giảm ngay 170K khi mua combo 1 th&ugrave;ng bia Bỉ bất k&igrave; + 1 th&ugrave;ng Sprite 24 lon 250ml</p>', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"22/12/2019\";s:11:\"coupon_note\";s:0:\"\";s:14:\"affiliate_link\";s:89:\"https://go.isclix.com/deep_link/5229553935641796647?url=https://vuabia.com/tet_sang_khoai\";}', 'vuabia khuyen mai, vuabia giảm giá 2020, vuabia khuyến mại cực khủng, tết này uống vuabia, tết 2020 vuabia', 1, 16, 'uploads/images/image_750x_5df8fde8a42b2.jpg', 'uploads/images/image_750x415_5df8fde8f0967.jpg', 'uploads/images/image_100x75_5df8fde959331.jpg', 'uploads/images/image_650x433_5df8fde987446.jpg', 'jpg', 0, 0, 27, 0, '', 'post', '', '', '', 0, 1, 1, '2019-12-17 16:10:40'),
(14, 2, '[Vuabia] Vui bia - Tết tri ân, Xuân nghĩa tình', 'vui-bia-tet-tri-an-xuan-nghia-tinh', NULL, '', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"22/12/2019\";s:11:\"coupon_note\";s:0:\"\";s:14:\"affiliate_link\";s:85:\"https://go.isclix.com/deep_link/5229553935641796647?url=https://vuabia.com/tet_tri_an\";}', 'vuabia giam gia hang ngay, vuabia khuyen mai khung', 1, 16, 'uploads/images/image_750x_5df8ff99da3e4.jpg', 'uploads/images/image_750x415_5df8ff9a35229.jpg', 'uploads/images/image_100x75_5df8ff9a8c87a.jpg', 'uploads/images/image_650x433_5df8ff9abf872.jpg', 'jpg', 0, 0, 38, 0, '', 'post', '', '', '', 0, 1, 1, '2019-12-17 16:17:48'),
(15, 2, '[Vuabia] Tết đảm đang - Xuân ấm cùng cùng Vuabia 2020', 'tet-dam-dang-xuan-am-cung-cung-vuabia-2020', NULL, '<p>Tết Canh T&yacute; đang đến rất gần rồi, c&ograve;n chờ g&igrave; nữa m&agrave; kh&ocirc;ng mua Vuabia khuyến mại cực khủng với chương tr&igrave;nh:</p>\r\n<p style=\"text-align: center;\"><a href=\"https://giamgiahangngay.com/tag/vuabia-khuyen-mai-cuc-khung\"><strong>TẾT ĐẢM ĐANG - XU&Acirc;N ẤM CŨNG C&Ugrave;NG VUABIA</strong></a></p>\r\n<p style=\"text-align: left;\">Nội dung chương tr&igrave;nh khuyến mại vua bia th&aacute;ng 12/2019:</p>\r\n<p style=\"text-align: left;\"><span>+ Tặng ngay 1 chai dầu ăn Neptune 2L trị gi&aacute; 100k khi mua 1 th&ugrave;ng bia bất k&igrave;</span></p>', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"22/12/2019\";s:11:\"coupon_note\";s:0:\"\";s:14:\"affiliate_link\";s:87:\"https://go.isclix.com/deep_link/5229553935641796647?url=https://vuabia.com/tet_dam_dang\";}', 'vuabia khuyến mại khủng, vuabia giamgiahnagngay.com', 1, 16, 'uploads/images/image_750x_5df9004c8958f.jpg', 'uploads/images/image_750x415_5df9004cd5825.jpg', 'uploads/images/image_100x75_5df9004d34938.jpg', 'uploads/images/image_650x433_5df9004d65fb9.jpg', 'jpg', 0, 0, 48, 0, '', 'post', '', '', '', 0, 1, 1, '2019-12-17 16:22:54'),
(16, 2, '[Sendo] Sale lớn mừng giáng sinh 2019', 'sendo-sale-lon-mung-giang-sinh-2019', NULL, '<p><span>&nbsp;1. B&Iacute; MẬT Đ&Ecirc;M Đ&Ocirc;NG: <br /></span><span>- 00:00 mỗi ng&agrave;y: <a href=\"https://giamgiahangngay.com/tag/coupon-sendo\" title=\"Voucher sendo 2019\"><strong>Voucher b&iacute; mật</strong></a> ngẫu nhi&ecirc;n <strong>30K</strong> hoặc <strong>50K</strong> cho đơn h&agrave;ng từ <strong>100K</strong>. <br /></span><span>- 00:00 ng&agrave;y 24/12: Ho&agrave;n tiền 50% tối đa <strong>100K</strong>, đơn h&agrave;ng từ 100K. Ho&agrave;n tiền <strong>10%</strong> cả ng&agrave;y </span></p>\r\n<p><span>2. <a href=\"https://giamgiahangngay.com/tag/sendo-giang-sinh\" title=\"Sendo mừng gi&aacute;ng sinh đồng gi&aacute; tất cả sản phẩm\">ĐỒ Đ&Ocirc;NG ĐỒNG GI&Aacute;</a> : <strong>19K</strong> - <strong>29K</strong> - <strong>69K</strong> - <a href=\"https://giamgiahangngay.com/tag/sendo-giang-sinh\"><strong>99K</strong></a> - <strong>129K</strong>. </span></p>\r\n<p><span>3. SẮC M&Agrave;U GI&Aacute;NG SINH: Bộ sưu tập thời trang, phụ kiện, gi&agrave;y d&eacute;p, t&uacute;i x&aacute;ch.., theo m&agrave;u: Đỏ, Trắng, Xanh l&aacute;. </span></p>\r\n<p><span>4. THƯƠNG HIỆU M&Ugrave;A Đ&Ocirc;NG: <br /></span><span>- 17/12: <a href=\"https://giamgiahangngay.com/tag/sendo-giang-sinh\" title=\"sendo giảm gi&aacute; thời trang\">Gi&aacute;ng sinh mặc đẹp</a> (Thời trang) <br />- 18/12: <a href=\"https://giamgiahangngay.com/tag/sendo-giang-sinh\" title=\"sendo gi&aacute;ng sinh sale 2019\">Gi&aacute;ng sinh mẹ b&eacute;</a> (Ti&ecirc;u d&ugrave;ng, mẹ b&eacute;) <br />- 19/12: <a href=\"https://giamgiahangngay.com/tag/ma-giam-gia-giang-sinh\" title=\"Vui nodel đo&agrave;n vi&ecirc;n hạnh ph&uacute;c c&ugrave;ng giảm gi&aacute; sendo hằng ng&agrave;y\">Gi&aacute;ng sinh đo&agrave;n vi&ecirc;n</a> (Gia dụng) <br />- 20/12: <a href=\"https://giamgiahangngay.com/tag/sendo-giang-sinh\" title=\"sendo giảm gi&aacute; c&ocirc;ng nghệ 2019\">Gi&aacute;ng sinh thời thượng</a> (C&ocirc;ng nghệ) <br />- 21/12 &amp; 22/12: <a href=\"https://giamgiahangngay.com/tag/sendo-giang-sinh\" title=\"C&ugrave;ng sendo đ&oacute;n gi&aacute;ng sinh với sale cực lớn mỹ phẩm l&agrave;m đẹp\">Gi&aacute;ng sinh lung linh</a> (l&agrave;m đẹp) <br />- 23/12: <a href=\"https://giamgiahangngay.com/tag/ma-giam-gia-sendo\" title=\"Du lịch vi vu 2019 với sendo giảm gi&aacute; du lịch\">Vui lễ gi&aacute;ng sinh</a> (Du lịch dịch vụ) <br />- 24/12: <a href=\"https://giamgiahangngay.com/tag/sendo-giang-sinh\" title=\"giảm gi&aacute; tất cả ng&agrave;nh h&agrave;ng mừng gi&aacute;ng sinh noel 2019\">Si&ecirc;u phẩm gi&aacute;ng sinh</a> (Tất cả ng&agrave;nh h&agrave;ng)</span></p>', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"24/12/2019\";s:11:\"coupon_note\";s:275:\"- 00:00 mỗi ngày: Voucher bí mật ngẫu nhiên 30K hoặc 50K cho đơn hàng từ 100K. \r\n- 00:00 ngày 24/12: Hoàn tiền 50% tối đa 100K, đơn hàng từ 100K. Hoàn tiền 10% cả ngày \r\nĐặc biệt: ĐỒ ĐÔNG ĐỒNG GIÁ : 19K - 29K - 69K - 99K - 129K\";s:14:\"affiliate_link\";s:100:\"https://go.isclix.com/deep_link/5229553935641796647?url=https://www.sendo.vn/su-kien/sale-giang-sinh\";}', 'sendo giáng sinh, sendo coupon nodel 2019, sendo noel, mã giảm giá sendo noel, noel sendo', 1, 20, 'uploads/images/image_750x_5dfb8c4e30310.jpg', 'uploads/images/image_750x415_5dfb8c4ec0e54.jpg', 'uploads/images/image_100x75_5dfb8c4f4eaa4.jpg', 'uploads/images/image_650x433_5dfb8c4f964bd.jpg', 'jpg', 0, 0, 42, 0, '', 'post', '', '', '', 0, 1, 1, '2019-12-19 14:55:06'),
(17, 2, '[Reddoorz] X-MAS SALE - Giảm 20% toàn bộ giá phòng - Khuyến mãi hấp dẫn', 'reddoorz-x-mas-sale-giam-20-toan-bo-gia-phong-khuyen-mai-hap-dan', NULL, '<p class=\"fontSize20 colorBlack\" style=\"text-align: center;\">ƯU Đ&Atilde;I M&Ugrave;A GI&Aacute;NG SINH!</p>\r\n<p style=\"text-align: center;\"><strong>XMAS C&Oacute; Đ&Ocirc;I - Đ&Ecirc;M VỀ CHUNG LỐI!&nbsp;&nbsp;&nbsp;</strong></p>\r\n<p style=\"text-align: center;\">&nbsp;Nhập m&atilde; để được <a href=\"https://giamgiahangngay.com/tag/reddoorz+khuyen+mai\" title=\"Reddoorz khuyến mại giảm gi&aacute;p ph&ograve;ng\">GIẢM 20% gi&aacute; ph&ograve;ng</a> + TẶNG 2 V&Eacute; CGV sau khi bạn trả ph&ograve;ng&nbsp;&nbsp;</p>\r\n<p align=\"left\"><strong>ĐIỀUN KIỆN SỬ DỤNG M&Atilde;:</strong></p>\r\n<ul align=\"left\" class=\"deal_page_1\">\r\n<li>Thời gian đặt ph&ograve;ng: <strong>18-23/12/2019</strong></li>\r\n<li>Thời gian nhận ph&ograve;ng: <strong>Until 31/12/2020</strong></li>\r\n<li>&Aacute;p dụng cho tất cả kh&aacute;ch sạn <a href=\"https://giamgiahangngay.com/tag/reddoorz+khuyen+mai\" title=\"Reddoorz giảm gi&aacute; khủng\"><strong>RedDoorz</strong></a> tại Việt Nam&nbsp;&nbsp;</li>\r\n<li>&Aacute;p dụng khi đặt ph&ograve;ng tr&ecirc;n website/app RedDoorz&nbsp; &nbsp;</li>\r\n<li>Kh&ocirc;ng &aacute;p dụng chung với m&atilde; giảm gi&aacute; kh&aacute;c. VẪN <strong>GIẢM TH&Ecirc;M 12%</strong> cho Hội Vi&ecirc;n RedClub&nbsp;&nbsp;</li>\r\n</ul>\r\n<p align=\"left\"><strong>LƯU &Yacute; NHỎ XINH:</strong></p>\r\n<ul align=\"left\" class=\"deal_page_1\">\r\n<li>Qu&agrave; tặng 2 v&eacute; CGV 2D chỉ &aacute;p dụng cho đặt ph&ograve;ng c&oacute; tổng gi&aacute; ph&ograve;ng từ 300.000 VNĐ, sau khi khấu trừ giảm gi&aacute; nhờ nhập m&atilde;</li>\r\n<li>Qu&agrave; tặng sẽ được gửi đến email của bạn trong v&ograve;ng 48h sau khi bạn trả ph&ograve;ng</li>\r\n<li>Chương tr&igrave;nh k&eacute;o d&agrave;i đến hết 23/12, hoặc đến khi hết v&eacute; tặng</li>\r\n</ul>', 'a:4:{s:11:\"coupon_code\";s:7:\"HENHO20\";s:11:\"coupon_date\";s:10:\"23/12/2019\";s:11:\"coupon_note\";s:345:\"Quà tặng 2 vé CGV 2D chỉ áp dụng cho đặt phòng có tổng giá phòng từ 300.000 VNĐ, sau khi khấu trừ giảm giá nhờ nhập mã <br> \r\nQuà tặng sẽ được gửi đến email của bạn trong vòng 48h sau khi bạn trả phòng <br>\r\nChương trình kéo dài đến hết 23/12, hoặc đến khi hết vé tặng\";s:14:\"affiliate_link\";s:122:\"https://go.isclix.com/deep_link/5229553935641796647?url=https://www.reddoorz.com/vi-vn/deal_page/171?id=171&page=deal_page\";}', 'reddoorz giảm giá, reddoorz coupon giảm giá, reddoorz  khuyến mãi', 1, 21, 'uploads/images/image_750x_5dfba22cbe134.jpg', 'uploads/images/image_750x415_5dfba22d152c5.jpg', 'uploads/images/image_100x75_5dfba22d5ba07.jpg', 'uploads/images/image_650x433_5dfba22d7be5c.jpg', 'jpg', 0, 0, 32, 0, '', 'post', '', '', '', 0, 1, 1, '2019-12-19 16:23:21'),
(18, 2, '[Tiki] Mã giảm giá 15% sản phẩm Sách tiếng Anh', 'tiki-ma-giam-gia-15-san-pham-sach-tieng-anh', NULL, '', 'a:4:{s:11:\"coupon_code\";s:6:\"TIKIFB\";s:11:\"coupon_date\";s:10:\"31/12/2019\";s:11:\"coupon_note\";s:54:\"Mã giảm giá 15% cho sản phẩm sách tiếng Anh\";s:14:\"affiliate_link\";s:80:\"https://tiki.vn/sach-tieng-anh/c320?_lc=&src=c.320.hamburger_menu_fly_out_banner\";}', 'sach tiki, ma giam gia tiki, giam gia sach tiki, coupon tiki sach, coupon sach tiki', 1, 22, 'uploads/images/image_750x_5dfbb050bb90c.jpg', 'uploads/images/image_750x415_5dfbb050e17c8.jpg', 'uploads/images/image_100x75_5dfbb05120bfd.jpg', 'uploads/images/image_650x433_5dfbb0513259b.jpg', 'jpg', 0, 0, 43, 0, '', 'post', '', '', '', 0, 1, 1, '2019-12-19 17:16:27'),
(19, 2, '[Shopee] Siêu giảm giá 50% hầu hết các ngành hàng - Miễn phí ship', 'shopee-sieu-giam-gia-50-hau-het-cac-nganh-hang-mien-phi-ship', NULL, '', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"31/12/2019\";s:11:\"coupon_note\";s:101:\"Siêu sale giảm 50% theo ngành hàng. Khuyến mại chỉ áp dụng khi Click [Nhận ưu đãi]\";s:14:\"affiliate_link\";s:91:\"https://go.isclix.com/deep_link/5229553935641796647?url=https://shopee.vn/m/sieu-nganh-hang\";}', 'giảm giá shopee, coupon shopee all, shopee tết 2020, shopee tết canh tý, giam gia hang ngay', 1, 23, 'uploads/images/image_750x_5dfbb48f96eb6.jpg', 'uploads/images/image_750x415_5dfbb48fd1023.jpg', 'uploads/images/image_100x75_5dfbb49018565.jpg', 'uploads/images/image_650x433_5dfbb490319ab.jpg', 'jpg', 0, 0, 30, 0, '', 'post', '', '', '', 0, 1, 1, '2019-12-19 17:34:55'),
(20, 2, '[Shopee - Bitisvn] Giảm 15% tối đa 120K cho đơn từ 599K', 'shopee-bitisvn-giam-15-toi-da-120k-cho-don-tu-599k', NULL, 'Giảm 15% tối đa 120K cho đơn từ 599K', 'a:4:{s:11:\"coupon_code\";s:13:\"FASHIONBITI15\";s:11:\"coupon_date\";s:10:\"31/12/2019\";s:11:\"coupon_note\";s:45:\"Giảm 15% tối đa 120K cho đơn từ 599K\";s:14:\"affiliate_link\";s:89:\"https://go.isclix.com/deep_link/5229553935641790464?url=https%3A%2F%2Fshopee.vn%2Fbitisvn\";}', 'mã giảm giá bitis, voucher bitis 2019, mã coupon bitis, shopee bitis', 1, 17, 'uploads/import/image_750x_5e0b0d5601287.jpg', 'uploads/import/image_750x415_5e0b0d561b1d9.jpg', 'uploads/import/image_100x75_5e0b0d56444e6.jpg', 'uploads/import/image_650x433_5e0b0d5652699.jpg', 'jpg', 0, 0, 16, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2019-12-31 08:56:54'),
(21, 2, '[Shopee - Juno] Giảm 20%. tối đa 80k cho đơn từ 199k', 'shopee-juno-giam-20-toi-da-80k-cho-don-tu-199k', NULL, 'Giảm 20%. tối đa 80k cho đơn từ 199k', 'a:4:{s:11:\"coupon_code\";s:11:\"FASHIONJU31\";s:11:\"coupon_date\";s:10:\"31/12/2019\";s:11:\"coupon_note\";s:78:\"Áp dụng cho các sản phẩm Thời Trang thuộc shop @juno.officialstore\";s:14:\"affiliate_link\";s:100:\"https://go.isclix.com/deep_link/5229553935641790464?url=https%3A%2F%2Fshopee.vn%2Fjuno.officialstore\";}', 'mã giảm giá juno shopee, mã giảm giá thời trang juno, giảm giá juno shopee', 1, 17, 'uploads/import/image_750x_5e0b0d57f2b6f.jpg', 'uploads/import/image_750x415_5e0b0d5816dca.jpg', 'uploads/import/image_100x75_5e0b0d583fe66.jpg', 'uploads/import/image_650x433_5e0b0d584d1eb.jpg', 'jpg', 0, 0, 19, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2019-12-31 08:56:56'),
(22, 2, '[Shopee - Giày] Giảm 100k cho đơn hàng từ 800k giày Converse chính hãng', 'shopee-giay-giam-100k-cho-don-hang-tu-800k-giay-converse-chinh-hang', NULL, 'Giảm 100k cho đơn hàng từ 800k giày Converse chính hãng', 'a:4:{s:11:\"coupon_code\";s:8:\"MACOV312\";s:11:\"coupon_date\";s:10:\"31/12/2019\";s:11:\"coupon_note\";s:69:\"Áp dụng cho các sản phẩm Converse thuộc shop @drakedangkhoa\";s:14:\"affiliate_link\";s:95:\"https://go.isclix.com/deep_link/5229553935641790464?url=https%3A%2F%2Fshopee.vn%2Fdrakedangkhoa\";}', 'mã giảm giá giày converse, giày converse giá rẻ shopee', 1, 17, 'uploads/import/image_750x_5e0b0d59e3696.jpg', 'uploads/import/image_750x415_5e0b0d5a072d5.jpg', 'uploads/import/image_100x75_5e0b0d5a30855.jpg', 'uploads/import/image_650x433_5e0b0d5a3e124.jpg', 'jpg', 0, 0, 25, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2019-12-31 08:56:58'),
(23, 2, '[Shopee - Thời trang] Giảm 20% tối đa 100k cho đơn từ 399k thời trang Kappa', 'shopee-thoi-trang-giam-20-toi-da-100k-cho-don-tu-399k-thoi-trang-kappa', NULL, '<p>Giảm 20% tối đa 100k cho đơn từ 399k</p>', 'a:4:{s:11:\"coupon_code\";s:14:\"FASHIONKAPPA31\";s:11:\"coupon_date\";s:10:\"31/12/2019\";s:11:\"coupon_note\";s:75:\"Áp dụng cho các sản phẩm Kappa thuộc shop @hoangphucinternational\";s:14:\"affiliate_link\";s:96:\"https://go.isclix.com/deep_link/5229553935641790464?url=https://shopee.vn/hoangphucinternational\";}', 'mã giảm giá thời trang kappa, kappa shopee coupon, shopee kappa chính hãng', 1, 17, 'uploads/import/image_750x_5e0b0d5a7b5ca.jpg', 'uploads/import/image_750x415_5e0b0d5a7f1dc.jpg', 'uploads/import/image_100x75_5e0b0d5a942a5.jpg', 'uploads/import/image_650x433_5e0b0d5a96a46.jpg', 'jpg', 0, 0, 20, 0, '', 'post', '', '', '', 0, 1, 1, '2019-12-31 08:56:58'),
(24, 2, '[Lazada] Chương trình siêu giảm giá mừng Tết là lá la', 'lazada-chuong-trinh-sieu-giam-gia-mung-tet-la-la-la', NULL, '???? Cam kết giá rẻ - 8000+ DEAL ĐÃ BÁN  - Giảm đến 75%<br>\n???? Voucher 888K cho đơn từ 6,8tr vào 12h trưa mỗi ngày<br>\n???? Miễn phí vận chuyển toàn quốc <br>\n???? Deal Lộc Phát 8K<br>\n???? Khung giờ Flash Sale mỗi ngày từ 3-12/1: 00:00<br>\n???? Khung giờ Crazy Flash Sale mỗi ngày từ 3-12/1: 9:00 - 12:00 - 16:00 - 19:00 - 21:00<br>\n???? Chém giá  - Chỉ trên App. Khung giờ Chém giá: 1 khung giờ 9pm-0am ngày 3-7/1<br>\n???? Shake it - Lắc nhiều để trúng voucher 88K mỗi ngày - 12h trưa & 20h tối.', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:9:\"12/1/2020\";s:11:\"coupon_note\";s:84:\"CLICK [Nhận ưu đãi] để được hưởng ưu đãi chương trình này nhé\";s:14:\"affiliate_link\";s:104:\"https://go.isclix.com/deep_link/5229553935641790464?url=https%3A%2F%2Fwww.lazada.vn%2Flazada-tet-2020%2F\";}', 'mã giảm giá hằng ngày lazada, mã giảm giá lazada 2020, ma giam gia lazada, giam gia lazada 2020, lazada giam gia tet nguyen dan, lazada coupon 2020, voucher lazada 2020', 1, 25, 'uploads/import/image_750x_5e0d776ad5b83.jpg', 'uploads/import/image_750x415_5e0d776b2769c.jpg', 'uploads/import/image_100x75_5e0d776b670bc.jpg', 'uploads/import/image_650x433_5e0d776b84bfc.jpg', 'jpg', 0, 0, 19, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-02 04:54:03'),
(25, 2, '[Lazada - Điện tử] Giảm giá lên đến 60% điện thoại - máy tính bảng', 'lazada-dien-tu-giam-gia-len-den-60-dien-thoai-may-tinh-bang', NULL, '\"????Giảm đến 60%<br>\n????Voucher 1 triệu\"', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:9:\"12/1/2020\";s:11:\"coupon_note\";s:84:\"CLICK [Nhận ưu đãi] để được hưởng ưu đãi chương trình này nhé\";s:14:\"affiliate_link\";s:169:\"https://go.isclix.com/deep_link/5229553935641790464?url=https%3A%2F%2Fpages.lazada.vn%2Fwow%2Fcamp%2Flazada%2Fmegascenario%2Fvn%2Fvn-tet-2020%2FHome-Page-Mobiles-Tablets\";}', 'mã giảm giá hằng ngày lazada, mã giảm giá lazada 2020, ma giam gia lazada, giam gia lazada 2020, lazada giam gia tet nguyen dan, lazada coupon 2020, voucher lazada 2020, ma giam gia dien thoai lazada', 1, 25, 'uploads/import/image_750x_5e0d776e1c479.jpg', 'uploads/import/image_750x415_5e0d776e5b804.jpg', 'uploads/import/image_100x75_5e0d776e98711.jpg', 'uploads/import/image_650x433_5e0d776eb4f02.jpg', 'jpg', 0, 0, 20, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-02 04:54:06'),
(26, 2, '[Lazada - Sức khoẻ sắc đẹp] Giảm giá khủng chỉ từ 188k hàng chính hãng', 'lazada-suc-khoe-sac-dep-giam-gia-khung-chi-tu-188k-hang-chinh-hang', NULL, '????Giftbox chỉ từ 188K', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:9:\"12/2/2020\";s:11:\"coupon_note\";s:84:\"CLICK [Nhận ưu đãi] để được hưởng ưu đãi chương trình này nhé\";s:14:\"affiliate_link\";s:173:\"https://go.isclix.com/deep_link/5229553935641790464?url=https%3A%2F%2Fpages.lazada.vn%2Fwow%2Fcamp%2Flazada%2Fmegascenario%2Fvn%2Fvn-tet-2020%2Fsuc-khoe-lam-dep%3Fhybrid%3D1\";}', 'mã giảm giá hằng ngày lazada, mã giảm giá lazada 2020, ma giam gia lazada, giam gia lazada 2020, lazada giam gia tet nguyen dan, lazada coupon 2020, voucher lazada 2020, ma giam gia dien thoai lazada, ma giam gia my pham lazada', 1, 25, 'uploads/import/image_750x_5e0d77712911b.jpg', 'uploads/import/image_750x415_5e0d777165aaf.jpg', 'uploads/import/image_100x75_5e0d7771a11d6.jpg', 'uploads/import/image_650x433_5e0d7771be69b.jpg', 'jpg', 0, 0, 15, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-02 04:54:10'),
(27, 2, '[Lazada - Đồ chơi trẻ em] Nhận voucher 200k, siêu quà lên đến 800k', 'lazada-do-choi-tre-em-nhan-voucher-200k-sieu-qua-len-den-800k', NULL, '????Voucher 200K<br>\n????Siêu Quà Đến 800K', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:9:\"12/3/2020\";s:11:\"coupon_note\";s:84:\"CLICK [Nhận ưu đãi] để được hưởng ưu đãi chương trình này nhé\";s:14:\"affiliate_link\";s:171:\"https://go.isclix.com/deep_link/5229553935641790464?url=https%3A%2F%2Fpages.lazada.vn%2Fwow%2Fcamp%2Flazada%2Fmegascenario%2Fvn%2Fvn-tet-2020%2Fme-be-vui-xuan%3Fhybrid%3D1\";}', 'mã giảm giá hằng ngày lazada, mã giảm giá lazada 2020, ma giam gia lazada, giam gia lazada 2020, lazada giam gia tet nguyen dan, lazada coupon 2020, voucher lazada 2020, ma giam gia dien thoai lazada, ma giam gia my pham lazada, do choi tre em lazada', 1, 25, 'uploads/import/image_750x_5e0d777443dd2.jpg', 'uploads/import/image_750x415_5e0d77749239d.jpg', 'uploads/import/image_100x75_5e0d7774cec11.jpg', 'uploads/import/image_650x433_5e0d7774f1849.jpg', 'jpg', 0, 0, 22, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-02 04:54:13'),
(28, 2, '[Lazada - Thời trang & Thể thao] Săn deal măn mắn chỉ 8k, miễn phí vận chuyển', 'lazada-thoi-trang-the-thao-san-deal-man-man-chi-8k-mien-phi-van-chuyen', NULL, '', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"12/01/2020\";s:11:\"coupon_note\";s:78:\"Thời trang & Thể thao: Săn deal may mắn 8K. Miễn phí vận chuyển.\";s:14:\"affiliate_link\";s:94:\"https://go.isclix.com/deep_link/5229553935641796647?url=https://www.lazada.vn/lazada-tet-2020/\";}', 'mã khuyến mại hằng ngày, mã khuyến mại lazada, voucher lazada 2020, tết lazada, new year 2020 lazada, hàng chính hãng giá rẻ lazada', 1, 25, 'uploads/images/image_750x_5e1063d3a0990.jpg', 'uploads/images/image_750x415_5e1063d3e055c.jpg', 'uploads/images/image_100x75_5e1063d428333.jpg', 'uploads/images/image_650x433_5e1063d4461ce.jpg', 'jpg', 0, 0, 13, 0, '', 'post', '', '', '', 0, 1, 1, '2020-01-04 10:07:35'),
(29, 2, '[Yes24 - Thời trang] Giày nữ hàn quốc giảm đến 45%', 'yes24-thoi-trang-giay-nu-han-quoc-giam-den-45', NULL, '<h3 class=\"text-uppercase push-10 push-10-t\">GI&Agrave;Y NỮ H&Agrave;N QUỐC TẠI YES24 GIẢM ĐẾN 45% - CHỈ C&Oacute; TẠI BLOG GIẢM GI&Aacute; HẰNG NG&Agrave;Y</h3>', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"05/01/2020\";s:11:\"coupon_note\";s:67:\"Giày nữ Hàn Quốc tại Yes24 đang được giảm đến 45%\";s:14:\"affiliate_link\";s:107:\"https://go.isclix.com/deep_link/5229553935641796647?url=https://www.yes24.vn/giay-nu-han-quoc-km509600.html\";}', 'yes24 giảm giá, yes24 voucher thời trang, yes24 voucher giày dép, yes24 coupon, khuyến mại yes24', 1, 27, 'uploads/images/image_750x_5e10651c0a15d.jpg', 'uploads/images/image_750x415_5e10651c36453.jpg', 'uploads/images/image_100x75_5e10651c6c9f4.jpg', 'uploads/images/image_650x433_5e10651c8088a.jpg', 'jpg', 0, 0, 14, 0, '', 'post', '', '', '', 0, 1, 1, '2020-01-04 10:17:22'),
(30, 2, '[RedDoorz] Mừng năm mới khuyến mại lên đến 29%', 'reddoorz-mung-nam-moi-khuyen-mai-len-den-29', NULL, '<p class=\"fontSize20 colorBlack\" style=\"text-align: center;\"><strong>ƯU Đ&Atilde;I NĂM MỚI 2020</strong></p>\r\n<p style=\"text-align: center;\"><strong>C&Oacute; REDDOORZ, TẾT N&Agrave;Y&nbsp;VI VU KHẮP CHỐN!&nbsp;&nbsp;&nbsp;</strong></p>\r\n<p style=\"text-align: center;\">&nbsp;Nhập m&atilde; ngay | Giảm 29% kh&aacute;ch sạn RedDoorz&nbsp;&nbsp;</p>\r\n<p align=\"left\"><strong>ĐIỀU KIỆN SỬ DỤNG M&Atilde; GIẢM GI&Aacute;:</strong></p>\r\n<ul align=\"left\" class=\"deal_page_1\">\r\n<li>Thời gian đặt ph&ograve;ng: 02-07/01/2020</li>\r\n<li>Thời gian nhận ph&ograve;ng: Đến hết&nbsp;31/01/2020</li>\r\n<li>&Aacute;p dụng cho tất cả đặt ph&ograve;ng qua website hoặc app RedDoorz&nbsp;&nbsp;</li>\r\n<li>&Aacute;p dụng cho tất cả kh&aacute;ch sạn RedDoorz ở TP. Hồ Ch&iacute; Minh v&agrave; H&agrave; Nội&nbsp;&nbsp; &nbsp;</li>\r\n<li>Kh&ocirc;ng &aacute;p dụng chung với c&aacute;c chương tr&igrave;nh giảm gi&aacute; kh&aacute;c. VẪN GIẢM TH&Ecirc;M 12% cho Hội vi&ecirc;n RedClub&nbsp;</li>\r\n</ul>', 'a:4:{s:11:\"coupon_code\";s:5:\"ANTET\";s:11:\"coupon_date\";s:10:\"07/01/2020\";s:11:\"coupon_note\";s:181:\"Giảm 29% cho các khách sạn thuộc hệ thống của RedDoorz ở HCM và Hà Nội. Thời gian đặt phòng từ 02-07/01/2020, thời gian nhận phòng đến 31/01/2020\";s:14:\"affiliate_link\";s:122:\"https://go.isclix.com/deep_link/5229553935641796647?url=https://www.reddoorz.com/vi-vn/deal_page/174?id=174&page=deal_page\";}', 'RedDoorz tết canh tý 2020, RedDoorz giảm giá, RedDoorz voucher', 1, 21, 'uploads/images/image_750x_5e1066df388d4.jpg', 'uploads/images/image_750x415_5e1066df7715c.jpg', 'uploads/images/image_100x75_5e1066dfb52ac.jpg', 'uploads/images/image_650x433_5e1066dfd1b09.jpg', 'jpg', 0, 0, 22, 0, '', 'post', '', '', '', 0, 1, 1, '2020-01-04 10:22:42'),
(31, 2, '[Yes24 - Điện gia dụng] Sunhouse xả kho điện gia dụng giảm giá cuối năm lên đến 50%', 'yes24-dien-gia-dung-sunhouse-xa-kho-dien-gia-dung-giam-gia-cuoi-nam-len-den-50', NULL, '<p><span>Sunhouse điện gia dụng - giảm đến 50%</span></p>', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"10/01/2020\";s:11:\"coupon_note\";s:47:\"Sunhouse điện gia dụng - giảm đến 50%\";s:14:\"affiliate_link\";s:121:\"https://go.isclix.com/deep_link/5229553935641796647?url=https://www.yes24.vn/sunhouse-dien-gia-dung-gia-soc-km703330.html\";}', 'yes24 đồ gia dụng, đồ gia dụng giá rẻ, mã giảm giá đồ gia dụng, yes24 giảm giá sunhouse, sunhouse giảm giá, sunhouse chính hãng giá rẻ', 1, 28, 'uploads/images/image_750x_5e10686fb46f3.jpg', 'uploads/images/image_750x415_5e10687049264.jpg', 'uploads/images/image_100x75_5e106870c1828.jpg', 'uploads/images/image_650x433_5e106871265a3.jpg', 'jpg', 0, 0, 9, 0, '', 'post', '', '', '', 0, 1, 1, '2020-01-04 10:28:55'),
(32, 2, '[Sendo - Siêu deal] Ngày hội toàn dân sắm Tết - Săn deal cực khủng', 'sendo-sieu-deal-ngay-hoi-toan-dan-sam-tet-san-deal-cuc-khung', NULL, '<p><span>Ho&agrave;n tiền 888K +Si&ecirc;u deal 12K + Miễn ph&iacute; vận chuyển</span></p>', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"10/01/2020\";s:11:\"coupon_note\";s:62:\"Hoàn tiền 888K +Siêu deal 12K + Miễn phí vận chuyển\";s:14:\"affiliate_link\";s:129:\"https://go.isclix.com/deep_link/5229553935641796647?url=https://www.sendo.vn/su-kien/tet-tieu-dung-me-be/?ref=home_slider_desktop\";}', 'sendo săn deal, sendo tết canh tý, voucher sendo', 1, 20, 'uploads/images/image_750x_5e106aabc8f46.jpg', 'uploads/images/image_750x415_5e106aabeaa25.jpg', 'uploads/images/image_100x75_5e106aac1f2c1.jpg', 'uploads/images/image_650x433_5e106aac2ea10.jpg', 'jpg', 0, 0, 24, 0, '', 'post', '', '', '', 0, 1, 1, '2020-01-04 10:36:42'),
(33, 2, 'Top Brand Thời trang nam nữ Hàn Quốc khuyến mãi 50%', 'top-brand-thoi-trang-nam-nu-han-quoc-khuyen-mai-50', NULL, 'Top Brand Thời trang nam nữ Hàn Quốc khuyến mãi 50%', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"14/01/2020\";s:11:\"coupon_note\";s:0:\"\";s:14:\"affiliate_link\";s:134:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.yes24.vn%2Fsan-pham-hang-nhap-korea-khuyen-mai-km573530.html\";}', 'mã giảm giá yes24, voucher giảm giá yes24, coupon yes24, voucher giảm giá yes24 năm 2020', 1, 29, 'uploads/import/image_750x_5e18214c200ca.jpg', 'uploads/import/image_750x415_5e18214e1c42d.jpg', 'uploads/import/image_100x75_5e18214f832b5.jpg', 'uploads/import/image_650x433_5e182150bf8fd.jpg', 'jpg', 0, 0, 16, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-10 07:01:38'),
(34, 2, '4U - NEW LAUNCHING - CHỈ TỪ 99K', '4u-new-launching-chi-tu-99k', NULL, '4U - NEW LAUNCHING - CHỈ TỪ 99K', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"21/01/2020\";s:11:\"coupon_note\";s:0:\"\";s:14:\"affiliate_link\";s:116:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.yes24.vn%2F4u--new-launching-km743410.html\";}', 'mã giảm giá yes24, voucher giảm giá yes24, coupon yes24, voucher giảm giá yes24 năm 2020', 1, 29, 'uploads/import/image_750x_5e1821525aab2.jpg', 'uploads/import/image_750x415_5e1821536299f.jpg', 'uploads/import/image_100x75_5e1821541903e.jpg', 'uploads/import/image_650x433_5e182154a2a07.jpg', 'jpg', 0, 0, 15, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-10 07:01:41'),
(35, 2, 'Chợ phiên tý vàng - Deal ngon giá khủng', 'cho-phien-ty-vang-deal-ngon-gia-khung', NULL, '⭐️Ngày 8/1-9/1:                                                \r\n+ Nhập TYVANG8 giảm 8% tất cả sản phẩm                                                \r\n+ Nhập TYVANG10 giảm 10% cho ĐH từ 500K                                                \r\n+Nhập TYVANG12 giảm 12%  cho ĐH từ 1 triệu                                                \r\n⭐️Ngày 10/1-12/1:                                                \r\n+ Mã CHOTET50 giảm 50K cho ĐH từ 500K                                                 \r\n+ Mã CHOTET100 giảm 100K cho ĐH từ 1,000K                                                \r\n+  Mã SPEED10 giảm 10% ĐH từ 300K sản phẩm giao nhanh', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"16/01/2020\";s:11:\"coupon_note\";s:0:\"\";s:14:\"affiliate_link\";s:106:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fevent.yes24.vn%2Fdeal-ngon-gia-khung\";}', 'mã giảm giá yes24, voucher giảm giá yes24, coupon yes24, voucher giảm giá yes24 năm 2020', 1, 29, 'uploads/import/image_750x_5e1821559d829.jpg', 'uploads/import/image_750x415_5e1821564ae46.jpg', 'uploads/import/image_100x75_5e182156d2b2e.jpg', 'uploads/import/image_650x433_5e18215743e79.jpg', 'jpg', 0, 0, 14, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-10 07:01:43'),
(36, 2, 'Giảm 150K khi mua combo 2 thùng 24 lon Budweiser 330ml 840K', 'giam-150k-khi-mua-combo-2-thung-24-lon-budweiser-330ml-840k', NULL, '<p>Nhập m&atilde; M&atilde; giảm gi&aacute; để được giảm 60K khi mua 1 th&ugrave;ng 24 lon Budweiser 330ml 420K (Sau khi &aacute;p dụng c&ograve;n 360K. Mỗi kh&aacute;ch h&agrave;ng chỉ được sử dung 1 lần, tối đa 2 sản phẩm/ 1 lần v&agrave; code được d&ugrave;ng 1000 lần)</p>', 'a:4:{s:11:\"coupon_code\";s:6:\"AT150K\";s:11:\"coupon_date\";s:10:\"15/01/2020\";s:11:\"coupon_note\";s:0:\"\";s:14:\"affiliate_link\";s:125:\"https://go.isclix.com/deep_link/5229553935641796647?url=https://vuabia.com/combo-2-thung-budweiser-lon-330ml-thung-24-AT.html\";}', '', 1, 16, 'uploads/import/image_750x_5e183f6c372b0.jpg', 'uploads/import/image_750x415_5e183f6c83a4d.jpg', 'uploads/import/image_100x75_5e183f6cd81ae.jpg', 'uploads/import/image_650x433_5e183f6d15876.jpg', 'jpg', 0, 0, 17, 0, '', 'post', '', '', '', 0, 1, 1, '2020-01-10 09:10:05'),
(37, 2, 'Voucher khủng đến 500K', 'voucher-khung-den-500k', NULL, 'Voucher khủng đến 500K', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"13/01/2020\";s:11:\"coupon_note\";s:0:\"\";s:14:\"affiliate_link\";s:133:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.sendo.vn%2Fsu-kien%2Fvoucher%2F%3Fref%3Dhome_slider_desktop\";}', NULL, 1, 20, 'uploads/import/image_750x_5e1840255ac17.jpg', 'uploads/import/image_750x415_5e1840256ff71.jpg', 'uploads/import/image_100x75_5e18402590e33.jpg', 'uploads/import/image_650x433_5e18402593a07.jpg', 'jpg', 0, 0, 12, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-10 09:13:09'),
(38, 2, 'ƯU ĐÃI TẾT 2020 THEO NGÀNH HÀNG', 'uu-dai-tet-2020-theo-nganh-hang', NULL, 'Nhà cửa đời sống: Mua 1 tặng 1, săn deal 88K', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"12/01/2020\";s:11:\"coupon_note\";s:0:\"\";s:14:\"affiliate_link\";s:104:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.lazada.vn%2Flazada-tet-2020%2F\";}', NULL, 1, 30, 'uploads/import/image_750x_5e18413e1c7ff.jpg', 'uploads/import/image_750x415_5e18413e58a72.jpg', 'uploads/import/image_100x75_5e18413e96527.jpg', 'uploads/import/image_650x433_5e18413eb2f80.jpg', 'jpg', 0, 0, 12, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-10 09:17:50'),
(39, 2, 'Thời trang & Thể thao: Săn deal may mắn 8K', 'thoi-trang-the-thao-san-deal-may-man-8k', NULL, 'Thời trang & Thể thao: Săn deal may mắn 8K.\r\nMiễn phí vận chuyển.', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"12/01/2020\";s:11:\"coupon_note\";s:0:\"\";s:14:\"affiliate_link\";s:104:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.lazada.vn%2Flazada-tet-2020%2F\";}', NULL, 1, 30, 'uploads/import/image_750x_5e18413f28d9a.jpg', 'uploads/import/image_750x415_5e18413f69613.jpg', 'uploads/import/image_100x75_5e18413fa735a.jpg', 'uploads/import/image_650x433_5e18413fc52e2.jpg', 'jpg', 0, 0, 14, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-10 09:17:52'),
(40, 2, 'Bách hóa online - Đồng giá 88K', 'bach-hoa-online-dong-gia-88k', NULL, 'Bách hóa online - Đồng giá 88K', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"12/01/2020\";s:11:\"coupon_note\";s:0:\"\";s:14:\"affiliate_link\";s:104:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.lazada.vn%2Flazada-tet-2020%2F\";}', NULL, 1, 30, 'uploads/import/image_750x_5e1841404dba8.jpg', 'uploads/import/image_750x415_5e1841405c180.jpg', 'uploads/import/image_100x75_5e18414080623.jpg', 'uploads/import/image_650x433_5e18414087568.jpg', 'jpg', 0, 0, 12, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-10 09:17:52'),
(41, 2, 'Sức khỏe & làm đẹp: Bộ quà tặng chỉ từ 188K', 'suc-khoe-lam-dep-bo-qua-tang-chi-tu-188k', NULL, 'Sức khỏe & làm đẹp: Bộ quà tặng chỉ từ 188K + Miễn phí vận chuyển.', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"12/01/2020\";s:11:\"coupon_note\";s:0:\"\";s:14:\"affiliate_link\";s:104:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.lazada.vn%2Flazada-tet-2020%2F\";}', NULL, 1, 30, 'uploads/import/image_750x_5e184140d4c20.jpg', 'uploads/import/image_750x415_5e184140e2dee.jpg', 'uploads/import/image_100x75_5e18414113eda.jpg', 'uploads/import/image_650x433_5e1841411c23e.jpg', 'jpg', 0, 0, 10, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-10 09:17:53'),
(42, 2, 'Mẹ và bé: Voucher 200K', 'me-va-be-voucher-200k', NULL, 'Mẹ và bé: Voucher 200K + Siêu quà tặng đến 800K', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"12/01/2020\";s:11:\"coupon_note\";s:0:\"\";s:14:\"affiliate_link\";s:104:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.lazada.vn%2Flazada-tet-2020%2F\";}', NULL, 1, 30, 'uploads/import/image_750x_5e1841416c382.jpg', 'uploads/import/image_750x415_5e1841417d5ef.jpg', 'uploads/import/image_100x75_5e184141a9462.jpg', 'uploads/import/image_650x433_5e184141b2562.jpg', 'jpg', 0, 0, 16, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-10 09:17:53'),
(43, 2, 'CLICK TẾT VÀO NHÀ - KHUYẾN MÃI CỰC SỐC', 'click-tet-vao-nha-khuyen-mai-cuc-soc', NULL, '????0đ vận chuyển toàn quốc: Miễn phí vận chuyển đơn hàng cho mọi đơn hàng.\r\n????Thu thập voucher:\r\n- Voucher 888K cho đơn từ 6.8tr vào 12h trưa mỗi ngày.\r\n- Voucher miễn phí vận chuyển 50K cho đơn 199K, voucher 40K cho đơn 400K,... xuất hiện các khung giờ 0h - 9h - 12h - 16h - 20h.\r\n⚡️Deal chớp nhoáng/Deal huỷ diệt:\r\n- Khung giờ Deal Chớp nhoáng mỗi ngày từ 3-12/1: 00:00\r\n- Khung giờ Deal Huỷ Diệt mỗi ngày từ 3-12/1: 9:00 - 12:00; 12:00 - 16:00; 16:00 - 19:00; 19:00 - 21:00; 21:00 - 24:00', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"12/01/2020\";s:11:\"coupon_note\";s:0:\"\";s:14:\"affiliate_link\";s:104:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.lazada.vn%2Flazada-tet-2020%2F\";}', NULL, 1, 30, 'uploads/import/image_750x_5e1841421e378.jpg', 'uploads/import/image_750x415_5e1841425b351.jpg', 'uploads/import/image_100x75_5e1841429e25e.jpg', 'uploads/import/image_650x433_5e184142c0386.jpg', 'jpg', 0, 0, 17, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-10 09:17:55'),
(44, 2, 'Hot Deal, Flash sale - Voucher 400k cho đơn hàng từ 5 Triệu', 'hot-deal-flash-sale-voucher-400k-cho-don-hang-tu-5-trieu', NULL, 'Hot Deal, Flash sale - Voucher 400k cho đơn hàng từ 5 Triệu.', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"12/01/2020\";s:11:\"coupon_note\";s:0:\"\";s:14:\"affiliate_link\";s:104:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.lazada.vn%2Flazada-tet-2020%2F\";}', NULL, 1, 30, 'uploads/import/image_750x_5e1841433229c.jpg', 'uploads/import/image_750x415_5e1841436d4d7.jpg', 'uploads/import/image_100x75_5e184143a6402.jpg', 'uploads/import/image_650x433_5e184143c397e.jpg', 'jpg', 0, 0, 11, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-10 09:17:56'),
(45, 2, 'Masan khuyến mãi hấp dẫn', 'masan-khuyen-mai-hap-dan', NULL, 'Chiến dịch Tết Là Lá La vẫn còn kéo dài trong 3 ngày đến hết 12.01', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"12/01/2020\";s:11:\"coupon_note\";s:0:\"\";s:14:\"affiliate_link\";s:138:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fpages.lazada.vn%2Fwow%2Fcamp%2Flazada%2Fchannel%2Fvn%2Fmasan%2Fmasan\";}', NULL, 1, 30, 'uploads/import/image_750x_5e1841443c355.jpg', 'uploads/import/image_750x415_5e184144b3066.jpg', 'uploads/import/image_100x75_5e1841453726e.jpg', 'uploads/import/image_650x433_5e1841458db0d.jpg', 'jpg', 0, 0, 13, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-10 09:17:58'),
(46, 2, 'Shinhanbank - Ưu đãi dành riêng cho du lịch tự túc 2019', 'shinhanbank-uu-dai-danh-rieng-cho-du-lich-tu-tuc-2019', NULL, 'Giảm 150.000 đồng cho hóa đơn đầu tiên tại ứng dụng Klook có giá trị tối thiểu 1.500.000 đồng khi thanh toán bằng Thẻ do Ngân Hàng TNHH MTV Shinhan Việt Nam phát hành (Thẻ Tín Dụng, Thẻ Ghi Nợ, Thẻ Doanh Nghiệp).', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"31/08/2020\";s:11:\"coupon_note\";s:265:\"Giảm 150.000 đồng cho hóa đơn đầu tiên tại ứng dụng Klook có giá trị tối thiểu 1.500.000 đồng khi thanh toán bằng Thẻ do Ngân Hàng TNHH MTV Shinhan Việt Nam phát hành (Thẻ Tín Dụng, Thẻ Ghi Nợ, Thẻ Doanh Nghiệp).\";s:14:\"affiliate_link\";s:91:\"https://go.isclix.com/deep_link/5229553935641796647?url=http%3A%2F%2Fshinhanbank.vnfiba.com\";}', NULL, 1, 36, 'uploads/import/image_750x_5e1847cbe34af.jpg', 'uploads/import/image_750x415_5e1847cc7ae38.jpg', 'uploads/import/image_100x75_5e1847cce8338.jpg', 'uploads/import/image_650x433_5e1847cd3f118.jpg', 'jpg', 0, 0, 15, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-10 09:45:49'),
(47, 2, 'ƯU ĐÃI CHO NHIỀU MẶT HÀNG: SỮA, TÃ, ĐỒ CHƠI, THỜI TRANG', 'uu-dai-cho-nhieu-mat-hang-sua-ta-do-choi-thoi-trang', NULL, 'ĐỒ CHƠI GIẢM 50%.\r\nMUA TÃ HẾT MÌNH - NHẬN QUÀ HẾT Ý.', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"31/01/2020\";s:11:\"coupon_note\";s:68:\"ĐỒ CHƠI GIẢM 50%.\r\nMUA TÃ HẾT MÌNH - NHẬN QUÀ HẾT Ý.\";s:14:\"affiliate_link\";s:130:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fconcung.com%2Fthoi-trang-cf-uu-dai-ngay-50-hashtag-7629.html\";}', NULL, 1, 37, 'uploads/import/image_750x_5e192e2b38693.jpg', 'uploads/import/image_750x415_5e192e2bbd48f.jpg', 'uploads/import/image_100x75_5e192e2c1fe87.jpg', 'uploads/import/image_650x433_5e192e2c575e7.jpg', 'jpg', 0, 0, 13, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-11 02:08:44'),
(48, 2, 'REDDOORZ GIẢM GIÁ CUỐI NĂM', 'reddoorz-giam-gia-cuoi-nam', NULL, '<p>⏳Thời gian &aacute;p dụng: Từ 31/12/19 đến 30/1/2020. ???? Điều kiện &aacute;p dụng: Code: 30% apply all RedDoorz properties in Vietnam.</p>', 'a:4:{s:11:\"coupon_code\";s:7:\"RELAX30\";s:11:\"coupon_date\";s:10:\"30/01/2020\";s:11:\"coupon_note\";s:138:\"⏳Thời gian áp dụng: Từ 31/12/19 đến 30/1/2020.\r\nĐiều kiện áp dụng: Code: 30% apply all RedDoorz properties in Vietnam.\";s:14:\"affiliate_link\";s:110:\"https://go.isclix.com/deep_link/5229553935641796647?url=https://www.reddoorz.com/vi-vn/?utm_source=accesstrade\";}', '', 1, 21, 'uploads/import/image_750x_5e192e9a816dd.jpg', 'uploads/import/image_750x415_5e192e9aa43c9.jpg', 'uploads/import/image_100x75_5e192e9adae69.jpg', 'uploads/import/image_650x433_5e192e9aeb6aa.jpg', 'jpg', 0, 0, 14, 0, '', 'post', '', '', '', 0, 1, 1, '2020-01-11 02:10:35'),
(50, 2, 'END OF SEASON - SALE CÚ TRÓT 80%', 'end-of-season-sale-cu-trot-80', NULL, '⏰ Đặt lịch săn sale: 1 - 25/1/2020\r\n⏰ Áp dụng loạt Vali - Balo - Túi siêu Hot\r\n⏰ Số lượng có hạn, săn ngay kẻo lỡ!', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"25/01/2020\";s:11:\"coupon_note\";s:146:\"⏰ Đặt lịch săn sale: 1 - 25/1/2020\r\n⏰ Áp dụng loạt Vali - Balo - Túi siêu Hot\r\n⏰ Số lượng có hạn, săn ngay kẻo lỡ!\";s:14:\"affiliate_link\";s:79:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Flug.vn%2F\";}', NULL, 1, 38, 'uploads/import/image_750x_5e192f970328e.jpg', 'uploads/import/image_750x415_5e192f974dd8d.jpg', 'uploads/import/image_100x75_5e192f97a51fd.jpg', 'uploads/import/image_650x433_5e192f97d3584.jpg', 'jpg', 0, 0, 15, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-11 02:14:48'),
(51, 2, 'TẾT CÙNG BAMBOO - VI VU ĐẮC LỘC', 'tet-cung-bamboo-vi-vu-dac-loc', NULL, 'Từ 7/12/2019 - 28/02/2020, đặt vé bay Bamboo Airways dịp Xuân Canh Tý sẽ có cơ hội tham dự quay thưởng hàng tuần gồm hàng chục chiếc iphone 11 ProMax 64Gb, hàng trăm Voucher vé máy bay khứ hồi Nội địa & Quốc tế của Bamboo Airways.\r\nNgoài ra, còn được tham gia quay thưởng cuối kỳ đặc biệt vào ngày 29/02/2020 và có cơ hội nhận về:\r\n☘ Voucher 01 năm bay miễn phí (tất cả các chặng bay nội địa Bamboo Airways khai thác)\r\n☘ Voucher 50 đêm nghỉ miễn phí tại Hệ thống nghỉ dưỡng FLC\r\n☘ Xe ô tô Mazda CX5 2019 Premium', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"28/02/2020\";s:11:\"coupon_note\";s:633:\"Từ 7/12/2019 - 28/02/2020, đặt vé bay Bamboo Airways dịp Xuân Canh Tý sẽ có cơ hội tham dự quay thưởng hàng tuần gồm hàng chục chiếc iphone 11 ProMax 64Gb, hàng trăm Voucher vé máy bay khứ hồi Nội địa & Quốc tế của Bamboo Airways.\r\nNgoài ra, còn được tham gia quay thưởng cuối kỳ đặc biệt vào ngày 29/02/2020 và có cơ hội nhận về:\r\n☘ Voucher 01 năm bay miễn phí (tất cả các chặng bay nội địa Bamboo Airways khai thác)\r\n☘ Voucher 50 đêm nghỉ miễn phí tại Hệ thống nghỉ dưỡng FLC\r\n☘ Xe ô tô Mazda CX5 2019 Premium\";s:14:\"affiliate_link\";s:94:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Ftet.bambooairways.com%2F\";}', NULL, 1, 39, 'uploads/import/image_750x_5e19306b43c89.jpg', 'uploads/import/image_750x415_5e19306b53e33.jpg', 'uploads/import/image_100x75_5e19306b7c05c.jpg', 'uploads/import/image_650x433_5e19306b8306f.jpg', 'jpg', 0, 0, 11, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-11 02:18:19'),
(52, 2, 'SINH NHẬT WEBSITE MỞ NGÀN ƯU ĐÃI', 'sinh-nhat-website-mo-ngan-uu-dai', NULL, 'Mừng sinh nhật website https://bambooairways.com cùng tham gia “đại tiệc” tri ân khách hàng siêu chất: ưu đãi vàng của thứ Tư hạnh phúc sẽ kéo dài trong tận 5 ngày liên tiếp, từ ngày 08/01/2020 - 12/01/2020.\r\n???? 76 chặng bay nội địa và quốc tế sẽ có giá từ 49K*/vé\r\n(*) Giá vé 1 chiều; chưa bao gồm thuế, phí.', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"12/01/2020\";s:11:\"coupon_note\";s:385:\"Mừng sinh nhật website https://bambooairways.com cùng tham gia “đại tiệc” tri ân khách hàng siêu chất: ưu đãi vàng của thứ Tư hạnh phúc sẽ kéo dài trong tận 5 ngày liên tiếp, từ ngày 08/01/2020 - 12/01/2020.\r\n???? 76 chặng bay nội địa và quốc tế sẽ có giá từ 49K*/vé\r\n(*) Giá vé 1 chiều; chưa bao gồm thuế, phí.\";s:14:\"affiliate_link\";s:164:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.bambooairways.com%2Fvn-vi%2Fthong-tin-dat-ve%2Fuu-dai%2Fsinh-nhat-vang-rinh-ngan-uu-dai%2F\";}', NULL, 1, 39, 'uploads/import/image_750x_5e19306bdf816.jpg', 'uploads/import/image_750x415_5e19306c459aa.jpg', 'uploads/import/image_100x75_5e19306c902c9.jpg', 'uploads/import/image_650x433_5e19306cb8018.jpg', 'jpg', 0, 0, 11, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-11 02:18:21'),
(53, 2, 'TẾT SẮM ĐỒNG HỒ MỚI - LÌ XÌ TỚI 51%', 'tet-sam-dong-ho-moi-li-xi-toi-51', NULL, '???? Sale 8% đối với đồng hồ Orient, Seiko, Citizen, Elixa, FreeLook, Festina, SR Watch, Bulova.\r\n???? Sale 5% đối với đồng hồ OP, Ogival, Bentley.', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"31/01/2020\";s:11:\"coupon_note\";s:167:\"???? Sale 8% đối với đồng hồ Orient, Seiko, Citizen, Elixa, FreeLook, Festina, SR Watch, Bulova.\r\n???? Sale 5% đối với đồng hồ OP, Ogival, Bentley.\";s:14:\"affiliate_link\";s:97:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fxwatch.vn%2Fldp-tet-2020%2F\";}', NULL, 1, 40, 'uploads/import/image_750x_5e193c620fcbd.jpg', 'uploads/import/image_750x415_5e193c625168e.jpg', 'uploads/import/image_100x75_5e193c629ad69.jpg', 'uploads/import/image_650x433_5e193c62c5e22.jpg', 'jpg', 0, 1, 18, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-11 03:09:23'),
(54, 2, 'Vé JR Pass Toàn Nhật Bản 7 Ngày Và SIM 4G Nhật Bản', 've-jr-pass-toan-nhat-ban-7-ngay-va-sim-4g-nhat-ban', NULL, '✈️Ưu đãi giới hạn: Mua ngay gói combo tuyệt vời gồm JR Pass Toàn Nhật Bản 7 ngày cùng SIM 4G Nhật Bản này\r\n✈️Tận hưởng việc đi lại trên đa số các tuyến đường sắt JR và xe buýt địa phương để đến mọi nơi một cách dễ dàng\r\n✈️Bạn có thể chọn nhận SIM tại Tokyo hoặc Osaka\r\n✈️Tận hưởng gói dữ liệu internet nhanh chóng, hiệu quả với SIM 4G từ nhà mạng viễn thông nổi bật ở Nhật\r\n\r\nGía chỉ còn: 6,191,521\r\nNgày sớm nhất có hiệu lực: 15/12/2019', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"31/01/2020\";s:11:\"coupon_note\";s:584:\"✈️Ưu đãi giới hạn: Mua ngay gói combo tuyệt vời gồm JR Pass Toàn Nhật Bản 7 ngày cùng SIM 4G Nhật Bản này\r\n✈️Tận hưởng việc đi lại trên đa số các tuyến đường sắt JR và xe buýt địa phương để đến mọi nơi một cách dễ dàng\r\n✈️Bạn có thể chọn nhận SIM tại Tokyo hoặc Osaka\r\n✈️Tận hưởng gói dữ liệu internet nhanh chóng, hiệu quả với SIM 4G từ nhà mạng viễn thông nổi bật ở Nhật\r\n\r\nGía chỉ còn: 6,191,521\r\nNgày sớm nhất có hiệu lực: 15/12/2019\";s:14:\"affiliate_link\";s:155:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.klook.com%2Fvi%2Factivity%2F20279-7-day-whole-japan-4G-sim-tokyo-combo-jr-pass%2F\";}', NULL, 1, 41, 'uploads/import/image_750x_5e193d22cdbcf.jpg', 'uploads/import/image_750x415_5e193d233588d.jpg', 'uploads/import/image_100x75_5e193d2392ca3.jpg', 'uploads/import/image_650x433_5e193d23c9c05.jpg', 'jpg', 0, 0, 13, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-11 03:12:36');
INSERT INTO `posts` (`id`, `lang_id`, `title`, `title_slug`, `summary`, `content`, `coupon_options`, `keywords`, `user_id`, `category_id`, `image_big`, `image_mid`, `image_small`, `image_slider`, `image_mime`, `is_slider`, `is_picked`, `hit`, `slider_order`, `optional_url`, `post_type`, `video_url`, `video_embed_code`, `image_url`, `need_auth`, `visibility`, `status`, `created_at`) VALUES
(55, 2, '[KLOOK] TẾT 2020 GIẢM GIÁ 5%', 'klook-tet-2020-giam-gia-5', NULL, '<p>Giảm 5% đến 200,000 VND cho đơn h&agrave;ng từ 450,000 VND. Bắt đầu từ 01.01 đến 09.02.2020. cả web &amp; App.</p>', 'a:4:{s:11:\"coupon_code\";s:6:\"TET202\";s:11:\"coupon_date\";s:10:\"09/02/2020\";s:11:\"coupon_note\";s:133:\"Tết 2020 giảm 5% đến 200,000 VND cho đơn hàng từ 450,000 VND.\r\nBắt đầu từ 01.01 đến 09.02.2020. cả web & App.\";s:14:\"affiliate_link\";s:81:\"https://go.isclix.com/deep_link/5229553935641796647?url=https://www.klook.com/vi/\";}', '', 1, 41, 'uploads/import/image_750x_5e193d2451f10.jpg', 'uploads/import/image_750x415_5e193d247b184.jpg', 'uploads/import/image_100x75_5e193d24b2bff.jpg', 'uploads/import/image_650x433_5e193d24c99b9.jpg', 'jpg', 0, 1, 17, 0, '', 'post', '', '', '', 0, 1, 1, '2020-01-11 03:12:37'),
(56, 2, '[KLOOK] TẾT CANH TÝ 2020 GIẢM GIÁ 10%', 'klook-tet-canh-ty-2020-giam-gia-10', NULL, '<p>Giảm ngay 10% l&ecirc;n đến 300,000 VND cho đơn h&agrave;ng từ 1,200,000 VND. Bắt đầu &aacute;p dụng từ 01.01 đến 09.02.2020. cả web &amp; App.</p>', 'a:4:{s:11:\"coupon_code\";s:8:\"CANHTY20\";s:11:\"coupon_date\";s:10:\"09/02/2020\";s:11:\"coupon_note\";s:146:\"Giảm ngay 10% lên đến 300,000 VND cho đơn hàng từ 1,200,000 VND.\r\nBắt đầu áp dụng từ 01.01 đến 09.02.2020. cả web & App.\";s:14:\"affiliate_link\";s:81:\"https://go.isclix.com/deep_link/5229553935641796647?url=https://www.klook.com/vi/\";}', '', 1, 41, 'uploads/import/image_750x_5e193d253eba3.jpg', 'uploads/import/image_750x415_5e193d2571efb.jpg', 'uploads/import/image_100x75_5e193d25aa4e6.jpg', 'uploads/import/image_650x433_5e193d25bce40.jpg', 'jpg', 0, 1, 16, 0, '', 'post', '', '', '', 0, 1, 1, '2020-01-11 03:12:37'),
(57, 2, 'Kỳ nghỉ du xuân đón Tết Canh Tý tại Vinpearl - Giá chỉ từ 1.500.000 VNĐ/người', 'ky-nghi-du-xuan-don-tet-canh-ty-tai-vinpearl-gia-chi-tu-1500000-vndnguoi', NULL, 'Ưu đãi Kỳ nghỉ cao cấp giá tốt nhất năm trong giai đoạn chạy thử website Vinpearltravel.\r\n- Giá chỉ từ 1.500.000 VNĐ/khách/đêm tại các điểm du lịch hot nhất cả nước.\r\n- Cơ hội bốc thăm trúng thưởng gói nghỉ dưỡng thượng hạng tại Vinpearl Phú Quốc trị giá 100.000.000 VNĐ. khi đặt phòng trên Vinpearl.com và sản phẩm bất kỳ tại Vinpearltravel.vn', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"29/02/2020\";s:11:\"coupon_note\";s:436:\"Ưu đãi Kỳ nghỉ cao cấp giá tốt nhất năm trong giai đoạn chạy thử website Vinpearltravel.\r\n- Giá chỉ từ 1.500.000 VNĐ/khách/đêm tại các điểm du lịch hot nhất cả nước.\r\n- Cơ hội bốc thăm trúng thưởng gói nghỉ dưỡng thượng hạng tại Vinpearl Phú Quốc trị giá 100.000.000 VNĐ. khi đặt phòng trên Vinpearl.com và sản phẩm bất kỳ tại Vinpearltravel.vn\";s:14:\"affiliate_link\";s:280:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.vinpearltravel.vn%2Fblog%2Fra-mat-nen-tang-dat-phong-khach-san-dich-vu-du-lich-truc-tuyen-vinpearl-travel-co-hoi-trai-nghiem-3n2d-lam-tong-thong-tai-vinpearl-khi-dat-mua-tren-vinpearltravelvn-va-vinpearlcom\";}', NULL, 1, 42, 'uploads/import/image_750x_5e1945a1468b8.jpg', 'uploads/import/image_750x415_5e1945a1aacd4.jpg', 'uploads/import/image_100x75_5e1945a20f9bc.jpg', 'uploads/import/image_650x433_5e1945a23f1c5.jpg', 'jpg', 0, 0, 16, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-11 03:48:50'),
(58, 2, 'Kỳ nghỉ thượng lưu - Giá cực tối ưu với Combo nghỉ dưỡng trọn gói thông minh dành cho Gia đình Tết nguyên đán', 'ky-nghi-thuong-luu-gia-cuc-toi-uu-voi-combo-nghi-duong-tron-goi-thong-minh-danh-cho-gia-dinh-tet-nguyen-dan', NULL, 'Trọn gói nghỉ dưỡng 2N1Đ/3N2Đ tiết kiệm tại Vinpearl Hội An - Đà Nẵng - Nha Trang - Phú Quốc cho gia đình, trọn gói di chuyển, lưu trú và ăn sáng, giá chỉ từ 1.500.000vnđ/khách', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"29/02/2020\";s:11:\"coupon_note\";s:220:\"Trọn gói nghỉ dưỡng 2N1Đ/3N2Đ tiết kiệm tại Vinpearl Hội An - Đà Nẵng - Nha Trang - Phú Quốc cho gia đình, trọn gói di chuyển, lưu trú và ăn sáng, giá chỉ từ 1.500.000vnđ/khách\";s:14:\"affiliate_link\";s:110:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.vinpearltravel.vn%2Fvoucher%2Fsearch\";}', NULL, 1, 42, 'uploads/import/image_750x_5e1945a2bd258.jpg', 'uploads/import/image_750x415_5e1945a325a1a.jpg', 'uploads/import/image_100x75_5e1945a37e412.jpg', 'uploads/import/image_650x433_5e1945a3ac441.jpg', 'jpg', 0, 0, 13, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-11 03:48:52'),
(59, 2, 'Tour trọn gói du xuân hành hương lễ Phật cầu an tại Hải Phòng - Hà Nam - Thanh Hóa - Tây Ninh Tết Nguyên đán', 'tour-tron-goi-du-xuan-hanh-huong-le-phat-cau-an-tai-hai-phong-ha-nam-thanh-hoa-tay-ninh-tet-nguyen-dan', NULL, 'Trọn gói xe đưa đón 2 chiều từ HN và HCM, phòng nghỉ 5 sao và tour tham quan các địa danh nổi tiếng nhất tại địa phương, giá sốc chỉ từ 2.431.500đ/khách', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"29/02/2020\";s:11:\"coupon_note\";s:194:\"Trọn gói xe đưa đón 2 chiều từ HN và HCM, phòng nghỉ 5 sao và tour tham quan các địa danh nổi tiếng nhất tại địa phương, giá sốc chỉ từ 2.431.500đ/khách\";s:14:\"affiliate_link\";s:110:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.vinpearltravel.vn%2Fvoucher%2Fsearch\";}', NULL, 1, 42, 'uploads/import/image_750x_5e1945a431613.jpg', 'uploads/import/image_750x415_5e1945a48863b.jpg', 'uploads/import/image_100x75_5e1945a4d55ae.jpg', 'uploads/import/image_650x433_5e1945a50c993.jpg', 'jpg', 0, 0, 15, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-11 03:48:53'),
(60, 2, 'Kỳ nghỉ dưỡng biệt thự biển đẳng cấp Vinpearl dành cho Đại gia đình, kết hợp tour tham quan địa danh tại Đà Nẵng - Hội An - Nha Trang - Phú Quốc', 'ky-nghi-duong-biet-thu-bien-dang-cap-vinpearl-danh-cho-dai-gia-dinh-ket-hop-tour-tham-quan-dia-danh-tai-da-nang-hoi-an-nha-trang-phu-quoc', NULL, 'Trọn gói du xuân đại gia đình bao gồm 3N2Đ nghỉ dưỡng tại biệt thự biển 5 sao, buffet 3 bữa, tour trọn gói di chuyển tham quan địa danh nổi tiếng và các dịch vụ cao cấp nhất tại cơ sở (Massage, chơi golf...). Giá chỉ 2.370.000 VNĐ/người/đêm', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"29/02/2020\";s:11:\"coupon_note\";s:304:\"Trọn gói du xuân đại gia đình bao gồm 3N2Đ nghỉ dưỡng tại biệt thự biển 5 sao, buffet 3 bữa, tour trọn gói di chuyển tham quan địa danh nổi tiếng và các dịch vụ cao cấp nhất tại cơ sở (Massage, chơi golf...). Giá chỉ 2.370.000 VNĐ/người/đêm\";s:14:\"affiliate_link\";s:110:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.vinpearltravel.vn%2Fvoucher%2Fsearch\";}', NULL, 1, 42, 'uploads/import/image_750x_5e1945a58709b.jpg', 'uploads/import/image_750x415_5e1945a5e81b7.jpg', 'uploads/import/image_100x75_5e1945a64965f.jpg', 'uploads/import/image_650x433_5e1945a677b95.jpg', 'jpg', 0, 0, 10, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-11 03:48:54'),
(61, 2, 'Mừng Xuân Đang Tới - Đón Ngay Sale Mới', 'mung-xuan-dang-toi-don-ngay-sale-moi', NULL, 'GALAXY NOTE 10 SERIES SALE SỐC CHỈ TỪ 16.990.000Đ', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"15/01/2020\";s:11:\"coupon_note\";s:56:\"GALAXY NOTE 10 SERIES SALE SỐC CHỈ TỪ 16.990.000Đ\";s:14:\"affiliate_link\";s:123:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fhoanghamobile.com%2Fsamsung-galaxy-note-10-c2522.html\";}', NULL, 1, 43, 'uploads/import/image_750x_5e194721273e8.jpg', 'uploads/import/image_750x415_5e1947218b5ce.jpg', 'uploads/import/image_100x75_5e194721ed49f.jpg', 'uploads/import/image_650x433_5e19472238853.jpg', 'jpg', 0, 1, 20, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-11 03:55:14'),
(62, 2, 'Galaxy A51 Sập Giá Chỉ Còn Hơn 6 Triệu Đồng', 'galaxy-a51-sap-gia-chi-con-hon-6-trieu-dong', NULL, 'Mua ngay #Galaxy_A51 tại #Hoàng_Hà_Mobile giá chỉ còn hơn 6 triệu thông qua Galaxy Gift dành cho khách sở hữu : Galaxy J7 Prime và J7 pro.\r\n???? Cơ hội trúng ngay đồng tiền vàng 9999 trị giá 12 triệu.\r\n???? Trả góp lãi suất 0%.\r\n???? Miễn phí giao hàng toàn quốc.', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"31/01/2020\";s:11:\"coupon_note\";s:310:\"Mua ngay #Galaxy_A51 tại #Hoàng_Hà_Mobile giá chỉ còn hơn 6 triệu thông qua Galaxy Gift dành cho khách sở hữu : Galaxy J7 Prime và J7 pro.\r\n???? Cơ hội trúng ngay đồng tiền vàng 9999 trị giá 12 triệu.\r\n???? Trả góp lãi suất 0%.\r\n???? Miễn phí giao hàng toàn quốc.\";s:14:\"affiliate_link\";s:299:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fhoanghamobile.com%2Fsamsung-galaxy-a51-chinh-hang-p16390.html%3Futm_source%3Dfb%26utm_medium%3Dfba%26utm_content%3D3112_GalaxyA51%26utm_campaign%3DGalaxyA51%26fbclid%3DIwAR20nmzPQv0cNEvSL-_Ukl8B90aq_w6xVuZFtn-Hp4GN-fZOeamlVaQjyLc\";}', NULL, 1, 43, 'uploads/import/image_750x_5e194722d0d34.jpg', 'uploads/import/image_750x415_5e19472343869.jpg', 'uploads/import/image_100x75_5e194723a3bfb.jpg', 'uploads/import/image_650x433_5e194723dde7a.jpg', 'jpg', 0, 1, 21, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-11 03:55:16'),
(63, 2, 'Quà sung túc, Tết sum vầy', 'qua-sung-tuc-tet-sum-vay', NULL, 'Quà sung túc, Tết sum vầy', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"16/01/2020\";s:11:\"coupon_note\";s:31:\"Quà sung túc, Tết sum vầy\";s:14:\"affiliate_link\";s:128:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fshop.vnexpress.net%2Fevent%2Fqua-sung-tuc-tet-sum-vay.html\";}', NULL, 1, 44, 'uploads/import/image_750x_5e1be362bf24a.jpg', 'uploads/import/image_750x415_5e1be36319519.jpg', 'uploads/import/image_100x75_5e1be3637f3cf.jpg', 'uploads/import/image_650x433_5e1be363c1a0f.jpg', 'jpg', 0, 0, 17, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-13 03:26:28'),
(64, 2, 'Đồng hồ cao cấp - Giảm đến 50%', 'dong-ho-cao-cap-giam-den-50', NULL, 'Đồng hồ cao cấp - Giảm đến 50%', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"16/01/2020\";s:11:\"coupon_note\";s:42:\"Đồng hồ cao cấp - Giảm đến 50%\";s:14:\"affiliate_link\";s:143:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fshop.vnexpress.net%2Fretail%2Fdong-ho-cao-cap%2Fpage%2F1.html%3Fpin%3D759\";}', NULL, 1, 44, 'uploads/import/image_750x_5e1be3645d691.jpg', 'uploads/import/image_750x415_5e1be36492b6c.jpg', 'uploads/import/image_100x75_5e1be364d6303.jpg', 'uploads/import/image_650x433_5e1be365012e8.jpg', 'jpg', 0, 0, 16, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-13 03:26:29'),
(65, 2, 'Sản phẩm về tinh dầu - Ưu đãi lên đến 50%', 'san-pham-ve-tinh-dau-uu-dai-len-den-50', NULL, 'Sản phẩm về tinh dầu - Ưu đãi lên đến 50%', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"16/01/2020\";s:11:\"coupon_note\";s:56:\"Sản phẩm về tinh dầu - Ưu đãi lên đến 50%\";s:14:\"affiliate_link\";s:137:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fshop.vnexpress.net%2Fretail%2Florganic%2F%3F%26orderby%3Dprice_desc\";}', NULL, 1, 44, 'uploads/import/image_750x_5e1be3656e9df.jpg', 'uploads/import/image_750x415_5e1be365a51fb.jpg', 'uploads/import/image_100x75_5e1be365e94bb.jpg', 'uploads/import/image_650x433_5e1be366199f3.jpg', 'jpg', 0, 0, 16, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-13 03:26:30'),
(69, 2, 'THỨ 4 VUI VẺ - NGÀY HỘI FREESHIP', 'thu-4-vui-ve-ngay-hoi-freeship', NULL, '???? Nhận Hàng Trong 4h.\r\n????Freeshipping Toàn Quốc cho đơn hàng 149k.\r\n????Freeshipping Nội Thành HN HCM cho đơn hàng 99k.', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"31/01/2020\";s:11:\"coupon_note\";s:139:\"???? Nhận Hàng Trong 4h.\r\n????Freeshipping Toàn Quốc cho đơn hàng 149k.\r\n????Freeshipping Nội Thành HN HCM cho đơn hàng 99k.\";s:14:\"affiliate_link\";s:268:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fpages.lazada.vn%2Fwow%2Fcamp%2Flazada%2Fdailycampaign%2Fvn%2F0120C0A3-Happy-Wednesday-15-Jan%2Fngay-hoi-freeship%3Fhybrid%3D1%26fbclid%3DIwAR0rSDbT8aO9HfZ4JKb6Qfkdnu0wuaSD9WJmIUP9gXEIALA29Q5fBjqKQZU\";}', NULL, 1, 30, 'uploads/import/image_750x_5e227eb34110e.jpg', 'uploads/import/image_750x415_5e227eb379645.jpg', 'uploads/import/image_100x75_5e227eb3afed2.jpg', 'uploads/import/image_650x433_5e227eb3c448e.jpg', 'jpg', 0, 0, 20, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-18 03:42:44'),
(70, 2, 'Chương trình tết 2020 cực kỳ hấp dẫn', 'chuong-trinh-tet-2020-cuc-ky-hap-dan', NULL, 'Giảm đến 50%.\r\nLì xì thêm 20% sản phẩm thứ hai.\r\nLì xì đến 5 triệu khi thanh toán Mastercard.', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"24/01/2020\";s:11:\"coupon_note\";s:115:\"Giảm đến 50%.\r\nLì xì thêm 20% sản phẩm thứ hai.\r\nLì xì đến 5 triệu khi thanh toán Mastercard.\";s:14:\"affiliate_link\";s:129:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.nguyenkim.com%2Fnang-cap-tien-nghi-mung-xuan-nhu-y.html\";}', NULL, 1, 45, 'uploads/import/image_750x_5e227efb2301c.jpg', 'uploads/import/image_750x415_5e227efb32524.jpg', 'uploads/import/image_100x75_5e227efb56165.jpg', 'uploads/import/image_650x433_5e227efb5e5ad.jpg', 'jpg', 0, 0, 19, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-18 03:43:55'),
(71, 2, 'Nâng cấp tiện nghi - Mừng xuân như ý', 'nang-cap-tien-nghi-mung-xuan-nhu-y', NULL, 'Lì xì thêm 20% khi mua sản phẩm thứ 2', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"24/01/2020\";s:11:\"coupon_note\";s:46:\"Lì xì thêm 20% khi mua sản phẩm thứ 2\";s:14:\"affiliate_link\";s:136:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.nguyenkim.com%2Fmung-xuan-giam-them-20-cho-san-pham-thu-2.html\";}', NULL, 1, 45, 'uploads/import/image_750x_5e227efbaf324.jpg', 'uploads/import/image_750x415_5e227efbe8b68.jpg', 'uploads/import/image_100x75_5e227efc3a8d0.jpg', 'uploads/import/image_650x433_5e227efc5ba14.jpg', 'jpg', 0, 0, 9, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-18 03:43:56'),
(72, 2, 'Mua tivi tặng ngay bộ quà hấp dẫn', 'mua-tivi-tang-ngay-bo-qua-hap-dan', NULL, 'Mua tivi tặng ngay bộ quà hấp dẫn', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"28/02/2020\";s:11:\"coupon_note\";s:42:\"Mua tivi tặng ngay bộ quà hấp dẫn\";s:14:\"affiliate_link\";s:150:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.nguyenkim.com%2Ftivi-lcd-samsung%2F%3Fsort_by%3Dposition%26sort_order%3Ddesc\";}', NULL, 1, 45, 'uploads/import/image_750x_5e227efcc43ce.jpg', 'uploads/import/image_750x415_5e227efd0b59a.jpg', 'uploads/import/image_100x75_5e227efd508a2.jpg', 'uploads/import/image_650x433_5e227efd713f7.jpg', 'jpg', 0, 0, 13, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-18 03:43:57'),
(73, 2, 'Nhà đẹp xịn sang - Khang trang đón tết', 'nha-dep-xin-sang-khang-trang-don-tet', NULL, 'Tặng phiếu mua hàng trị giá 200K, áp dụng cho các đơn hàng từ 10 Triệu đồng.', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"19/01/2020\";s:11:\"coupon_note\";s:98:\"Tặng phiếu mua hàng trị giá 200K, áp dụng cho các đơn hàng từ 10 Triệu đồng.\";s:14:\"affiliate_link\";s:131:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.nguyenkim.com%2Fnha-dep-xin-sang-khang-trang-don-tet.html\";}', NULL, 1, 45, 'uploads/import/image_750x_5e227efddf30a.jpg', 'uploads/import/image_750x415_5e227efe28a34.jpg', 'uploads/import/image_100x75_5e227efe6f7e9.jpg', 'uploads/import/image_650x433_5e227efe9109f.jpg', 'jpg', 0, 0, 17, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-18 03:43:58'),
(74, 2, 'Điện thoại Samsung - Lì xì ngay 100K + Galaxy gift đến 5 Triệu đồng', 'dien-thoai-samsung-li-xi-ngay-100k-galaxy-gift-den-5-trieu-dong', NULL, 'Điện thoại Samsung - Lì xì ngay 100K + Galaxy gift đến 5 Triệu đồng', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"19/01/2020\";s:11:\"coupon_note\";s:82:\"Điện thoại Samsung - Lì xì ngay 100K + Galaxy gift đến 5 Triệu đồng\";s:14:\"affiliate_link\";s:160:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.nguyenkim.com%2Fdien-thoai-di-dong-samsung%2F%3Fsort_by%3Dposition%26sort_order%3Ddesc\";}', NULL, 1, 45, 'uploads/import/image_750x_5e227eff0a283.jpg', 'uploads/import/image_750x415_5e227eff445db.jpg', 'uploads/import/image_100x75_5e227eff86f5c.jpg', 'uploads/import/image_650x433_5e227effaa8e3.jpg', 'jpg', 0, 0, 14, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-01-18 03:43:59'),
(75, 2, 'Giày túi style Hàn Quốc - giảm đến 30%', 'giay-tui-style-han-quoc-giam-den-30', NULL, 'Giày túi style Hàn Quốc - giảm đến 30%', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"18/02/2020\";s:11:\"coupon_note\";s:48:\"Giày túi style Hàn Quốc - giảm đến 30%\";s:14:\"affiliate_link\";s:125:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.yes24.vn%2Fgiay-tui-the-thao-han-quoc-km567850.html\";}', NULL, 1, 26, 'uploads/import/image_750x_5e4205969396f.jpg', 'uploads/import/image_750x415_5e420596cfb02.jpg', 'uploads/import/image_100x75_5e42059723efa.jpg', 'uploads/import/image_650x433_5e42059747e90.jpg', 'jpg', 0, 0, 4, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-02-11 01:38:31'),
(76, 2, 'NIKE chính hãng khuyến mãi 50%', 'nike-chinh-hang-khuyen-mai-50', NULL, 'NIKE chính hãng khuyến mãi 50%', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"15/02/2020\";s:11:\"coupon_note\";s:35:\"NIKE chính hãng khuyến mãi 50%\";s:14:\"affiliate_link\";s:128:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.yes24.vn%2Fnike-chinh-hang-khuyen-mai-50-km670720.html\";}', NULL, 1, 26, 'uploads/import/image_750x_5e420597d0166.jpg', 'uploads/import/image_750x415_5e420598ba4ff.jpg', 'uploads/import/image_100x75_5e4205997b4a2.jpg', 'uploads/import/image_650x433_5e42059a101d8.jpg', 'jpg', 0, 0, 9, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-02-11 01:38:34'),
(77, 2, 'MỪNG VALENTINE FLASH SALE 142K', 'mung-valentine-flash-sale-142k', NULL, 'Nhập mã các ngành hàng sau giảm thêm 14%: THOITRANG, PHUKIEN, MEBE, GIAYTUI, THETHAO, NOIY', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"12/02/2020\";s:11:\"coupon_note\";s:99:\"Nhập mã các ngành hàng sau giảm thêm 14%: THOITRANG, PHUKIEN, MEBE, GIAYTUI, THETHAO, NOIY\";s:14:\"affiliate_link\";s:119:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fwww.yes24.vn%2Fvalentine-super-deal-km630730.html\";}', NULL, 1, 26, 'uploads/import/image_750x_5e42059b09fc1.jpg', 'uploads/import/image_750x415_5e42059b5232c.jpg', 'uploads/import/image_100x75_5e42059b9a3fa.jpg', 'uploads/import/image_650x433_5e42059bbd534.jpg', 'jpg', 1, 0, 4, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-02-11 01:38:36'),
(78, 2, 'BE MY VALENTINE - DUY NHẤT NGÀY 11/02', 'be-my-valentine-duy-nhat-ngay-1102', NULL, 'Mã VLT10 giảm 10% cho đơn hàng Mỹ Phẩm bất kỳ (tối đa 100k)\r\n+ Mã VLT12 giảm 12% cho đơn hàng Mỹ Phẩm từ 800k (tối đa 100k)\r\n+ Ưu đãi từ 8 thương hiệu HOT (mã 15%)\r\n+ List sản phẩm ILOVEU giảm thêm 12%', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"11/02/2020\";s:11:\"coupon_note\";s:250:\"Mã VLT10 giảm 10% cho đơn hàng Mỹ Phẩm bất kỳ (tối đa 100k)\r\n+ Mã VLT12 giảm 12% cho đơn hàng Mỹ Phẩm từ 800k (tối đa 100k)\r\n+ Ưu đãi từ 8 thương hiệu HOT (mã 15%)\r\n+ List sản phẩm ILOVEU giảm thêm 12%\";s:14:\"affiliate_link\";s:102:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fevent.yes24.vn%2Fbe-my-valentine\";}', NULL, 1, 26, 'uploads/import/image_750x_5e42059c37d3a.jpg', 'uploads/import/image_750x415_5e42059c873bc.jpg', 'uploads/import/image_100x75_5e42059cdc92f.jpg', 'uploads/import/image_650x433_5e42059d0e3b3.jpg', 'jpg', 1, 0, 4, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-02-11 01:38:37'),
(79, 2, 'THÍNH SALE NGÀY YÊU', 'thinh-sale-ngay-yeu', NULL, 'Ngày 11,12/2: Giá ưu đãi cho Member Shaphire, Ruby, Diamond\r\n+ Nhập mã LOVE10 giảm 10% cho tất cả đơn hàng.\r\n+ Nhập mã LOVE12 giảm 12% cho ĐH >800K.\r\n+ List SP giảm 18% - mã LOVE18 (share MK 12% - MD 6%).\r\nMỗi ID chỉ sử dụng mã 2 lần.\r\nNgày 13,14/2: Ưu đãi Valentine:\r\n+ Mã SWEET8 giảm 8% cho tất cả đơn hàng  \r\n+ Mã SWEET10 giảm 10% cho đơn hàng từ 500K\r\n+ Mã SWEET12 giảm 12% cho đơn hàng từ 1,000,000đ\r\n+ Mã giảm giá tối đa 120k/ đơn hàng\r\n+Top 100 sản phẩm QUÀ TẶNG nhập mã VALEN12 giảm thêm 12%. Giới hạn số lượng', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"14/02/2020\";s:11:\"coupon_note\";s:618:\"Ngày 11,12/2: Giá ưu đãi cho Member Shaphire, Ruby, Diamond\r\n+ Nhập mã LOVE10 giảm 10% cho tất cả đơn hàng.\r\n+ Nhập mã LOVE12 giảm 12% cho ĐH >800K.\r\n+ List SP giảm 18% - mã LOVE18 (share MK 12% - MD 6%).\r\nMỗi ID chỉ sử dụng mã 2 lần.\r\nNgày 13,14/2: Ưu đãi Valentine:\r\n+ Mã SWEET8 giảm 8% cho tất cả đơn hàng  \r\n+ Mã SWEET10 giảm 10% cho đơn hàng từ 500K\r\n+ Mã SWEET12 giảm 12% cho đơn hàng từ 1,000,000đ\r\n+ Mã giảm giá tối đa 120k/ đơn hàng\r\n+Top 100 sản phẩm QUÀ TẶNG nhập mã VALEN12 giảm thêm 12%. Giới hạn số lượng\";s:14:\"affiliate_link\";s:102:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fevent.yes24.vn%2Fmembership-show\";}', NULL, 1, 26, 'uploads/import/image_750x_5e42059d817b9.jpg', 'uploads/import/image_750x415_5e42059dc71b8.jpg', 'uploads/import/image_100x75_5e42059e263bf.jpg', 'uploads/import/image_650x433_5e42059e5f3b3.jpg', 'jpg', 1, 0, 7, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-02-11 01:38:38'),
(80, 2, 'NÂNG ĐỘ TÌNH YÊU - GIẢM THÊM 15%', 'nang-do-tinh-yeu-giam-them-15', NULL, '????Combo tình nhân - Giảm thêm 15%\r\n????Top 500 quà tặng yêu thích nhất', 'a:4:{s:11:\"coupon_code\";s:0:\"\";s:11:\"coupon_date\";s:10:\"14/02/2020\";s:11:\"coupon_note\";s:84:\"????Combo tình nhân - Giảm thêm 15%\r\n????Top 500 quà tặng yêu thích nhất\";s:14:\"affiliate_link\";s:175:\"https://go.isclix.com/deep_link/5229553935641796647?url=https%3A%2F%2Fpages.lazada.vn%2Fwow%2Fcamp%2Flazada%2Fdailycampaign%2Fvn%2F0220C0A2-Valentine-6-14-2%2Fnang-do-tinh-yeu\";}', NULL, 1, 30, 'uploads/import/image_750x_5e4205ba11edb.jpg', 'uploads/import/image_750x415_5e4205ba520e1.jpg', 'uploads/import/image_100x75_5e4205ba8b938.jpg', 'uploads/import/image_650x433_5e4205baa665a.jpg', 'jpg', 1, 0, 4, 0, NULL, 'post', NULL, NULL, NULL, 0, 1, 1, '2020-02-11 01:39:06'),
(81, 2, 'TRẢI NGHIỆM KIỂU MỚI - DU LỊCH TUYỆT VỜI', 'trai-nghiem-kieu-moi-du-lich-tuyet-voi', NULL, '<p>- Nhập code TRAINGHIEM, giảm gi&aacute; 50,000 VND cho đơn h&agrave;ng tối thiểu 1,000,000 - Nhập code DIKIEUMOI, giảm gi&aacute; 8% tối đa 400,000 VND đơn h&agrave;ng tối thiểu 1,700,000 VND.</p>', 'a:4:{s:11:\"coupon_code\";s:9:\"DIKIEUMOI\";s:11:\"coupon_date\";s:10:\"01/03/2020\";s:11:\"coupon_note\";s:193:\"- Nhập code TRAINGHIEM, giảm giá 50,000 VND cho đơn hàng tối thiểu 1,000,000 \r\n- Nhập code DIKIEUMOI, giảm giá 8% tối đa 400,000 VND đơn hàng tối thiểu 1,700,000 VND.\";s:14:\"affiliate_link\";s:101:\"https://go.isclix.com/deep_link/5229553935641796647?url=https://www.klook.com/vi/promo/vn-ma-giam-gia\";}', '', 1, 41, 'uploads/import/image_750x_5e42066281a22.jpg', 'uploads/import/image_750x415_5e42066344803.jpg', 'uploads/import/image_100x75_5e420663d16a1.jpg', 'uploads/import/image_650x433_5e420664479bf.jpg', 'jpg', 1, 0, 8, 0, '', 'post', '', '', '', 0, 1, 1, '2020-02-11 01:41:56');

-- --------------------------------------------------------

--
-- Table structure for table `post_files`
--

CREATE TABLE `post_files` (
  `id` int(11) NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `post_images`
--

CREATE TABLE `post_images` (
  `id` int(11) NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `image_path` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post_images`
--

INSERT INTO `post_images` (`id`, `post_id`, `image_path`) VALUES
(1, 5, 'uploads/images/image_750x_5df3337a49abf.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `reactions`
--

CREATE TABLE `reactions` (
  `id` int(11) NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `re_like` int(11) DEFAULT 0,
  `re_dislike` int(11) DEFAULT 0,
  `re_love` int(11) DEFAULT 0,
  `re_funny` int(11) DEFAULT 0,
  `re_angry` int(11) DEFAULT 0,
  `re_sad` int(11) DEFAULT 0,
  `re_wow` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reactions`
--

INSERT INTO `reactions` (`id`, `post_id`, `re_like`, `re_dislike`, `re_love`, `re_funny`, `re_angry`, `re_sad`, `re_wow`) VALUES
(1, 1, 0, 0, 0, 0, 0, 0, 0),
(2, 2, 0, 0, 0, 0, 0, 0, 0),
(3, 3, 0, 0, 0, 0, 0, 0, 0),
(4, 4, 0, 0, 0, 0, 0, 0, 0),
(5, 5, 0, 0, 0, 0, 0, 0, 0),
(6, 6, 0, 0, 0, 0, 0, 0, 0),
(7, 7, 0, 0, 0, 0, 0, 0, 0),
(8, 8, 0, 0, 0, 0, 0, 0, 0),
(9, 12, 0, 0, 0, 0, 0, 0, 0),
(10, 10, 0, 0, 0, 0, 0, 0, 0),
(11, 11, 0, 0, 0, 0, 0, 0, 0),
(12, 13, 0, 0, 0, 0, 0, 0, 0),
(13, 15, 0, 0, 0, 0, 0, 0, 0),
(14, 14, 0, 0, 0, 0, 0, 0, 0),
(15, 16, 0, 0, 0, 0, 0, 0, 0),
(16, 17, 0, 0, 0, 0, 0, 0, 0),
(17, 18, 0, 0, 0, 0, 0, 0, 0),
(18, 19, 0, 0, 0, 0, 0, 0, 0),
(19, 22, 0, 0, 0, 0, 0, 0, 0),
(20, 20, 0, 0, 0, 0, 0, 0, 0),
(21, 21, 0, 0, 0, 0, 0, 0, 0),
(22, 24, 0, 0, 0, 0, 0, 0, 0),
(23, 23, 0, 0, 0, 0, 0, 0, 0),
(24, 27, 0, 0, 0, 0, 0, 0, 0),
(25, 26, 0, 0, 0, 0, 0, 0, 0),
(26, 25, 0, 0, 0, 0, 0, 0, 0),
(27, 32, 0, 0, 0, 0, 0, 0, 0),
(28, 30, 0, 0, 0, 0, 0, 0, 0),
(29, 28, 0, 0, 0, 0, 0, 0, 0),
(30, 31, 0, 0, 0, 0, 0, 0, 0),
(31, 29, 0, 0, 0, 0, 0, 0, 0),
(32, 43, 0, 0, 0, 0, 0, 0, 0),
(33, 46, 0, 0, 0, 0, 0, 0, 0),
(34, 40, 0, 0, 0, 0, 0, 0, 0),
(35, 45, 0, 0, 0, 0, 0, 0, 0),
(36, 54, 0, 0, 0, 0, 0, 0, 0),
(37, 42, 0, 0, 0, 0, 0, 0, 0),
(38, 51, 0, 0, 0, 0, 0, 0, 0),
(39, 61, 0, 0, 0, 0, 0, 0, 0),
(40, 58, 0, 0, 0, 0, 0, 0, 0),
(41, 50, 0, 0, 0, 0, 0, 0, 0),
(42, 53, 0, 0, 0, 0, 0, 0, 0),
(43, 62, 0, 0, 0, 0, 0, 0, 0),
(44, 44, 0, 0, 0, 0, 0, 0, 0),
(45, 55, 0, 0, 0, 0, 0, 0, 0),
(46, 56, 0, 0, 0, 0, 0, 0, 0),
(47, 60, 0, 0, 0, 0, 0, 0, 0),
(48, 48, 0, 0, 0, 0, 0, 0, 0),
(49, 41, 0, 0, 0, 0, 0, 0, 0),
(50, 37, 0, 0, 0, 0, 0, 0, 0),
(51, 34, 0, 0, 0, 0, 0, 0, 0),
(52, 35, 0, 0, 0, 0, 0, 0, 0),
(53, 36, 0, 0, 0, 0, 0, 0, 0),
(54, 57, 0, 0, 0, 0, 0, 0, 0),
(55, 52, 0, 0, 0, 0, 0, 0, 0),
(56, 39, 0, 0, 0, 0, 0, 0, 0),
(57, 33, 0, 0, 0, 0, 0, 0, 0),
(58, 59, 0, 0, 0, 0, 0, 0, 0),
(59, 47, 0, 0, 0, 0, 0, 0, 0),
(60, 38, 0, 0, 0, 0, 0, 0, 0),
(61, 67, 0, 0, 0, 0, 0, 0, 0),
(62, 66, 0, 0, 0, 0, 0, 0, 0),
(63, 63, 0, 0, 0, 0, 0, 0, 0),
(64, 68, 0, 0, 0, 0, 0, 0, 0),
(65, 64, 0, 0, 0, 0, 0, 0, 0),
(66, 65, 0, 0, 0, 0, 0, 0, 0),
(67, 70, 0, 0, 0, 0, 0, 0, 0),
(68, 74, 0, 0, 0, 0, 0, 0, 0),
(69, 73, 0, 0, 0, 0, 0, 0, 0),
(70, 69, 0, 0, 0, 0, 0, 0, 0),
(71, 72, 0, 0, 0, 0, 0, 0, 0),
(72, 71, 0, 0, 0, 0, 0, 0, 0),
(73, 76, 0, 0, 0, 0, 0, 0, 0),
(74, 79, 0, 0, 0, 0, 0, 0, 0),
(75, 81, 0, 0, 0, 0, 0, 0, 0),
(76, 75, 0, 0, 0, 0, 0, 0, 0),
(77, 80, 0, 0, 0, 0, 0, 0, 0),
(78, 78, 0, 0, 0, 0, 0, 0, 0),
(79, 77, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `reading_lists`
--

CREATE TABLE `reading_lists` (
  `id` int(11) NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reading_lists`
--

INSERT INTO `reading_lists` (`id`, `post_id`, `user_id`) VALUES
(4, 15, 3),
(5, 13, 4);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `lang_id` tinyint(4) DEFAULT 1,
  `application_name` varchar(255) DEFAULT 'DEVNGUYENCMS',
  `site_title` varchar(255) DEFAULT NULL,
  `home_title` varchar(255) DEFAULT NULL,
  `site_description` varchar(500) DEFAULT NULL,
  `keywords` varchar(500) DEFAULT NULL,
  `facebook_url` varchar(500) DEFAULT NULL,
  `twitter_url` varchar(500) DEFAULT NULL,
  `instagram_url` varchar(500) DEFAULT NULL,
  `pinterest_url` varchar(500) DEFAULT NULL,
  `linkedin_url` varchar(500) DEFAULT NULL,
  `vk_url` varchar(500) DEFAULT NULL,
  `optional_url_button_name` varchar(500) DEFAULT 'Click Here to Visit',
  `about_footer` varchar(1000) DEFAULT NULL,
  `contact_text` text DEFAULT NULL,
  `contact_address` varchar(500) DEFAULT NULL,
  `contact_email` varchar(255) DEFAULT NULL,
  `contact_phone` varchar(255) DEFAULT NULL,
  `copyright` varchar(500) DEFAULT 'Copyright © 2019 DEVNGUYEN - All Rights Reserved.',
  `cookies_warning` tinyint(1) DEFAULT 0,
  `cookies_warning_text` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `lang_id`, `application_name`, `site_title`, `home_title`, `site_description`, `keywords`, `facebook_url`, `twitter_url`, `instagram_url`, `pinterest_url`, `linkedin_url`, `vk_url`, `optional_url_button_name`, `about_footer`, `contact_text`, `contact_address`, `contact_email`, `contact_phone`, `copyright`, `cookies_warning`, `cookies_warning_text`) VALUES
(1, 1, 'DEVNGUYENCMS', 'DEVNGUYENCMS', 'Index', 'DEVNGUYENCMS', 'devnguyen.com, Blog, Magazine', NULL, NULL, NULL, NULL, NULL, NULL, 'Click Here To See More', NULL, NULL, NULL, NULL, NULL, 'Copyright © 2019 DEVNGUYEN - All Rights Reserved.', 0, NULL),
(2, 2, 'DEVCMS', 'Mã giảm giá, mã khuyến mại, chia sẻ coupon giảm giá', 'Mã giảm giá cập nhật', 'Giảm giá hằng ngày là trang web tổng hợp các chương trình giảm giá của Tiki, Shopee, Sendo,... Mã coupon giảm giá Tết cuối năm, giảm giá Tết Canh tý khuyến mại cực khủng', 'mã giảm giá hằng ngày, mã coupon tết, coupon tết canh tý, coupon giam gia 2020, coupon shoppe, ma giam gia tiki, ma giam gia shoppe, sendo coupon, sale tết 2020, sale tết canh tý', 'https://www.facebook.com/dainguyenbw', 'https://twitter.com/daikupj', 'https://www.instagram.com/dainguyenbw/', '', '', '', '', 'Giamgiahangngay.com là trang web tổng hợp các chương trình giảm giá của Tiki, Shopee, Sendo,... Mã coupon giảm giá Tết cuối năm, giảm giá Tết Canh tý khuyến mại cực khủng.', '', '', '', '', 'Copyright © 2019 DEVNGUYEN.COM', 0, '<p>Blog giảm giá hằng ngày có sử dụng cookies. Bằng cách tiếp tục duyệt nội dung trang web, bạn đã đồng ý với việc sử dụng cookies của chúng tôi.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `email`, `token`, `created_at`) VALUES
(1, 'ubf.anhat@gmail.com', '5e0177ca44f1f8-16007446-88340050', '2019-12-24 02:28:26'),
(2, 'dai@devnguyen.com', '5e19a71090b6e0-37202684-99483145', '2020-01-11 10:44:32'),
(3, 'giamgiahangngay.com@domstat.su', '5e35861b0eb7c5-15660019-83443729', '2020-02-01 14:07:23');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `tag_slug` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `post_id`, `tag`, `tag_slug`) VALUES
(13, 11, 'BEAUTY BUFFET giảm giá', 'beauty-buffet-giam-gia'),
(14, 11, 'coupon beauty buffet', 'coupon-beauty-buffet'),
(15, 11, 'coupon tết beauty buffet', 'coupon-tet-beauty-buffet'),
(16, 11, 'coupon beauty buffet 2020', 'coupon-beauty-buffet-2020'),
(17, 12, 'beauty buffet coupon 2019', 'beauty-buffet-coupon-2019'),
(18, 12, 'beauty buffet coupon 50k', 'beauty-buffet-coupon-50k'),
(19, 12, 'giam gia beauty buffet', 'giam-gia-beauty-buffet'),
(20, 10, 'mã giảm giá RedDoorz', 'ma-giam-gia-reddoorz'),
(21, 10, 'mã khuyến mại RedDoorz', 'ma-khuyen-mai-reddoorz'),
(22, 10, 'coupn RedDoorz 2019', 'coupn-reddoorz-2019'),
(23, 10, 'coupon RedDoorz 2020', 'coupon-reddoorz-2020'),
(32, 13, 'vuabia khuyen mai', 'vuabia-khuyen-mai'),
(33, 13, 'tết 2020 vuabia', 'tet-2020-vuabia'),
(34, 13, 'tết này uống vuabia', 'tet-nay-uong-vuabia'),
(35, 13, 'vuabia khuyến mại cực khủng', 'vuabia-khuyen-mai-cuc-khung'),
(36, 14, 'vuabia tet 2020', 'vuabia-tet-2020'),
(37, 14, 'vuabia khuyen mai tet', 'vuabia-khuyen-mai-tet'),
(38, 14, 'vuabia tet nguyen dan', 'vuabia-tet-nguyen-dan'),
(39, 14, 'giam gia vuabia', 'giam-gia-vuabia'),
(41, 16, 'sendo giáng sinh', 'sendo-giang-sinh'),
(42, 16, 'sendo coupon nodel 2019', 'sendo-coupon-nodel-2019'),
(43, 16, 'sendo noel', 'sendo-noel'),
(44, 16, 'mã giảm giá sendo noel', 'ma-giam-gia-sendo-noel'),
(45, 16, 'noel sendo', 'noel-sendo'),
(46, 16, 'mã giảm giá giáng sinh', 'ma-giam-gia-giang-sinh'),
(47, 16, 'sale giáng sinh', 'sale-giang-sinh'),
(48, 16, 'coupon sendo', 'coupon-sendo'),
(49, 17, 'reddoorz giảm giá', 'reddoorz-giam-gia'),
(50, 17, 'reddoorz coupon giảm giá', 'reddoorz-coupon-giam-gia'),
(51, 17, 'reddoorz  khuyến mãi', 'reddoorz-khuyen-mai'),
(52, 18, 'sach tiki', 'sach-tiki'),
(53, 18, 'ma giam gia tiki', 'ma-giam-gia-tiki'),
(54, 18, 'giam gia sach tiki', 'giam-gia-sach-tiki'),
(55, 18, 'coupon tiki sach', 'coupon-tiki-sach'),
(56, 18, 'coupon sach tiki', 'coupon-sach-tiki'),
(57, 19, 'giảm giá shopee', 'giam-gia-shopee'),
(58, 19, 'giam gia hang ngay', 'giam-gia-hang-ngay'),
(59, 19, 'coupon shopee all', 'coupon-shopee-all'),
(60, 19, 'shopee tết 2020', 'shopee-tet-2020'),
(61, 19, 'shopee tết canh tý', 'shopee-tet-canh-ty'),
(62, 15, 'vuabia khuyen mai', 'vuabia-khuyen-mai'),
(63, 20, 'mã giảm giá bitis', 'ma-giam-gia-bitis'),
(64, 20, 'voucher bitis 2019', 'voucher-bitis-2019'),
(65, 20, 'mã coupon bitis', 'ma-coupon-bitis'),
(66, 20, 'shopee bitis', 'shopee-bitis'),
(67, 21, 'mã giảm giá juno shopee', 'ma-giam-gia-juno-shopee'),
(68, 21, 'mã giảm giá thời trang juno', 'ma-giam-gia-thoi-trang-juno'),
(69, 21, 'giảm giá juno shopee', 'giam-gia-juno-shopee'),
(70, 22, 'mã giảm giá giày converse', 'ma-giam-gia-giay-converse'),
(71, 22, 'giày converse giá rẻ shopee', 'giay-converse-gia-re-shopee'),
(75, 23, 'mã giảm giá thời trang kappa', 'ma-giam-gia-thoi-trang-kappa'),
(76, 23, 'kappa shopee coupon', 'kappa-shopee-coupon'),
(77, 23, 'shopee kappa chính hãng', 'shopee-kappa-chinh-hang'),
(78, 24, 'mã giảm giá hằng ngày lazada', 'ma-giam-gia-hang-ngay-lazada'),
(79, 24, 'mã giảm giá lazada 2020', 'ma-giam-gia-lazada-2020'),
(80, 24, 'ma giam gia lazada', 'ma-giam-gia-lazada'),
(81, 24, 'giam gia lazada 2020', 'giam-gia-lazada-2020'),
(82, 24, 'lazada giam gia tet nguyen dan', 'lazada-giam-gia-tet-nguyen-dan'),
(83, 24, 'lazada coupon 2020', 'lazada-coupon-2020'),
(84, 24, 'voucher lazada 2020', 'voucher-lazada-2020'),
(85, 24, 'voucher lazada 2020', 'voucher-lazada-2020'),
(86, 25, 'mã giảm giá hằng ngày lazada', 'ma-giam-gia-hang-ngay-lazada'),
(87, 25, 'mã giảm giá lazada 2020', 'ma-giam-gia-lazada-2020'),
(88, 25, 'ma giam gia lazada', 'ma-giam-gia-lazada'),
(89, 25, 'giam gia lazada 2020', 'giam-gia-lazada-2020'),
(90, 25, 'lazada giam gia tet nguyen dan', 'lazada-giam-gia-tet-nguyen-dan'),
(91, 25, 'lazada coupon 2020', 'lazada-coupon-2020'),
(92, 25, 'voucher lazada 2020', 'voucher-lazada-2020'),
(93, 25, 'ma giam gia dien thoai lazada', 'ma-giam-gia-dien-thoai-lazada'),
(94, 26, 'mã giảm giá hằng ngày lazada', 'ma-giam-gia-hang-ngay-lazada'),
(95, 26, 'mã giảm giá lazada 2020', 'ma-giam-gia-lazada-2020'),
(96, 26, 'ma giam gia lazada', 'ma-giam-gia-lazada'),
(97, 26, 'giam gia lazada 2020', 'giam-gia-lazada-2020'),
(98, 26, 'lazada giam gia tet nguyen dan', 'lazada-giam-gia-tet-nguyen-dan'),
(99, 26, 'lazada coupon 2020', 'lazada-coupon-2020'),
(100, 26, 'voucher lazada 2020', 'voucher-lazada-2020'),
(101, 26, 'ma giam gia dien thoai lazada', 'ma-giam-gia-dien-thoai-lazada'),
(102, 26, 'ma giam gia my pham lazada', 'ma-giam-gia-my-pham-lazada'),
(103, 27, 'mã giảm giá hằng ngày lazada', 'ma-giam-gia-hang-ngay-lazada'),
(104, 27, 'mã giảm giá lazada 2020', 'ma-giam-gia-lazada-2020'),
(105, 27, 'ma giam gia lazada', 'ma-giam-gia-lazada'),
(106, 27, 'giam gia lazada 2020', 'giam-gia-lazada-2020'),
(107, 27, 'lazada giam gia tet nguyen dan', 'lazada-giam-gia-tet-nguyen-dan'),
(108, 27, 'lazada coupon 2020', 'lazada-coupon-2020'),
(109, 27, 'voucher lazada 2020', 'voucher-lazada-2020'),
(110, 27, 'ma giam gia dien thoai lazada', 'ma-giam-gia-dien-thoai-lazada'),
(111, 27, 'ma giam gia my pham lazada', 'ma-giam-gia-my-pham-lazada'),
(112, 28, 'mã khuyến mại hằng ngày', 'ma-khuyen-mai-hang-ngay'),
(113, 28, 'giamgiahangngay.com', 'giamgiahangngaycom'),
(114, 28, 'mã khuyến mại lazada', 'ma-khuyen-mai-lazada'),
(115, 28, 'voucher lazada 2020', 'voucher-lazada-2020'),
(116, 28, 'tết lazada', 'tet-lazada'),
(117, 28, 'new year 2020 lazada', 'new-year-2020-lazada'),
(118, 28, 'hàng chính hãng giá rẻ lazada', 'hang-chinh-hang-gia-re-lazada'),
(119, 29, 'yes24 giảm giá', 'yes24-giam-gia'),
(120, 29, 'yes24 voucher thời trang', 'yes24-voucher-thoi-trang'),
(121, 29, 'yes24 voucher giày dép', 'yes24-voucher-giay-dep'),
(122, 29, 'yes24 coupon', 'yes24-coupon'),
(123, 29, 'khuyến mại yes24', 'khuyen-mai-yes24'),
(124, 30, 'mã giảm giá  khách sạn RedDoorz', 'ma-giam-gia-khach-san-reddoorz'),
(125, 30, 'RedDoorz voucher', 'reddoorz-voucher'),
(126, 30, 'RedDoorz khuyến mại', 'reddoorz-khuyen-mai'),
(127, 31, 'yes24 đồ gia dụng', 'yes24-do-gia-dung'),
(128, 31, 'đồ gia dụng giá rẻ', 'do-gia-dung-gia-re'),
(129, 31, 'mã giảm giá đồ gia dụng', 'ma-giam-gia-do-gia-dung'),
(130, 31, 'yes24 giảm giá sunhouse', 'yes24-giam-gia-sunhouse'),
(131, 31, 'sunhouse giảm giá', 'sunhouse-giam-gia'),
(132, 31, 'sunhouse chính hãng giá rẻ', 'sunhouse-chinh-hang-gia-re'),
(133, 32, 'sendo săn deal', 'sendo-san-deal'),
(134, 32, 'sendo tết canh tý', 'sendo-tet-canh-ty'),
(135, 32, 'voucher sendo', 'voucher-sendo'),
(136, 33, 'mã giảm giá yes24vn', 'ma-giam-gia-yes24vn'),
(137, 33, 'mã coupon yes24vn', 'ma-coupon-yes24vn'),
(138, 33, 'khuyến mại yes24vn', 'khuyen-mai-yes24vn'),
(139, 33, 'ma giam gia yes24vn', 'ma-giam-gia-yes24vn'),
(140, 33, 'blog giam gia yes24vn', 'blog-giam-gia-yes24vn'),
(141, 33, 'voucher giảm giá yes24vn', 'voucher-giam-gia-yes24vn'),
(142, 33, 'voucher giam gia yes24vn', 'voucher-giam-gia-yes24vn'),
(143, 33, 'ma giam gia yes24vn 2020', 'ma-giam-gia-yes24vn-2020'),
(144, 33, 'mã giảm giá yes24vn 2020', 'ma-giam-gia-yes24vn-2020'),
(145, 33, 'Top Brand Thời trang nam nữ Hàn Quốc khuyến mãi 50%', 'top-brand-thoi-trang-nam-nu-han-quoc-khuyen-mai-50'),
(146, 34, 'mã giảm giá yes24vn', 'ma-giam-gia-yes24vn'),
(147, 34, 'mã coupon yes24vn', 'ma-coupon-yes24vn'),
(148, 34, 'khuyến mại yes24vn', 'khuyen-mai-yes24vn'),
(149, 34, 'ma giam gia yes24vn', 'ma-giam-gia-yes24vn'),
(150, 34, 'blog giam gia yes24vn', 'blog-giam-gia-yes24vn'),
(151, 34, 'voucher giảm giá yes24vn', 'voucher-giam-gia-yes24vn'),
(152, 34, 'voucher giam gia yes24vn', 'voucher-giam-gia-yes24vn'),
(153, 34, 'ma giam gia yes24vn 2020', 'ma-giam-gia-yes24vn-2020'),
(154, 34, 'mã giảm giá yes24vn 2020', 'ma-giam-gia-yes24vn-2020'),
(155, 34, '4U - NEW LAUNCHING - CHỈ TỪ 99K', '4u-new-launching-chi-tu-99k'),
(156, 35, 'mã giảm giá yes24vn', 'ma-giam-gia-yes24vn'),
(157, 35, 'mã coupon yes24vn', 'ma-coupon-yes24vn'),
(158, 35, 'khuyến mại yes24vn', 'khuyen-mai-yes24vn'),
(159, 35, 'ma giam gia yes24vn', 'ma-giam-gia-yes24vn'),
(160, 35, 'blog giam gia yes24vn', 'blog-giam-gia-yes24vn'),
(161, 35, 'voucher giảm giá yes24vn', 'voucher-giam-gia-yes24vn'),
(162, 35, 'voucher giam gia yes24vn', 'voucher-giam-gia-yes24vn'),
(163, 35, 'ma giam gia yes24vn 2020', 'ma-giam-gia-yes24vn-2020'),
(164, 35, 'mã giảm giá yes24vn 2020', 'ma-giam-gia-yes24vn-2020'),
(165, 35, 'Chợ phiên tý vàng - Deal ngon giá khủng', 'cho-phien-ty-vang-deal-ngon-gia-khung'),
(176, 36, 'mã giảm giá vuabia', 'ma-giam-gia-vuabia'),
(177, 36, 'mã coupon vuabia', 'ma-coupon-vuabia'),
(178, 36, 'khuyến mại vuabia', 'khuyen-mai-vuabia'),
(179, 36, 'ma giam gia vuabia', 'ma-giam-gia-vuabia'),
(180, 36, 'blog giam gia vuabia', 'blog-giam-gia-vuabia'),
(181, 36, 'voucher giảm giá vuabia', 'voucher-giam-gia-vuabia'),
(182, 36, 'voucher giam gia vuabia', 'voucher-giam-gia-vuabia'),
(183, 36, 'ma giam gia vuabia 2020', 'ma-giam-gia-vuabia-2020'),
(184, 36, 'mã giảm giá vuabia 2020', 'ma-giam-gia-vuabia-2020'),
(185, 36, 'Giảm 150K khi mua combo 2 thùng 24 lon Budweiser 330ml 840K', 'giam-150k-khi-mua-combo-2-thung-24-lon-budweiser-330ml-840k'),
(186, 37, 'mã giảm giá sendovn', 'ma-giam-gia-sendovn'),
(187, 37, 'mã coupon sendovn', 'ma-coupon-sendovn'),
(188, 37, 'khuyến mại sendovn', 'khuyen-mai-sendovn'),
(189, 37, 'ma giam gia sendovn', 'ma-giam-gia-sendovn'),
(190, 37, 'blog giam gia sendovn', 'blog-giam-gia-sendovn'),
(191, 37, 'voucher giảm giá sendovn', 'voucher-giam-gia-sendovn'),
(192, 37, 'voucher giam gia sendovn', 'voucher-giam-gia-sendovn'),
(193, 37, 'ma giam gia sendovn 2020', 'ma-giam-gia-sendovn-2020'),
(194, 37, 'mã giảm giá sendovn 2020', 'ma-giam-gia-sendovn-2020'),
(195, 37, 'Voucher khủng đến 500K', 'voucher-khung-den-500k'),
(196, 38, 'mã giảm giá lazadacps', 'ma-giam-gia-lazadacps'),
(197, 38, 'mã coupon lazadacps', 'ma-coupon-lazadacps'),
(198, 38, 'khuyến mại lazadacps', 'khuyen-mai-lazadacps'),
(199, 38, 'ma giam gia lazadacps', 'ma-giam-gia-lazadacps'),
(200, 38, 'blog giam gia lazadacps', 'blog-giam-gia-lazadacps'),
(201, 38, 'voucher giảm giá lazadacps', 'voucher-giam-gia-lazadacps'),
(202, 38, 'voucher giam gia lazadacps', 'voucher-giam-gia-lazadacps'),
(203, 38, 'ma giam gia lazadacps 2020', 'ma-giam-gia-lazadacps-2020'),
(204, 38, 'mã giảm giá lazadacps 2020', 'ma-giam-gia-lazadacps-2020'),
(205, 38, 'ƯU ĐÃI TẾT 2020 THEO NGÀNH HÀNG', 'uu-dai-tet-2020-theo-nganh-hang'),
(206, 39, 'mã giảm giá lazadacps', 'ma-giam-gia-lazadacps'),
(207, 39, 'mã coupon lazadacps', 'ma-coupon-lazadacps'),
(208, 39, 'khuyến mại lazadacps', 'khuyen-mai-lazadacps'),
(209, 39, 'ma giam gia lazadacps', 'ma-giam-gia-lazadacps'),
(210, 39, 'blog giam gia lazadacps', 'blog-giam-gia-lazadacps'),
(211, 39, 'voucher giảm giá lazadacps', 'voucher-giam-gia-lazadacps'),
(212, 39, 'voucher giam gia lazadacps', 'voucher-giam-gia-lazadacps'),
(213, 39, 'ma giam gia lazadacps 2020', 'ma-giam-gia-lazadacps-2020'),
(214, 39, 'mã giảm giá lazadacps 2020', 'ma-giam-gia-lazadacps-2020'),
(215, 39, 'Thời trang & Thể thao: Săn deal may mắn 8K', 'thoi-trang-the-thao-san-deal-may-man-8k'),
(216, 40, 'mã giảm giá lazadacps', 'ma-giam-gia-lazadacps'),
(217, 40, 'mã coupon lazadacps', 'ma-coupon-lazadacps'),
(218, 40, 'khuyến mại lazadacps', 'khuyen-mai-lazadacps'),
(219, 40, 'ma giam gia lazadacps', 'ma-giam-gia-lazadacps'),
(220, 40, 'blog giam gia lazadacps', 'blog-giam-gia-lazadacps'),
(221, 40, 'voucher giảm giá lazadacps', 'voucher-giam-gia-lazadacps'),
(222, 40, 'voucher giam gia lazadacps', 'voucher-giam-gia-lazadacps'),
(223, 40, 'ma giam gia lazadacps 2020', 'ma-giam-gia-lazadacps-2020'),
(224, 40, 'mã giảm giá lazadacps 2020', 'ma-giam-gia-lazadacps-2020'),
(225, 40, 'Bách hóa online - Đồng giá 88K', 'bach-hoa-online-dong-gia-88k'),
(226, 41, 'mã giảm giá lazadacps', 'ma-giam-gia-lazadacps'),
(227, 41, 'mã coupon lazadacps', 'ma-coupon-lazadacps'),
(228, 41, 'khuyến mại lazadacps', 'khuyen-mai-lazadacps'),
(229, 41, 'ma giam gia lazadacps', 'ma-giam-gia-lazadacps'),
(230, 41, 'blog giam gia lazadacps', 'blog-giam-gia-lazadacps'),
(231, 41, 'voucher giảm giá lazadacps', 'voucher-giam-gia-lazadacps'),
(232, 41, 'voucher giam gia lazadacps', 'voucher-giam-gia-lazadacps'),
(233, 41, 'ma giam gia lazadacps 2020', 'ma-giam-gia-lazadacps-2020'),
(234, 41, 'mã giảm giá lazadacps 2020', 'ma-giam-gia-lazadacps-2020'),
(235, 41, 'Sức khỏe & làm đẹp: Bộ quà tặng chỉ từ 188K', 'suc-khoe-lam-dep-bo-qua-tang-chi-tu-188k'),
(236, 42, 'mã giảm giá lazadacps', 'ma-giam-gia-lazadacps'),
(237, 42, 'mã coupon lazadacps', 'ma-coupon-lazadacps'),
(238, 42, 'khuyến mại lazadacps', 'khuyen-mai-lazadacps'),
(239, 42, 'ma giam gia lazadacps', 'ma-giam-gia-lazadacps'),
(240, 42, 'blog giam gia lazadacps', 'blog-giam-gia-lazadacps'),
(241, 42, 'voucher giảm giá lazadacps', 'voucher-giam-gia-lazadacps'),
(242, 42, 'voucher giam gia lazadacps', 'voucher-giam-gia-lazadacps'),
(243, 42, 'ma giam gia lazadacps 2020', 'ma-giam-gia-lazadacps-2020'),
(244, 42, 'mã giảm giá lazadacps 2020', 'ma-giam-gia-lazadacps-2020'),
(245, 42, 'Mẹ và bé: Voucher 200K', 'me-va-be-voucher-200k'),
(246, 43, 'mã giảm giá lazadacps', 'ma-giam-gia-lazadacps'),
(247, 43, 'mã coupon lazadacps', 'ma-coupon-lazadacps'),
(248, 43, 'khuyến mại lazadacps', 'khuyen-mai-lazadacps'),
(249, 43, 'ma giam gia lazadacps', 'ma-giam-gia-lazadacps'),
(250, 43, 'blog giam gia lazadacps', 'blog-giam-gia-lazadacps'),
(251, 43, 'voucher giảm giá lazadacps', 'voucher-giam-gia-lazadacps'),
(252, 43, 'voucher giam gia lazadacps', 'voucher-giam-gia-lazadacps'),
(253, 43, 'ma giam gia lazadacps 2020', 'ma-giam-gia-lazadacps-2020'),
(254, 43, 'mã giảm giá lazadacps 2020', 'ma-giam-gia-lazadacps-2020'),
(255, 43, 'CLICK TẾT VÀO NHÀ - KHUYẾN MÃI CỰC SỐC', 'click-tet-vao-nha-khuyen-mai-cuc-soc'),
(256, 44, 'mã giảm giá lazadacps', 'ma-giam-gia-lazadacps'),
(257, 44, 'mã coupon lazadacps', 'ma-coupon-lazadacps'),
(258, 44, 'khuyến mại lazadacps', 'khuyen-mai-lazadacps'),
(259, 44, 'ma giam gia lazadacps', 'ma-giam-gia-lazadacps'),
(260, 44, 'blog giam gia lazadacps', 'blog-giam-gia-lazadacps'),
(261, 44, 'voucher giảm giá lazadacps', 'voucher-giam-gia-lazadacps'),
(262, 44, 'voucher giam gia lazadacps', 'voucher-giam-gia-lazadacps'),
(263, 44, 'ma giam gia lazadacps 2020', 'ma-giam-gia-lazadacps-2020'),
(264, 44, 'mã giảm giá lazadacps 2020', 'ma-giam-gia-lazadacps-2020'),
(265, 44, 'Hot Deal', 'hot-deal'),
(266, 44, 'Flash sale - Voucher 400k cho đơn hàng từ 5 Triệu', 'flash-sale-voucher-400k-cho-don-hang-tu-5-trieu'),
(267, 45, 'mã giảm giá lazadacps', 'ma-giam-gia-lazadacps'),
(268, 45, 'mã coupon lazadacps', 'ma-coupon-lazadacps'),
(269, 45, 'khuyến mại lazadacps', 'khuyen-mai-lazadacps'),
(270, 45, 'ma giam gia lazadacps', 'ma-giam-gia-lazadacps'),
(271, 45, 'blog giam gia lazadacps', 'blog-giam-gia-lazadacps'),
(272, 45, 'voucher giảm giá lazadacps', 'voucher-giam-gia-lazadacps'),
(273, 45, 'voucher giam gia lazadacps', 'voucher-giam-gia-lazadacps'),
(274, 45, 'ma giam gia lazadacps 2020', 'ma-giam-gia-lazadacps-2020'),
(275, 45, 'mã giảm giá lazadacps 2020', 'ma-giam-gia-lazadacps-2020'),
(276, 45, 'Masan khuyến mãi hấp dẫn', 'masan-khuyen-mai-hap-dan'),
(277, 46, 'mã giảm giá shinhanbank', 'ma-giam-gia-shinhanbank'),
(278, 46, 'mã coupon shinhanbank', 'ma-coupon-shinhanbank'),
(279, 46, 'khuyến mại shinhanbank', 'khuyen-mai-shinhanbank'),
(280, 46, 'ma giam gia shinhanbank', 'ma-giam-gia-shinhanbank'),
(281, 46, 'blog giam gia shinhanbank', 'blog-giam-gia-shinhanbank'),
(282, 46, 'voucher giảm giá shinhanbank', 'voucher-giam-gia-shinhanbank'),
(283, 46, 'voucher giam gia shinhanbank', 'voucher-giam-gia-shinhanbank'),
(284, 46, 'ma giam gia shinhanbank 2020', 'ma-giam-gia-shinhanbank-2020'),
(285, 46, 'mã giảm giá shinhanbank 2020', 'ma-giam-gia-shinhanbank-2020'),
(286, 46, 'Shinhanbank - Ưu đãi dành riêng cho du lịch tự túc 2019', 'shinhanbank-uu-dai-danh-rieng-cho-du-lich-tu-tuc-2019'),
(287, 47, 'mã giảm giá concung', 'ma-giam-gia-concung'),
(288, 47, 'mã coupon concung', 'ma-coupon-concung'),
(289, 47, 'khuyến mại concung', 'khuyen-mai-concung'),
(290, 47, 'ma giam gia concung', 'ma-giam-gia-concung'),
(291, 47, 'blog giam gia concung', 'blog-giam-gia-concung'),
(292, 47, 'voucher giảm giá concung', 'voucher-giam-gia-concung'),
(293, 47, 'voucher giam gia concung', 'voucher-giam-gia-concung'),
(294, 47, 'ma giam gia concung 2020', 'ma-giam-gia-concung-2020'),
(295, 47, 'mã giảm giá concung 2020', 'ma-giam-gia-concung-2020'),
(296, 47, 'ƯU ĐÃI CHO NHIỀU MẶT HÀNG: SỮA', 'uu-dai-cho-nhieu-mat-hang-sua'),
(297, 47, 'TÃ', 'ta'),
(298, 47, 'ĐỒ CHƠI', 'do-choi'),
(299, 47, 'THỜI TRANG', 'thoi-trang'),
(330, 48, 'mã giảm giá reddoorz', 'ma-giam-gia-reddoorz'),
(331, 48, 'mã coupon reddoorz', 'ma-coupon-reddoorz'),
(332, 48, 'khuyến mại reddoorz', 'khuyen-mai-reddoorz'),
(333, 48, 'ma giam gia reddoorz', 'ma-giam-gia-reddoorz'),
(334, 48, 'blog giam gia reddoorz', 'blog-giam-gia-reddoorz'),
(335, 48, 'voucher giảm giá reddoorz', 'voucher-giam-gia-reddoorz'),
(336, 48, 'voucher giam gia reddoorz', 'voucher-giam-gia-reddoorz'),
(337, 48, 'ma giam gia reddoorz 2020', 'ma-giam-gia-reddoorz-2020'),
(338, 48, 'mã giảm giá reddoorz 2020', 'ma-giam-gia-reddoorz-2020'),
(339, 48, 'REDDOORZ GIẢM GIÁ CUỐI NĂM', 'reddoorz-giam-gia-cuoi-nam'),
(340, 50, 'mã giảm giá lug', 'ma-giam-gia-lug'),
(341, 50, 'mã coupon lug', 'ma-coupon-lug'),
(342, 50, 'khuyến mại lug', 'khuyen-mai-lug'),
(343, 50, 'ma giam gia lug', 'ma-giam-gia-lug'),
(344, 50, 'blog giam gia lug', 'blog-giam-gia-lug'),
(345, 50, 'voucher giảm giá lug', 'voucher-giam-gia-lug'),
(346, 50, 'voucher giam gia lug', 'voucher-giam-gia-lug'),
(347, 50, 'ma giam gia lug 2020', 'ma-giam-gia-lug-2020'),
(348, 50, 'mã giảm giá lug 2020', 'ma-giam-gia-lug-2020'),
(349, 50, 'END OF SEASON - SALE CÚ TRÓT 80%', 'end-of-season-sale-cu-trot-80'),
(350, 51, 'mã giảm giá bambooairways', 'ma-giam-gia-bambooairways'),
(351, 51, 'mã coupon bambooairways', 'ma-coupon-bambooairways'),
(352, 51, 'khuyến mại bambooairways', 'khuyen-mai-bambooairways'),
(353, 51, 'ma giam gia bambooairways', 'ma-giam-gia-bambooairways'),
(354, 51, 'blog giam gia bambooairways', 'blog-giam-gia-bambooairways'),
(355, 51, 'voucher giảm giá bambooairways', 'voucher-giam-gia-bambooairways'),
(356, 51, 'voucher giam gia bambooairways', 'voucher-giam-gia-bambooairways'),
(357, 51, 'ma giam gia bambooairways 2020', 'ma-giam-gia-bambooairways-2020'),
(358, 51, 'mã giảm giá bambooairways 2020', 'ma-giam-gia-bambooairways-2020'),
(359, 51, 'TẾT CÙNG BAMBOO - VI VU ĐẮC LỘC', 'tet-cung-bamboo-vi-vu-dac-loc'),
(360, 52, 'mã giảm giá bambooairways', 'ma-giam-gia-bambooairways'),
(361, 52, 'mã coupon bambooairways', 'ma-coupon-bambooairways'),
(362, 52, 'khuyến mại bambooairways', 'khuyen-mai-bambooairways'),
(363, 52, 'ma giam gia bambooairways', 'ma-giam-gia-bambooairways'),
(364, 52, 'blog giam gia bambooairways', 'blog-giam-gia-bambooairways'),
(365, 52, 'voucher giảm giá bambooairways', 'voucher-giam-gia-bambooairways'),
(366, 52, 'voucher giam gia bambooairways', 'voucher-giam-gia-bambooairways'),
(367, 52, 'ma giam gia bambooairways 2020', 'ma-giam-gia-bambooairways-2020'),
(368, 52, 'mã giảm giá bambooairways 2020', 'ma-giam-gia-bambooairways-2020'),
(369, 52, 'SINH NHẬT WEBSITE MỞ NGÀN ƯU ĐÃI', 'sinh-nhat-website-mo-ngan-uu-dai'),
(370, 53, 'mã giảm giá xwatch', 'ma-giam-gia-xwatch'),
(371, 53, 'mã coupon xwatch', 'ma-coupon-xwatch'),
(372, 53, 'khuyến mại xwatch', 'khuyen-mai-xwatch'),
(373, 53, 'ma giam gia xwatch', 'ma-giam-gia-xwatch'),
(374, 53, 'blog giam gia xwatch', 'blog-giam-gia-xwatch'),
(375, 53, 'voucher giảm giá xwatch', 'voucher-giam-gia-xwatch'),
(376, 53, 'voucher giam gia xwatch', 'voucher-giam-gia-xwatch'),
(377, 53, 'ma giam gia xwatch 2020', 'ma-giam-gia-xwatch-2020'),
(378, 53, 'mã giảm giá xwatch 2020', 'ma-giam-gia-xwatch-2020'),
(379, 53, 'TẾT SẮM ĐỒNG HỒ MỚI - LÌ XÌ TỚI 51%', 'tet-sam-dong-ho-moi-li-xi-toi-51'),
(380, 54, 'mã giảm giá klookvn', 'ma-giam-gia-klookvn'),
(381, 54, 'mã coupon klookvn', 'ma-coupon-klookvn'),
(382, 54, 'khuyến mại klookvn', 'khuyen-mai-klookvn'),
(383, 54, 'ma giam gia klookvn', 'ma-giam-gia-klookvn'),
(384, 54, 'blog giam gia klookvn', 'blog-giam-gia-klookvn'),
(385, 54, 'voucher giảm giá klookvn', 'voucher-giam-gia-klookvn'),
(386, 54, 'voucher giam gia klookvn', 'voucher-giam-gia-klookvn'),
(387, 54, 'ma giam gia klookvn 2020', 'ma-giam-gia-klookvn-2020'),
(388, 54, 'mã giảm giá klookvn 2020', 'ma-giam-gia-klookvn-2020'),
(389, 54, 'Vé JR Pass Toàn Nhật Bản 7 Ngày Và SIM 4G Nhật Bản', 've-jr-pass-toan-nhat-ban-7-ngay-va-sim-4g-nhat-ban'),
(410, 55, 'mã giảm giá klookvn', 'ma-giam-gia-klookvn'),
(411, 55, 'mã coupon klookvn', 'ma-coupon-klookvn'),
(412, 55, 'khuyến mại klookvn', 'khuyen-mai-klookvn'),
(413, 55, 'ma giam gia klookvn', 'ma-giam-gia-klookvn'),
(414, 55, 'blog giam gia klookvn', 'blog-giam-gia-klookvn'),
(415, 55, 'voucher giảm giá klookvn', 'voucher-giam-gia-klookvn'),
(416, 55, 'voucher giam gia klookvn', 'voucher-giam-gia-klookvn'),
(417, 55, 'ma giam gia klookvn 2020', 'ma-giam-gia-klookvn-2020'),
(418, 55, 'mã giảm giá klookvn 2020', 'ma-giam-gia-klookvn-2020'),
(419, 55, 'NHẬP MÃ TET2020 GIẢM 5%', 'nhap-ma-tet2020-giam-5'),
(420, 56, 'mã giảm giá klookvn', 'ma-giam-gia-klookvn'),
(421, 56, 'mã coupon klookvn', 'ma-coupon-klookvn'),
(422, 56, 'khuyến mại klookvn', 'khuyen-mai-klookvn'),
(423, 56, 'ma giam gia klookvn', 'ma-giam-gia-klookvn'),
(424, 56, 'blog giam gia klookvn', 'blog-giam-gia-klookvn'),
(425, 56, 'voucher giảm giá klookvn', 'voucher-giam-gia-klookvn'),
(426, 56, 'voucher giam gia klookvn', 'voucher-giam-gia-klookvn'),
(427, 56, 'ma giam gia klookvn 2020', 'ma-giam-gia-klookvn-2020'),
(428, 56, 'mã giảm giá klookvn 2020', 'ma-giam-gia-klookvn-2020'),
(429, 56, 'NHẬP MÃ CANHTY20 GIẢM 10%', 'nhap-ma-canhty20-giam-10'),
(430, 57, 'mã giảm giá vinpearl', 'ma-giam-gia-vinpearl'),
(431, 57, 'mã coupon vinpearl', 'ma-coupon-vinpearl'),
(432, 57, 'khuyến mại vinpearl', 'khuyen-mai-vinpearl'),
(433, 57, 'ma giam gia vinpearl', 'ma-giam-gia-vinpearl'),
(434, 57, 'blog giam gia vinpearl', 'blog-giam-gia-vinpearl'),
(435, 57, 'voucher giảm giá vinpearl', 'voucher-giam-gia-vinpearl'),
(436, 57, 'voucher giam gia vinpearl', 'voucher-giam-gia-vinpearl'),
(437, 57, 'ma giam gia vinpearl 2020', 'ma-giam-gia-vinpearl-2020'),
(438, 57, 'mã giảm giá vinpearl 2020', 'ma-giam-gia-vinpearl-2020'),
(439, 57, 'kỳ nghỉ du xuân đón tết canh tý tại vinpearl - giá chỉ từ 1.500.000 vnđ/người', 'ky-nghi-du-xuan-don-tet-canh-ty-tai-vinpearl-gia-chi-tu-1500000-vndnguoi'),
(440, 58, 'mã giảm giá vinpearl', 'ma-giam-gia-vinpearl'),
(441, 58, 'mã coupon vinpearl', 'ma-coupon-vinpearl'),
(442, 58, 'khuyến mại vinpearl', 'khuyen-mai-vinpearl'),
(443, 58, 'ma giam gia vinpearl', 'ma-giam-gia-vinpearl'),
(444, 58, 'blog giam gia vinpearl', 'blog-giam-gia-vinpearl'),
(445, 58, 'voucher giảm giá vinpearl', 'voucher-giam-gia-vinpearl'),
(446, 58, 'voucher giam gia vinpearl', 'voucher-giam-gia-vinpearl'),
(447, 58, 'ma giam gia vinpearl 2020', 'ma-giam-gia-vinpearl-2020'),
(448, 58, 'mã giảm giá vinpearl 2020', 'ma-giam-gia-vinpearl-2020'),
(449, 58, 'kỳ nghỉ thượng lưu - giá cực tối ưu với combo nghỉ dưỡng trọn gói thông minh dành cho gia đình tết nguyên đán', 'ky-nghi-thuong-luu-gia-cuc-toi-uu-voi-combo-nghi-duong-tron-goi-thong-minh-danh-cho-gia-dinh-tet-nguyen-dan'),
(450, 59, 'mã giảm giá vinpearl', 'ma-giam-gia-vinpearl'),
(451, 59, 'mã coupon vinpearl', 'ma-coupon-vinpearl'),
(452, 59, 'khuyến mại vinpearl', 'khuyen-mai-vinpearl'),
(453, 59, 'ma giam gia vinpearl', 'ma-giam-gia-vinpearl'),
(454, 59, 'blog giam gia vinpearl', 'blog-giam-gia-vinpearl'),
(455, 59, 'voucher giảm giá vinpearl', 'voucher-giam-gia-vinpearl'),
(456, 59, 'voucher giam gia vinpearl', 'voucher-giam-gia-vinpearl'),
(457, 59, 'ma giam gia vinpearl 2020', 'ma-giam-gia-vinpearl-2020'),
(458, 59, 'mã giảm giá vinpearl 2020', 'ma-giam-gia-vinpearl-2020'),
(459, 59, 'tour trọn gói du xuân hành hương lễ phật cầu an tại hải phòng - hà nam - thanh hóa - tây ninh tết nguyên đán', 'tour-tron-goi-du-xuan-hanh-huong-le-phat-cau-an-tai-hai-phong-ha-nam-thanh-hoa-tay-ninh-tet-nguyen-dan'),
(460, 60, 'mã giảm giá vinpearl', 'ma-giam-gia-vinpearl'),
(461, 60, 'mã coupon vinpearl', 'ma-coupon-vinpearl'),
(462, 60, 'khuyến mại vinpearl', 'khuyen-mai-vinpearl'),
(463, 60, 'ma giam gia vinpearl', 'ma-giam-gia-vinpearl'),
(464, 60, 'blog giam gia vinpearl', 'blog-giam-gia-vinpearl'),
(465, 60, 'voucher giảm giá vinpearl', 'voucher-giam-gia-vinpearl'),
(466, 60, 'voucher giam gia vinpearl', 'voucher-giam-gia-vinpearl'),
(467, 60, 'ma giam gia vinpearl 2020', 'ma-giam-gia-vinpearl-2020'),
(468, 60, 'mã giảm giá vinpearl 2020', 'ma-giam-gia-vinpearl-2020'),
(469, 60, 'kỳ nghỉ dưỡng biệt thự biển đẳng cấp vinpearl dành cho đại gia đình', 'ky-nghi-duong-biet-thu-bien-dang-cap-vinpearl-danh-cho-dai-gia-dinh'),
(470, 60, 'kết hợp tour tham quan địa danh tại đà nẵng - hội an - nha trang - phú quốc', 'ket-hop-tour-tham-quan-dia-danh-tai-da-nang-hoi-an-nha-trang-phu-quoc'),
(471, 61, 'mã giảm giá hoanghamobile', 'ma-giam-gia-hoanghamobile'),
(472, 61, 'mã coupon hoanghamobile', 'ma-coupon-hoanghamobile'),
(473, 61, 'khuyến mại hoanghamobile', 'khuyen-mai-hoanghamobile'),
(474, 61, 'ma giam gia hoanghamobile', 'ma-giam-gia-hoanghamobile'),
(475, 61, 'blog giam gia hoanghamobile', 'blog-giam-gia-hoanghamobile'),
(476, 61, 'voucher giảm giá hoanghamobile', 'voucher-giam-gia-hoanghamobile'),
(477, 61, 'voucher giam gia hoanghamobile', 'voucher-giam-gia-hoanghamobile'),
(478, 61, 'ma giam gia hoanghamobile 2020', 'ma-giam-gia-hoanghamobile-2020'),
(479, 61, 'mã giảm giá hoanghamobile 2020', 'ma-giam-gia-hoanghamobile-2020'),
(480, 61, 'mừng xuân đang tới - đón ngay sale mới', 'mung-xuan-dang-toi-don-ngay-sale-moi'),
(481, 62, 'mã giảm giá hoanghamobile', 'ma-giam-gia-hoanghamobile'),
(482, 62, 'mã coupon hoanghamobile', 'ma-coupon-hoanghamobile'),
(483, 62, 'khuyến mại hoanghamobile', 'khuyen-mai-hoanghamobile'),
(484, 62, 'ma giam gia hoanghamobile', 'ma-giam-gia-hoanghamobile'),
(485, 62, 'blog giam gia hoanghamobile', 'blog-giam-gia-hoanghamobile'),
(486, 62, 'voucher giảm giá hoanghamobile', 'voucher-giam-gia-hoanghamobile'),
(487, 62, 'voucher giam gia hoanghamobile', 'voucher-giam-gia-hoanghamobile'),
(488, 62, 'ma giam gia hoanghamobile 2020', 'ma-giam-gia-hoanghamobile-2020'),
(489, 62, 'mã giảm giá hoanghamobile 2020', 'ma-giam-gia-hoanghamobile-2020'),
(490, 62, 'galaxy a51 sập giá chỉ còn hơn 6 triệu đồng', 'galaxy-a51-sap-gia-chi-con-hon-6-trieu-dong'),
(491, 63, 'mã giảm giá shopvnexpress', 'ma-giam-gia-shopvnexpress'),
(492, 63, 'mã coupon shopvnexpress', 'ma-coupon-shopvnexpress'),
(493, 63, 'khuyến mại shopvnexpress', 'khuyen-mai-shopvnexpress'),
(494, 63, 'ma giam gia shopvnexpress', 'ma-giam-gia-shopvnexpress'),
(495, 63, 'blog giam gia shopvnexpress', 'blog-giam-gia-shopvnexpress'),
(496, 63, 'voucher giảm giá shopvnexpress', 'voucher-giam-gia-shopvnexpress'),
(497, 63, 'voucher giam gia shopvnexpress', 'voucher-giam-gia-shopvnexpress'),
(498, 63, 'ma giam gia shopvnexpress 2020', 'ma-giam-gia-shopvnexpress-2020'),
(499, 63, 'mã giảm giá shopvnexpress 2020', 'ma-giam-gia-shopvnexpress-2020'),
(500, 63, 'quà sung túc', 'qua-sung-tuc'),
(501, 63, 'tết sum vầy', 'tet-sum-vay'),
(502, 64, 'mã giảm giá shopvnexpress', 'ma-giam-gia-shopvnexpress'),
(503, 64, 'mã coupon shopvnexpress', 'ma-coupon-shopvnexpress'),
(504, 64, 'khuyến mại shopvnexpress', 'khuyen-mai-shopvnexpress'),
(505, 64, 'ma giam gia shopvnexpress', 'ma-giam-gia-shopvnexpress'),
(506, 64, 'blog giam gia shopvnexpress', 'blog-giam-gia-shopvnexpress'),
(507, 64, 'voucher giảm giá shopvnexpress', 'voucher-giam-gia-shopvnexpress'),
(508, 64, 'voucher giam gia shopvnexpress', 'voucher-giam-gia-shopvnexpress'),
(509, 64, 'ma giam gia shopvnexpress 2020', 'ma-giam-gia-shopvnexpress-2020'),
(510, 64, 'mã giảm giá shopvnexpress 2020', 'ma-giam-gia-shopvnexpress-2020'),
(511, 64, 'đồng hồ cao cấp - giảm đến 50%', 'dong-ho-cao-cap-giam-den-50'),
(512, 65, 'mã giảm giá shopvnexpress', 'ma-giam-gia-shopvnexpress'),
(513, 65, 'mã coupon shopvnexpress', 'ma-coupon-shopvnexpress'),
(514, 65, 'khuyến mại shopvnexpress', 'khuyen-mai-shopvnexpress'),
(515, 65, 'ma giam gia shopvnexpress', 'ma-giam-gia-shopvnexpress'),
(516, 65, 'blog giam gia shopvnexpress', 'blog-giam-gia-shopvnexpress'),
(517, 65, 'voucher giảm giá shopvnexpress', 'voucher-giam-gia-shopvnexpress'),
(518, 65, 'voucher giam gia shopvnexpress', 'voucher-giam-gia-shopvnexpress'),
(519, 65, 'ma giam gia shopvnexpress 2020', 'ma-giam-gia-shopvnexpress-2020'),
(520, 65, 'mã giảm giá shopvnexpress 2020', 'ma-giam-gia-shopvnexpress-2020'),
(521, 65, 'sản phẩm về tinh dầu - ưu đãi lên đến 50%', 'san-pham-ve-tinh-dau-uu-dai-len-den-50'),
(552, 69, 'mã giảm giá lazadacps', 'ma-giam-gia-lazadacps'),
(553, 69, 'mã coupon lazadacps', 'ma-coupon-lazadacps'),
(554, 69, 'khuyến mại lazadacps', 'khuyen-mai-lazadacps'),
(555, 69, 'ma giam gia lazadacps', 'ma-giam-gia-lazadacps'),
(556, 69, 'blog giam gia lazadacps', 'blog-giam-gia-lazadacps'),
(557, 69, 'voucher giảm giá lazadacps', 'voucher-giam-gia-lazadacps'),
(558, 69, 'voucher giam gia lazadacps', 'voucher-giam-gia-lazadacps'),
(559, 69, 'ma giam gia lazadacps 2020', 'ma-giam-gia-lazadacps-2020'),
(560, 69, 'mã giảm giá lazadacps 2020', 'ma-giam-gia-lazadacps-2020'),
(561, 69, 'thứ 4 vui vẻ - ngày hội freeship', 'thu-4-vui-ve-ngay-hoi-freeship'),
(562, 70, 'mã giảm giá nguyenkimvn', 'ma-giam-gia-nguyenkimvn'),
(563, 70, 'mã coupon nguyenkimvn', 'ma-coupon-nguyenkimvn'),
(564, 70, 'khuyến mại nguyenkimvn', 'khuyen-mai-nguyenkimvn'),
(565, 70, 'ma giam gia nguyenkimvn', 'ma-giam-gia-nguyenkimvn'),
(566, 70, 'blog giam gia nguyenkimvn', 'blog-giam-gia-nguyenkimvn'),
(567, 70, 'voucher giảm giá nguyenkimvn', 'voucher-giam-gia-nguyenkimvn'),
(568, 70, 'voucher giam gia nguyenkimvn', 'voucher-giam-gia-nguyenkimvn'),
(569, 70, 'ma giam gia nguyenkimvn 2020', 'ma-giam-gia-nguyenkimvn-2020'),
(570, 70, 'mã giảm giá nguyenkimvn 2020', 'ma-giam-gia-nguyenkimvn-2020'),
(571, 70, 'chương trình tết 2020 cực kỳ hấp dẫn', 'chuong-trinh-tet-2020-cuc-ky-hap-dan'),
(572, 71, 'mã giảm giá nguyenkimvn', 'ma-giam-gia-nguyenkimvn'),
(573, 71, 'mã coupon nguyenkimvn', 'ma-coupon-nguyenkimvn'),
(574, 71, 'khuyến mại nguyenkimvn', 'khuyen-mai-nguyenkimvn'),
(575, 71, 'ma giam gia nguyenkimvn', 'ma-giam-gia-nguyenkimvn'),
(576, 71, 'blog giam gia nguyenkimvn', 'blog-giam-gia-nguyenkimvn'),
(577, 71, 'voucher giảm giá nguyenkimvn', 'voucher-giam-gia-nguyenkimvn'),
(578, 71, 'voucher giam gia nguyenkimvn', 'voucher-giam-gia-nguyenkimvn'),
(579, 71, 'ma giam gia nguyenkimvn 2020', 'ma-giam-gia-nguyenkimvn-2020'),
(580, 71, 'mã giảm giá nguyenkimvn 2020', 'ma-giam-gia-nguyenkimvn-2020'),
(581, 71, 'nâng cấp tiện nghi - mừng xuân như ý', 'nang-cap-tien-nghi-mung-xuan-nhu-y'),
(582, 72, 'mã giảm giá nguyenkimvn', 'ma-giam-gia-nguyenkimvn'),
(583, 72, 'mã coupon nguyenkimvn', 'ma-coupon-nguyenkimvn'),
(584, 72, 'khuyến mại nguyenkimvn', 'khuyen-mai-nguyenkimvn'),
(585, 72, 'ma giam gia nguyenkimvn', 'ma-giam-gia-nguyenkimvn'),
(586, 72, 'blog giam gia nguyenkimvn', 'blog-giam-gia-nguyenkimvn'),
(587, 72, 'voucher giảm giá nguyenkimvn', 'voucher-giam-gia-nguyenkimvn'),
(588, 72, 'voucher giam gia nguyenkimvn', 'voucher-giam-gia-nguyenkimvn'),
(589, 72, 'ma giam gia nguyenkimvn 2020', 'ma-giam-gia-nguyenkimvn-2020'),
(590, 72, 'mã giảm giá nguyenkimvn 2020', 'ma-giam-gia-nguyenkimvn-2020'),
(591, 72, 'mua tivi tặng ngay bộ quà hấp dẫn', 'mua-tivi-tang-ngay-bo-qua-hap-dan'),
(592, 73, 'mã giảm giá nguyenkimvn', 'ma-giam-gia-nguyenkimvn'),
(593, 73, 'mã coupon nguyenkimvn', 'ma-coupon-nguyenkimvn'),
(594, 73, 'khuyến mại nguyenkimvn', 'khuyen-mai-nguyenkimvn'),
(595, 73, 'ma giam gia nguyenkimvn', 'ma-giam-gia-nguyenkimvn'),
(596, 73, 'blog giam gia nguyenkimvn', 'blog-giam-gia-nguyenkimvn'),
(597, 73, 'voucher giảm giá nguyenkimvn', 'voucher-giam-gia-nguyenkimvn'),
(598, 73, 'voucher giam gia nguyenkimvn', 'voucher-giam-gia-nguyenkimvn'),
(599, 73, 'ma giam gia nguyenkimvn 2020', 'ma-giam-gia-nguyenkimvn-2020'),
(600, 73, 'mã giảm giá nguyenkimvn 2020', 'ma-giam-gia-nguyenkimvn-2020'),
(601, 73, 'nhà đẹp xịn sang - khang trang đón tết', 'nha-dep-xin-sang-khang-trang-don-tet'),
(602, 74, 'mã giảm giá nguyenkimvn', 'ma-giam-gia-nguyenkimvn'),
(603, 74, 'mã coupon nguyenkimvn', 'ma-coupon-nguyenkimvn'),
(604, 74, 'khuyến mại nguyenkimvn', 'khuyen-mai-nguyenkimvn'),
(605, 74, 'ma giam gia nguyenkimvn', 'ma-giam-gia-nguyenkimvn'),
(606, 74, 'blog giam gia nguyenkimvn', 'blog-giam-gia-nguyenkimvn'),
(607, 74, 'voucher giảm giá nguyenkimvn', 'voucher-giam-gia-nguyenkimvn'),
(608, 74, 'voucher giam gia nguyenkimvn', 'voucher-giam-gia-nguyenkimvn'),
(609, 74, 'ma giam gia nguyenkimvn 2020', 'ma-giam-gia-nguyenkimvn-2020'),
(610, 74, 'mã giảm giá nguyenkimvn 2020', 'ma-giam-gia-nguyenkimvn-2020'),
(611, 74, 'điện thoại samsung - lì xì ngay 100k + galaxy gift đến 5 triệu đồng', 'dien-thoai-samsung-li-xi-ngay-100k-galaxy-gift-den-5-trieu-dong'),
(612, 75, 'mã giảm giá yes24vn', 'ma-giam-gia-yes24vn'),
(613, 75, 'mã coupon yes24vn', 'ma-coupon-yes24vn'),
(614, 75, 'khuyến mại yes24vn', 'khuyen-mai-yes24vn'),
(615, 75, 'ma giam gia yes24vn', 'ma-giam-gia-yes24vn'),
(616, 75, 'blog giam gia yes24vn', 'blog-giam-gia-yes24vn'),
(617, 75, 'voucher giảm giá yes24vn', 'voucher-giam-gia-yes24vn'),
(618, 75, 'voucher giam gia yes24vn', 'voucher-giam-gia-yes24vn'),
(619, 75, 'ma giam gia yes24vn 2020', 'ma-giam-gia-yes24vn-2020'),
(620, 75, 'mã giảm giá yes24vn 2020', 'ma-giam-gia-yes24vn-2020'),
(621, 75, 'giày túi style hàn quốc - giảm đến 30%', 'giay-tui-style-han-quoc-giam-den-30'),
(622, 76, 'mã giảm giá yes24vn', 'ma-giam-gia-yes24vn'),
(623, 76, 'mã coupon yes24vn', 'ma-coupon-yes24vn'),
(624, 76, 'khuyến mại yes24vn', 'khuyen-mai-yes24vn'),
(625, 76, 'ma giam gia yes24vn', 'ma-giam-gia-yes24vn'),
(626, 76, 'blog giam gia yes24vn', 'blog-giam-gia-yes24vn'),
(627, 76, 'voucher giảm giá yes24vn', 'voucher-giam-gia-yes24vn'),
(628, 76, 'voucher giam gia yes24vn', 'voucher-giam-gia-yes24vn'),
(629, 76, 'ma giam gia yes24vn 2020', 'ma-giam-gia-yes24vn-2020'),
(630, 76, 'mã giảm giá yes24vn 2020', 'ma-giam-gia-yes24vn-2020'),
(631, 76, 'nike chính hãng khuyến mãi 50%', 'nike-chinh-hang-khuyen-mai-50'),
(632, 77, 'mã giảm giá yes24vn', 'ma-giam-gia-yes24vn'),
(633, 77, 'mã coupon yes24vn', 'ma-coupon-yes24vn'),
(634, 77, 'khuyến mại yes24vn', 'khuyen-mai-yes24vn'),
(635, 77, 'ma giam gia yes24vn', 'ma-giam-gia-yes24vn'),
(636, 77, 'blog giam gia yes24vn', 'blog-giam-gia-yes24vn'),
(637, 77, 'voucher giảm giá yes24vn', 'voucher-giam-gia-yes24vn'),
(638, 77, 'voucher giam gia yes24vn', 'voucher-giam-gia-yes24vn'),
(639, 77, 'ma giam gia yes24vn 2020', 'ma-giam-gia-yes24vn-2020'),
(640, 77, 'mã giảm giá yes24vn 2020', 'ma-giam-gia-yes24vn-2020'),
(641, 77, 'mừng valentine flash sale 142k', 'mung-valentine-flash-sale-142k'),
(642, 78, 'mã giảm giá yes24vn', 'ma-giam-gia-yes24vn'),
(643, 78, 'mã coupon yes24vn', 'ma-coupon-yes24vn'),
(644, 78, 'khuyến mại yes24vn', 'khuyen-mai-yes24vn'),
(645, 78, 'ma giam gia yes24vn', 'ma-giam-gia-yes24vn'),
(646, 78, 'blog giam gia yes24vn', 'blog-giam-gia-yes24vn'),
(647, 78, 'voucher giảm giá yes24vn', 'voucher-giam-gia-yes24vn'),
(648, 78, 'voucher giam gia yes24vn', 'voucher-giam-gia-yes24vn'),
(649, 78, 'ma giam gia yes24vn 2020', 'ma-giam-gia-yes24vn-2020'),
(650, 78, 'mã giảm giá yes24vn 2020', 'ma-giam-gia-yes24vn-2020'),
(651, 78, 'be my valentine - duy nhất ngày 11/02', 'be-my-valentine-duy-nhat-ngay-1102'),
(652, 79, 'mã giảm giá yes24vn', 'ma-giam-gia-yes24vn'),
(653, 79, 'mã coupon yes24vn', 'ma-coupon-yes24vn'),
(654, 79, 'khuyến mại yes24vn', 'khuyen-mai-yes24vn'),
(655, 79, 'ma giam gia yes24vn', 'ma-giam-gia-yes24vn'),
(656, 79, 'blog giam gia yes24vn', 'blog-giam-gia-yes24vn'),
(657, 79, 'voucher giảm giá yes24vn', 'voucher-giam-gia-yes24vn'),
(658, 79, 'voucher giam gia yes24vn', 'voucher-giam-gia-yes24vn'),
(659, 79, 'ma giam gia yes24vn 2020', 'ma-giam-gia-yes24vn-2020'),
(660, 79, 'mã giảm giá yes24vn 2020', 'ma-giam-gia-yes24vn-2020'),
(661, 79, 'thính sale ngày yêu', 'thinh-sale-ngay-yeu'),
(662, 80, 'mã giảm giá lazadacps', 'ma-giam-gia-lazadacps'),
(663, 80, 'mã coupon lazadacps', 'ma-coupon-lazadacps'),
(664, 80, 'khuyến mại lazadacps', 'khuyen-mai-lazadacps'),
(665, 80, 'ma giam gia lazadacps', 'ma-giam-gia-lazadacps'),
(666, 80, 'blog giam gia lazadacps', 'blog-giam-gia-lazadacps'),
(667, 80, 'voucher giảm giá lazadacps', 'voucher-giam-gia-lazadacps'),
(668, 80, 'voucher giam gia lazadacps', 'voucher-giam-gia-lazadacps'),
(669, 80, 'ma giam gia lazadacps 2020', 'ma-giam-gia-lazadacps-2020'),
(670, 80, 'mã giảm giá lazadacps 2020', 'ma-giam-gia-lazadacps-2020'),
(671, 80, 'nâng độ tình yêu - giảm thêm 15%', 'nang-do-tinh-yeu-giam-them-15'),
(682, 81, 'mã giảm giá klookvn', 'ma-giam-gia-klookvn'),
(683, 81, 'mã coupon klookvn', 'ma-coupon-klookvn'),
(684, 81, 'khuyến mại klookvn', 'khuyen-mai-klookvn'),
(685, 81, 'ma giam gia klookvn', 'ma-giam-gia-klookvn'),
(686, 81, 'blog giam gia klookvn', 'blog-giam-gia-klookvn'),
(687, 81, 'voucher giảm giá klookvn', 'voucher-giam-gia-klookvn'),
(688, 81, 'voucher giam gia klookvn', 'voucher-giam-gia-klookvn'),
(689, 81, 'ma giam gia klookvn 2020', 'ma-giam-gia-klookvn-2020'),
(690, 81, 'mã giảm giá klookvn 2020', 'ma-giam-gia-klookvn-2020'),
(691, 81, 'trải nghiệm kiểu mới - du lịch tuyệt vời', 'trai-nghiem-kieu-moi-du-lich-tuyet-voi');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `fullname` varchar(50) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT 'name@domain.com',
  `password` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `role` varchar(30) DEFAULT 'user',
  `user_type` varchar(30) DEFAULT 'registered',
  `google_id` varchar(255) DEFAULT NULL,
  `facebook_id` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1,
  `about_me` varchar(5000) DEFAULT NULL,
  `last_seen` timestamp NULL DEFAULT NULL,
  `facebook_url` varchar(500) DEFAULT NULL,
  `twitter_url` varchar(500) DEFAULT NULL,
  `instagram_url` varchar(500) DEFAULT NULL,
  `pinterest_url` varchar(500) DEFAULT NULL,
  `linkedin_url` varchar(500) DEFAULT NULL,
  `vk_url` varchar(500) DEFAULT NULL,
  `youtube_url` varchar(500) DEFAULT NULL,
  `show_email_on_profile` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `fullname`, `slug`, `email`, `password`, `token`, `role`, `user_type`, `google_id`, `facebook_id`, `avatar`, `status`, `about_me`, `last_seen`, `facebook_url`, `twitter_url`, `instagram_url`, `pinterest_url`, `linkedin_url`, `vk_url`, `youtube_url`, `show_email_on_profile`, `created_at`) VALUES
(1, 'root', 'Blog giảm giá', 'root', 'dai@devnguyen.com', '$2a$08$YdvxIo1t6yPTyIF2ODqLUus8zuY5uGqYVojSDAYU612Fe5n46gM4y', '5df0911ed1ac99-92295804-17086181', 'admin', 'registered', NULL, NULL, 'uploads/profile/avatar_1_5df27870301c8.jpg', 1, 'Giảm giá hằng ngày là trang web tổng hợp các chương trình giảm giá của Tiki, Shopee, Sendo,... Mã coupon giảm giá Tết cuối năm, giảm giá Tết Canh tý khuyến mại cực khủng', '2020-02-16 06:23:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-12-11 06:47:58'),
(6, 'weand2019', NULL, 'weand2019', 'phongvanland@gmail.com', '$2a$08$2XTNXskjgHeFIvr/vr7NUuk50ZHOYh/LBcEvTVffA4hSBszZeyxIy', '5dfcf53ed8ddb9-42025821-63630905', 'user', 'registered', NULL, NULL, NULL, 0, NULL, '2019-12-20 16:23:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2019-12-20 16:22:22'),
(7, 'KurazhMaf', NULL, 'kurazhmaf', 'lillyvrxfch@onet.pl', '$2a$08$G3qxq9Q9ESY6vdVL2K.d/.rVSi2WYCXvuYKyW3Y09oBsLNKGDaCIm', '5e1bae8d961644-53652981-37781758', 'user', 'registered', NULL, NULL, NULL, 0, NULL, '2020-01-12 23:41:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-01-12 23:41:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ad_spaces`
--
ALTER TABLE `ad_spaces`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `followers`
--
ALTER TABLE `followers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_albums`
--
ALTER TABLE `gallery_albums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_categories`
--
ALTER TABLE `gallery_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_settings`
--
ALTER TABLE `general_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `polls`
--
ALTER TABLE `polls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `poll_votes`
--
ALTER TABLE `poll_votes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_files`
--
ALTER TABLE `post_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_images`
--
ALTER TABLE `post_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reactions`
--
ALTER TABLE `reactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reading_lists`
--
ALTER TABLE `reading_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ad_spaces`
--
ALTER TABLE `ad_spaces`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `followers`
--
ALTER TABLE `followers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gallery_albums`
--
ALTER TABLE `gallery_albums`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `gallery_categories`
--
ALTER TABLE `gallery_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `general_settings`
--
ALTER TABLE `general_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `polls`
--
ALTER TABLE `polls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `poll_votes`
--
ALTER TABLE `poll_votes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `post_files`
--
ALTER TABLE `post_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `post_images`
--
ALTER TABLE `post_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reactions`
--
ALTER TABLE `reactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `reading_lists`
--
ALTER TABLE `reading_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=702;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
