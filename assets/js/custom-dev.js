disableSelection(document.body);

function disableSelection(target) {
    if (typeof target.onselectstart != "undefined")
        target.onselectstart = function() {
            return false
        }
    else if (typeof target.style.MozUserSelect != "undefined")
        target.style.MozUserSelect = "none"
    else
        target.onmousedown = function() {
            return false
        }
    target.style.cursor = "default"
}

$(function () {
    $('.popup-modal').magnificPopup({
        type: 'inline',
        preloader: false,
        modal: true
    });

    $(document).on('click', '.button-link', function (e) {
        $(this).addClass('popup-modal-dismiss');
        $(this).html('<i class="fa fa-times" aria-hidden="true"></i> ĐÓNG LẠI');
    });

    $(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });

    $(document).on('click', '.popup-modal-redirect', function (e) {
        e.preventDefault();
    });
});

function copyCoupon(element) {
    var copyText = document.getElementById(element);
    copyText.select();
    copyText.setSelectionRange(0, 99999);
    document.execCommand("copy");
    $('#alert-copy').text('ĐÃ COPY, CHỈ CẦN DÁN (PASTE) RA LÀ DÙNG');
}