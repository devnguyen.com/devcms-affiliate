<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import_model extends CI_Model {
    
    public function importData($data) {
        $res = $this->db->insert_batch('posts', $data);

        if ($res) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    public function importTags($data) {
        $res = $this->db->insert_batch('tags', $data);

        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
?>