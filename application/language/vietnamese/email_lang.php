<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2017, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['email_must_be_array']         = 'Phương thức xác thực email phải được truyền qua một mảng.';
$lang['email_invalid_address']       = 'Địa chỉ email hợp lệ (ví dụ: name@example.com): %s';
$lang['email_attachment_missing']    = 'Không thể xác định (Vị trí) các tập tin đính kèm: %s';
$lang['email_attachment_unreadable'] = 'Không thể mở tập tin đính kèm: %s';
$lang['email_no_from']               = 'Không thể gửi thư không có header "From".';
$lang['email_no_recipients']         = 'Phải ghi người nhận Email: To, Cc, or Bcc';
$lang['email_send_failure_phpmail']  = 'Không thể gửi EMAIL sử dụng PHP mail(). Server không hỗ trợ phương thức này.';
$lang['email_send_failure_sendmail'] = 'Không thể gửi EMAIL sử dụng PHP Sendmail. Server không hỗ trợ phương thức này.';
$lang['email_send_failure_smtp']     = 'Không thể gửi EMAIL sử dụng PHP SMTP. Server không hỗ trợ phương thức này.';
$lang['email_sent']                  = 'Tin nhắn của bạn đã được gửi thành công sử dụng phương thức sau: %s';
$lang['email_no_socket']             = 'Không thể mở socket để Sendmail. Kiểm tra lại cài đặt.';
$lang['email_no_hostname']           = 'Bạn chưa chỉ định một tên máy chủ SMTP.';
$lang['email_smtp_error']            = 'Phát hiện lỗi SMTP: %s';
$lang['email_no_smtp_unpw']          = 'Lỗi: Bạn phải ghi chính xác tên người dùng và mật khẩu SMTP.';
$lang['email_failed_smtp_login']     = 'Không thể gửi lệnh AUTH LOGIN. Lỗi: %s';
$lang['email_smtp_auth_un']          = 'Xác thực tên người dùng thất bại. Lỗi: %s';
$lang['email_smtp_auth_pw']          = 'Xác thực mật khẩu thất bại. Lỗi: %s';
$lang['email_smtp_data_failure']     = 'Không thể gửi dữ liệu: %s';
$lang['email_exit_status']           = 'Mã trạng thái thoát: %s';
