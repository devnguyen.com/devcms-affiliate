<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Core_Controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Settings
		$global_data['general_settings']	= $this->settings_model->get_general_settings();
		$this->general_settings 			= $global_data['general_settings'];

		// Set timezone
		if (!empty($this->general_settings->timezone)) {
			date_default_timezone_set($this->general_settings->timezone);
		}
		// Lang base url
		$global_data['lang_base_url'] 	= base_url();

		// languages
		$global_data['languages'] 		= $this->language_model->get_active_languages();
		$this->languages = $global_data['languages'];

		// Site lang
		$global_data['site_lang'] = $this->language_model->get_language($this->general_settings->site_lang);
		if (empty($global_data['site_lang'])) {
			$global_data['site_lang'] = $this->language_model->get_language('1');
		}
		$global_data['selected_lang'] = $global_data['site_lang'];

		// Set language
		$lang_segment = $this->uri->segment(1);
		foreach ($global_data['languages'] as $lang) {
			if ($lang_segment == $lang->short_form) {
				if ($this->general_settings->multilingual_system == 1):
					$global_data['selected_lang'] = $lang;
					$global_data['lang_base_url'] = base_url() . $lang->short_form . "/";
				else:
					redirect(base_url());
				endif;
			}
		}

		$this->selected_lang = $global_data['selected_lang'];
		$this->lang_base_url = $global_data['lang_base_url'];

		$this->config->set_item('language', $global_data['selected_lang']->folder_name);
		$this->lang->load("site_lang", $global_data['selected_lang']->folder_name);

		$global_data['rtl'] = false;
		if ($global_data['selected_lang']->text_direction == "rtl") {
			$global_data['rtl'] = true;
		}
		$this->rtl = $global_data['rtl'];

		// Set lang base url
		if ($this->general_settings->site_lang == $global_data['selected_lang']->id) {
			$global_data['lang_base_url'] = base_url();
		} else {
			$global_data['lang_base_url'] = base_url() . $global_data['selected_lang']->short_form . "/";
		}

		$global_data['settings'] = $this->settings_model->get_settings($global_data['selected_lang']->id);
		$this->settings = $global_data['settings'];

		// Selected layout
		$global_data['layout'] = $global_data['general_settings']->layout;
		$this->layout = $global_data['general_settings']->layout;

		// Get site primary font
		$this->config->load('fonts');
		$global_data['primary_font']        = $this->general_settings->primary_font;
		$global_data['primary_font_family'] = $this->config->item($global_data['primary_font'] . '_font_family');
		$global_data['primary_font_url']    = $this->config->item($global_data['primary_font'] . '_font_url');

		// Get site secondary font
		$global_data['secondary_font']        = $this->general_settings->secondary_font;
		$global_data['secondary_font_family'] = $this->config->item($global_data['secondary_font'] . '_font_family');
		$global_data['secondary_font_url']    = $this->config->item($global_data['secondary_font'] . '_font_url');

		// Check auth
		$this->auth_check = auth_check();
		if ($this->auth_check) {
			$this->auth_user = user();
		}

		// Update last seen
		$this->auth_model->update_last_seen();

		$this->username_character_limit = 25;

		$this->load->vars($global_data);
	}

}

class Home_Core_Controller extends Core_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->input->method() == "post") {
			// Set post language
			$lang_folder = $this->input->post('lang_folder', true);
			if (!empty($lang_folder)) {
				$this->config->set_item('language', $lang_folder);
				$this->lang->load("site_lang", $lang_folder);
			}
		}

		// Main menu
		$global_data['main_menu'] = $this->navigation_model->get_menu_links_by_lang($this->selected_lang->id);

		// Popular posts
		$this->popular_posts = get_cached_data('popular_posts');
		if (empty($this->popular_posts)) {
			$this->popular_posts = $this->post_model->get_popular_posts(5);
			set_cache_data('popular_posts', $this->popular_posts);
		}

		// Our picks
		$this->our_picks = get_cached_data('our_picks');
		if (empty($this->our_picks)) {
			$this->our_picks = $this->post_model->get_our_picks(5);
			set_cache_data('our_picks', $this->our_picks);
		}

		// Random posts
		$this->random_posts = get_cached_data('random_posts');
		if (empty($this->random_posts)) {
			$this->random_posts = $this->post_model->get_random_posts(10);
			set_cache_data('random_posts', $this->random_posts);
		}

		// Tags
		$this->tags = get_cached_data('tags');
		if (empty($this->tags)) {
			$this->tags = $this->tag_model->get_random_tags();
			set_cache_data('tags', $this->tags);
		}

		$global_data['categories'] = $this->category_model->get_categories();
		$global_data['polls']      = $this->poll_model->get_polls();

		//recaptcha status
		$global_data['recaptcha_status'] = true;
		if (empty($this->general_settings->recaptcha_site_key) || empty($this->general_settings->recaptcha_secret_key)) {
			$global_data['recaptcha_status'] = false;
		}
		$this->recaptcha_status = $global_data['recaptcha_status'];

		$this->load->vars($global_data);
	}

	// Verify recaptcha
	public function recaptcha_verify_request()
	{
		if (!$this->recaptcha_status) {
			return true;
		}

		$this->load->library('recaptcha');
		$recaptcha = $this->input->post('g-recaptcha-response');
		if (!empty($recaptcha)) {
			$response = $this->recaptcha->verifyResponse($recaptcha);
			if (isset($response['success']) && $response['success'] === true) {
				return true;
			}
		}
		return false;
	}

	public function paginate($url, $total_rows)
	{
		$per_page = $this->general_settings->pagination_per_page;
		
		// Initialize pagination
		$page = clean_number($this->input->get('page'));
		if (empty($page)) {
			$page = 0;
		}
		if ($page != 0) {
			$page = $page - 1;
		}
		$config['num_links']          = 4;
		$config['base_url']           = $url;
		$config['total_rows']         = $total_rows;
		$config['per_page']           = $per_page;
		$config['reuse_query_string'] = true;
		$this->pagination->initialize($config);
		return array('per_page' => $per_page, 'offset' => $page * $per_page, 'current_page' => $page + 1);
	}

}

class Admin_Core_Controller extends Core_Controller
{

	public function __construct()
	{
		parent::__construct();

	}

	public function paginate($url, $total_rows)
	{
		//Initialize pagination
		$page     = $this->security->xss_clean($this->input->get('page'));
		$per_page = $this->input->get('show', true);
		if (empty($page)) {
			$page = 0;
		}

		if ($page != 0) {
			$page = $page - 1;
		}

		if (empty($per_page)) {
			$per_page = 15;
		}

		$config['num_links']          = 4;
		$config['base_url']           = $url;
		$config['total_rows']         = $total_rows;
		$config['per_page']           = $per_page;
		$config['reuse_query_string'] = true;
		$this->pagination->initialize($config);

		return array('per_page' => $per_page, 'offset' => $page * $per_page);
	}
}

