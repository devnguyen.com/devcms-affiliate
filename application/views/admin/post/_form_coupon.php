<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="box">
    <div class="box-header with-border">
        <div class="left">
            <h3 class="box-title"><?php echo trans('coupon'); ?> (<?php echo trans('options'); ?>)</h3>
        </div>
    </div><!-- /.box-header -->

    <?php
        $coupon_code    = '';
        $coupon_date    = '';
        $coupon_note    = '';
        $affiliate_link = '';

        if (isset($post)) {
            $coupon_option  = unserialize($post->coupon_options);
            $coupon_code    = $coupon_option['coupon_code'] ? $coupon_option['coupon_code'] : '';
            $coupon_date    = $coupon_option['coupon_date'] ? $coupon_option['coupon_date'] : '';
            $coupon_note    = $coupon_option['coupon_note'] ? $coupon_option['coupon_note'] : '';
            $affiliate_link = $coupon_option['affiliate_link'] ? $coupon_option['affiliate_link'] : '';
        }
    ?>

    <div class="box-body">
        <div class="form-group">
            <label><?php echo trans("coupon_value"); ?></label>
            <input type="text" name="coupon_code" value="<?php echo $coupon_code; ?>" class="form-control">
        </div>

        <div class="form-group">
            <label><?php echo trans("coupon_expired"); ?></label>
            <input type="text" name="coupon_date" value="<?php echo $coupon_date; ?>" class="form-control datetimepicker">
        </div>

        <div class="form-group">
            <label><?php echo trans("coupon_note"); ?></label>
            <textarea class="form-control text-area" name="coupon_note" class="form-control"><?php echo $coupon_note; ?></textarea>
        </div>

        <div class="form-group">
            <label><?php echo trans("affiliate_link"); ?></label>
            <textarea class="form-control text-area" name="affiliate_link" class="form-control"><?php echo $affiliate_link; ?></textarea>
        </div>

    </div>
</div>
