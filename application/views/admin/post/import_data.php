<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php echo form_open('admin_controller/import_data'); ?>
<div class="row">
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-12 form-header">
				<h1 class="form-title"><?php echo trans('import_data'); ?></h1>
				<a href="<?php echo admin_url(); ?>posts" class="btn btn-success btn-add-new pull-right">
					<i class="fa fa-bars"></i>
					<?php echo trans('posts'); ?>
				</a>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="form-post">
					<div class="form-post-left">
						<!-- Start custom data import block -->
						<div class="box">
							<div class="box-header with-border">
								<div class="left">
									<h3 class="box-title">Thông tin tuỳ chọn (áp dụng chung cho tất cả dữ liệu)</h3>
								</div>
							</div><!-- /.box-header -->

							<div class="box-body">
								<!-- include message block -->
								<?php $this->load->view('admin/includes/_messages'); ?>

								<div class="form-group">
									<label class="control-label"><?php echo trans('keywords'); ?> (<?php echo trans('meta_tag'); ?>)</label>
									<input type="text" class="form-control" name="keywords" placeholder="Các từ khoá ngăn cách bằng dấu phẩy, nếu trống sẽ lấy dữ liệu ở file Excel (Mẹo: nên thêm từ khoá file Excel để SEO tốt nhất)" value="<?php echo old('keywords'); ?>" <?php echo ($rtl == true) ? 'dir="rtl"' : ''; ?>>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
											<label class="control-label"><?php echo trans('tags'); ?></label>
											<input id="tags_1" type="text" name="tags" class="form-control tags"/>
											<small>(<?php echo trans('type_tag'); ?>)</small>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- End custom data import block -->
					</div>

					<div class="form-post-right">
						<div class="row">
							<div class="col-sm-12" style="position: -webkit-sticky; position: sticky; top: 0; z-index: 999;">
								<div class="box">
									<div class="box-header with-border">
										<div class="left">
											<h3 class="box-title"><?php echo trans('publish'); ?></h3>
										</div>
									</div><!-- /.box-header -->

									<div class="box-body">
										<div class="form-group">
											<input type="hidden" name="file_import" value="">
											<input type="submit" name="submit" value="Bắt đầu" class="btn btn-primary pull-left">
											<a href="<?php echo admin_url(); ?>" class="btn btn-danger pull-right">Huỷ bỏ</a>
										</div>
									</div>
								</div>
							</div>

							<div class="col-sm-12">
								<?php $this->load->view('admin/post/_upload_file_box'); ?>
							</div>

							<div class="col-sm-12">
								<div class="box">
									<div class="box-header with-border">
										<div class="left">
											<h3 class="box-title"><?php echo trans('category'); ?></h3>
										</div>
									</div><!-- /.box-header -->

									<div class="box-body">
										<div class="form-group">
											<label><?php echo trans("language"); ?></label>
											<select name="lang_id" class="form-control" onchange="get_top_categories_by_lang(this.value);">
												<?php foreach ($languages as $language): ?>
													<option value="<?php echo $language->id; ?>" <?php echo ($site_lang->id == $language->id) ? 'selected' : ''; ?>><?php echo $language->name; ?></option>
												<?php endforeach; ?>
											</select>
										</div>

										<div class="form-group">
											<label class="control-label"><?php echo trans('category'); ?></label>
											<select id="categories" name="category_id" class="form-control" onchange="get_sub_categories(this.value);" required>
												<option value=""><?php echo trans('select'); ?></option>
												<?php foreach ($top_categories as $item): ?>
													<?php if ($item->id == old('category_id')): ?>
														<option value="<?php echo html_escape($item->id); ?>"
																selected><?php echo html_escape($item->name); ?></option>
													<?php else: ?>
														<option value="<?php echo html_escape($item->id); ?>"><?php echo html_escape($item->name); ?></option>
													<?php endif; ?>
												<?php endforeach; ?>
											</select>
										</div>

										<div class="form-group">
											<label class="control-label"><?php echo trans('subcategory'); ?></label>
											<select id="subcategories" name="subcategory_id" class="form-control">
												<option value="0"><?php echo trans('select'); ?></option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo form_close(); ?>

<?php $this->load->view('admin/file-manager/_load_file_manager', ['load_images' => true, 'load_files' => true]); ?>

