<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Section: main -->
<section id="main">
	<div class="container">
		<div class="row">
			<!-- breadcrumb -->
			<div class="page-breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo lang_base_url(); ?>"> <?php echo html_escape(trans("home")); ?></a></li>
					<?php $category_array = get_category_array($post->category_id);
					if (!empty($category_array['parent_category'])):?>
						<li class="breadcrumb-item">
							<a href="<?php echo generate_category_url(null, $category_array['parent_category']->slug); ?>"><?php echo html_escape($category_array['parent_category']->name); ?></a>
						</li>
					<?php endif; ?>
					<?php if (!empty($category_array['subcategory'])): ?>
						<li class="breadcrumb-item">
							<a href="<?php echo generate_category_url($category_array['parent_category']->slug, $category_array['subcategory']->slug); ?>"><?php echo html_escape($category_array['subcategory']->name); ?></a>
						</li>
					<?php endif; ?>
					<li class="breadcrumb-item active"><?php echo html_escape($post->title); ?></li>
				</ol>
			</div>

			<div class="col-sm-12 col-md-8">
				<div class="content">
					<div class="post-content">
						<div class="post-title">
							<h1 class="title"><?php echo html_escape($post->title); ?></h1>
						</div>
						<div class="post-meta">
							<?php if (!empty($category) && !empty($category->parent_id)):
								$parent = helper_get_category($category->parent_id);
								if (!empty($parent)):?>
									<a href="<?php echo generate_category_url($parent->slug, $category->slug); ?>" class="font-weight-normal">
										<i class="icon-folder"></i>&nbsp;&nbsp;<?php echo html_escape($category->name); ?>
									</a>
								<?php endif; ?>
							<?php else: ?>
								<a href="<?php echo generate_category_url(null, $category->slug); ?>" class="font-weight-normal">
									<i class="icon-folder"></i>&nbsp;&nbsp;<?php echo html_escape($category->name); ?>
								</a>
							<?php endif; ?>
							<span><i class="icon-clock"></i>&nbsp;&nbsp;<?php echo helper_date_format($post->created_at); ?></span>

							<?php if ($general_settings->comment_system == 1) : ?>
								<span><i class="icon-comment"></i>&nbsp;&nbsp;<?php echo helper_get_comment_count($post->id); ?> </span>
							<?php endif; ?>

							<!--Show if enabled-->
							<?php if ($general_settings->show_pageviews == 1) : ?>
								<span><i class="icon-eye"></i>&nbsp;&nbsp;<?php echo $post->hit; ?></span>
							<?php endif; ?>


							<!--Add to Reading List-->
							<?php if (auth_check()) : ?>
								<?php if ($is_reading_list == false) : ?>
									<?php echo form_open('home_controller/add_delete_from_reading_list_post'); ?>
									<input type="hidden" name="post_id" value="<?php echo $post->id; ?>">
									<button type="submit" class="add-to-reading-list pull-right">
										<i class="icon-plus-circle"></i>&nbsp;
										<?php echo html_escape(trans("add_reading_list")); ?>
									</button>
									<?php echo form_close(); ?>
								<?php else: ?>
									<!--Remove from Reading List-->
									<?php echo form_open('home_controller/add_delete_from_reading_list_post'); ?>
									<input type="hidden" name="post_id" value="<?php echo $post->id; ?>">

									<button type="submit" class="delete-from-reading-list  pull-right">
										<i class="icon-negative-circle"></i>&nbsp;
										<?php echo html_escape(trans("delete_reading_list")); ?>
									</button>
									<?php echo form_close(); ?>

								<?php endif; ?>

							<?php else: ?>

								<a href="<?php echo lang_base_url(); ?>login" class="add-to-reading-list pull-right">
									<i class="icon-plus-circle"></i>&nbsp;<?php echo html_escape(trans("add_reading_list")); ?>
								</a>

							<?php endif; ?>
						</div>
						<div class="clearfix"></div>
						<div class="post-coupon">
							<div class="coupon_detail" style="border: 2px dashed #d52e26; width: 100%; padding: 15px; margin-bottom: 20px; margin-top: 0 !important;">
								<div class="coupon_date" style="color: #d52e26; font-weight: 600;">
								<?php
									$id				= $post->id;
									$coupon_date 	= unserialize($post->coupon_options)['coupon_date'];
									$coupon_date 	= DateTime::createFromFormat('d/m/Y', $coupon_date);
									$days_left		= (strtotime($coupon_date->format('Y-m-d')) - strtotime(date('Y-m-d'))) / 86400;
									$days_left		= ($days_left <= 0) ? 'ĐÃ HẾT HẠN' : (($days_left == 1) ? 'HẾT HẠN HÔM NAY' : unserialize($post->coupon_options)['coupon_date'] . ' (CÒN '. $days_left .' NGÀY)');
									$coupon_code	= unserialize($post->coupon_options)['coupon_code'];
									$coupon_note	= unserialize($post->coupon_options)['coupon_note'];
									$affiliate_link	= unserialize($post->coupon_options)['affiliate_link'];
									$coupon_action	= ($coupon_code != '') ? '<a href="#coupon-form-copy-'. $id .'" class="popup-modal get-coupon-button"><i class="fa fa-scissors" aria-hidden="true"></i> '. mb_strtoupper(trans('coupon_get')) .'</a>' : '<a href="#deal-popup-confirm-'. $id .'" class="popup-modal get-coupon-button"><i class="fa fa-external-link" aria-hidden="true"></i> '. mb_strtoupper(trans('deal_get')) .'</a>';
								?>
									<p><?php echo mb_strtoupper(trans('coupon_expired')) . ': <span style="color: #05295F">'. $days_left .'</span> <a href="#condition-modal" class="popup-modal pull-right" style="color: #0874B5;">NỘI DUNG <i class="icon-arrow-down"></i></a>'; ?></p>
									<div id="condition-modal" class="mfp-hide white-popup-block">
										<h4 class="text-center">NỘI DUNG KHUYẾN MẠI</h4>
										<p><?php echo unserialize($post->coupon_options)['coupon_note']; ?></p>
										<p class="text-center"><a class="popup-modal-dismiss" href="#" style="color: #d52e26; font-weight: bold;"><i class="fa fa-times" aria-hidden="true"></i> ĐÓNG LẠI</a></p>
									</div>
								</div>
								<div class="coupon_type">
									<p style="color: #d52e26;"><?php echo trans('category') . ': <a href="'. generate_category_url($post->parent_category_slug, NULL) .'" style="color: #05295F">'. $post->parent_category_name .'</a> '; ?></p>
								</div>
								<div class="kind_of_commodity">
									<p style="color: #d52e26;"><?php echo trans('kind_of_commodity') . ': <a href="'. generate_category_url($post->parent_category_slug, $post->category_slug) .'" style="color: #05295F">'. $post->category_name .'</a> '; ?></p>
								</div>
								<div class="text-center">
									<?php echo $coupon_action; ?>
									<a href="#coupon-form-report-<?php echo $id; ?>" class="popup-modal get-coupon-button" style="background-color: #0874B5 !important;"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> <?php echo mb_strtoupper(trans('error_report')); ?></a>
								</div>
							</div>
						</div>

						<!-- Popup coupon note -->
						<div id="condition-modal-<?php echo $id; ?>" class="mfp-hide white-popup-block">
							<h4 class="text-center">NỘI DUNG KHUYẾN MẠI</h4>
							<p><?php echo $coupon_note; ?></p>
							<p class="text-center"><a class="popup-modal-dismiss" href="#" style="color: #d52e26; font-weight: bold;"><i class="fa fa-times" aria-hidden="true"></i> ĐÓNG LẠI</a></p>
						</div>
						<!-- Form popup copy coupon -->
						<div id="coupon-form-copy-<?php echo $id; ?>" class="white-popup-block mfp-hide">
							<h4 class="text-center">MÃ GIẢM GIÁ</h4>
							<div class="form-inline text-center">
								<p></p>
								<p></p>
								<div class="input-group">
									<input type="text" class="form-control" style="border: 2px dashed #d52e26;" id="coupon_<?php echo $id; ?>" value="<?php echo $coupon_code; ?>">
								</div>
								<button class="get-coupon-button" onclick="copyCoupon('coupon_<?php echo $id; ?>')">COPY MÃ <i class="fa fa-clone" aria-hidden="true"></i></button>
							</div>
							<p></p>
							<p id="alert-copy" class="text-center" style="color: #0874B5;"><b>COPY MÃ</b> VÀ <b>SỬ DỤNG NGAY</b> ĐỂ ĐƯỢC HƯỞNG ƯU ĐÃI</p>
							<p></p>
							<p class="text-center">
								<a class="button-link get-coupon-button" target="_blank" href="<?php echo $affiliate_link; ?>" style="color: #fff;"><i class="fa fa-shopping-cart" aria-hidden="true"></i> SỬ DỤNG NGAY</a>
							</p>
						</div>
						<!-- Deal popup confirm -->
						<div id="deal-popup-confirm-<?php echo $id; ?>" class="white-popup-block mfp-hide">
							<h4 class="text-center">NHẬN ƯU ĐÃI CỰC KHỦNG</h4>
							<div class="form-inline text-center">
								<p id="alert-copy" class="text-center" style="color: #0874B5;">Khuyến mại này chỉ có hiệu lực khi bạn chọn XÁC NHẬN dưới đây:</p><p></p>
							</div>
							<p></p>
							<p class="text-center">
								<a class="button-link get-coupon-button" target="_blank" href="<?php echo $affiliate_link; ?>" style="color: #fff;">XÁC NHẬN ƯU ĐÃI <i class="fa fa-external-link" aria-hidden="true"></i></a>
							</p>
						</div>
						<!-- Form popup report coupon -->
						<div id="coupon-form-report-<?php echo $id; ?>" class="white-popup-block mfp-hide">
							<h5 class="text-center">BÁO LỖI MÃ GIẢM GIÁ NÀY KHÔNG SỬ DỤNG ĐƯỢC</h5>
							<div class="text-center">
								<p></p>
								<input type="text" class="form-control" id="report_name" placeholder="Họ tên bạn" value="">
								<p></p>
								<input type="text" class="form-control" id="report_phone" placeholder="Số điện thoại" value="">
								<p></p>
								<textarea type="text" class="form-control" id="report_messenge" placeholder="Tin nhắn" value=""></textarea>
								<p></p>
								<button class="get-coupon-button">GỬI BÁO LỖI</button>
							</div>
							<p></p>
							<p id="alert-report" class="text-center" style="color: #0874B5;"></p>
							<p></p>
							<p class="text-center"><a class="popup-modal-dismiss" href="#" style="color: #d52e26; font-weight: bold;"><i class="fa fa-times" aria-hidden="true"></i> ĐÓNG LẠI</a></p>
						</div>

						<?php if (!empty($post->summary)): ?>
							<div class="post-summary">
								<h2>
									<?php echo $post->summary; ?>
								</h2>
							</div>
						<?php endif; ?>
						<?php if (!empty($post->video_embed_code)): ?>
							<div class="post-video">
								<div class="embed-responsive embed-responsive-16by9">
									<iframe class="embed-responsive-item" src="<?php echo $post->video_embed_code; ?>" allowfullscreen></iframe>
								</div>
							</div>
						<?php else: ?>
							<div class="post-image">
								<?php if (!empty($additional_images)) :
									$this->load->view("post/_post_details_slider", ["ad_space" => "post_top"]);
								else:
									if (!empty($post->image_big)): ?>
										<img src="<?php echo get_post_image($post, 'big'); ?>" class="img-responsive center-image" alt="<?php echo html_escape($post->title); ?>"/>
									<?php endif; ?>
								<?php endif; ?>
							</div>
						<?php endif; ?>


						<?php $this->load->view("partials/_ad_spaces", ["ad_space" => "post_top"]); ?>

						<div class="post-text text-style">

							<?php echo $post->content; ?>

							<?php if (!empty($post->optional_url)) : ?>
								<br>
								<a href="<?php echo html_escape($post->optional_url); ?>"
								   class="btn btn-md btn-custom margin-bottom15 btn-optional-link"
								   target="_blank"><?php echo html_escape($settings->optional_url_button_name); ?></a>
							<?php endif; ?>
							<?php $files = get_post_files($post->id);
							if (!empty($files)):?>
								<div class="post-files">
									<h2 class="title"><?php echo trans("files"); ?></h2>
									<?php foreach ($files as $file): ?>
										<?php echo form_open('download-file'); ?>
											<input type="hidden" name="id" value="<?php echo $file->id; ?>">
											<div class="file">
												<button type="submit"><i class="icon-file"></i><?php echo $file->file_name; ?></button>
											</div>
										</form>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>
						</div>

						<div class="post-tags">
							<h3 class="tags-title"><?php echo html_escape(trans("tags")); ?></h3>
							<ul class="tag-list">
								<?php foreach ($post_tags as $tag) : ?>
									<li>
										<a href="<?php echo lang_base_url() . 'ma-khuyen-mai/' . html_escape($tag->tag_slug); ?>">
											<?php echo html_escape($tag->tag); ?>
										</a>
									</li>
								<?php endforeach; ?>
							</ul>
						</div>

						<div class="post-share">
							<a href="javascript:void(0)"
							   onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo lang_base_url() . html_escape($post->title_slug); ?>', 'Share This Post', 'width=640,height=450');return false"
							   class="btn-share share facebook">
								<i class="icon-facebook"></i>
								<span class="hidden-sm">Facebook</span>
							</a>

							<a href="javascript:void(0)"
							   onclick="window.open('https://twitter.com/share?url=<?php echo lang_base_url() . html_escape($post->title_slug); ?>&amp;text=<?php echo html_escape($post->title); ?>', 'Share This Post', 'width=640,height=450');return false"
							   class="btn-share share twitter">
								<i class="icon-twitter"></i>
								<span class="hidden-sm">Twitter</span>
							</a>

							<a href="https://api.whatsapp.com/send?text=<?php echo html_escape($post->title); ?> - <?php echo lang_base_url() . html_escape($post->title_slug); ?>" target="_blank"
							   class="btn-share share whatsapp">
								<i class="icon-whatsapp"></i>
								<span class="hidden-sm">Whatsapp</span>
							</a>

							<a href="javascript:void(0)"
							   onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo lang_base_url() . html_escape($post->title_slug); ?>', 'Share This Post', 'width=640,height=450');return false"
							   class="btn-share share linkedin">
								<i class="icon-linkedin"></i>
								<span class="hidden-sm">Linkedin</span>
							</a>

							<a href="javascript:void(0)"
							   onclick="window.open('http://pinterest.com/pin/create/button/?url=<?php echo lang_base_url() . html_escape($post->title_slug); ?>&amp;media=<?php echo base_url() . html_escape($post->image_big); ?>', 'Share This Post', 'width=640,height=450');return false"
							   class="btn-share share pinterest">
								<i class="icon-pinterest"></i>
								<span class="hidden-sm">Pinterest</span>
							</a>

						</div>

						<?php if ($general_settings->emoji_reactions == 1): ?>
							<div class="col-sm-12 col-xs-12">
								<div class="row">
									<div class="reactions noselect">
										<h4 class="title-reactions"><?php echo trans("whats_your_reaction"); ?></h4>
										<div id="reactions_result">
											<?php $this->load->view('partials/_emoji_reactions'); ?>
										</div>
									</div>
								</div>
							</div>
						<?php endif; ?>

						<div class="col-sm-12 col-xs-12">
							<div class="row">
								<div class="bn-bottom-post">
									<?php $this->load->view("partials/_ad_spaces", ["ad_space" => "post_bottom"]); ?>
								</div>
							</div>
						</div>

					</div><!--/post-content-->

					<!--include about author -->
					<?php $this->load->view('post/_post_about_author', ['post_user' => $post_user]); ?>

					<div class="related-posts">
						<div class="related-post-title">
							<h4 class="title"><?php echo html_escape(trans("related_posts")); ?></h4>
						</div>
						<div class="row related-posts-row">
							<ul class="post-list">
								<?php foreach ($related_posts as $item): ?>

									<li class="col-sm-4 col-xs-12 related-posts-col">
										<a href="<?php echo lang_base_url() . html_escape($item->title_slug); ?>">
											<?php $this->load->view("post/_post_image", ['post_item' => $item, 'type' => 'image_slider']); ?>
										</a>
										<h3 class="title">
											<a href="<?php echo lang_base_url() . html_escape($item->title_slug); ?>">
												<?php echo html_escape(character_limiter($item->title, 70, '...')); ?>
											</a>
										</h3>
									</li>

								<?php endforeach; ?>
							</ul>
						</div>
					</div>

					<div class="col-sm-12 col-xs-12">
						<div class="row">
							<div class="comment-section">
								<?php if ($general_settings->comment_system == 1 || $general_settings->facebook_comment == 1): ?>
									<ul class="nav nav-tabs">
										<?php if ($general_settings->comment_system == 1): ?>
											<li class="active"><a data-toggle="tab" href="#comments"><?php echo trans("comments"); ?></a></li>
										<?php endif; ?>
										<?php if ($general_settings->comment_system == 1): ?>
											<li><a data-toggle="tab" href="#facebook_comments"><?php echo trans("facebook_comments"); ?></a></li>
										<?php endif; ?>
									</ul>

									<div class="tab-content">
										<?php if ($general_settings->comment_system == 1): ?>
											<div id="comments" class="tab-pane fade in active">
												<!-- include comments -->
												<?php $this->load->view('post/_make_comment'); ?>
												<div id="comment-result">
													<?php $this->load->view('post/_comments'); ?>
												</div>
											</div>
										<?php endif; ?>
										<?php if ($general_settings->facebook_comment == 1): ?>
											<div class="tab-pane container " id="facebook_comments">

											</div>
										<?php endif; ?>
										<?php if ($general_settings->comment_system == 1): ?>
											<div id="facebook_comments" class="tab-pane <?php echo ($general_settings->comment_system != 1) ? 'active' : 'fade'; ?>">
												<div class="fb-comments" data-href="<?php echo current_url(); ?>" data-width="100%" data-numposts="5"
													 data-colorscheme="light"></div>
											</div>
										<?php endif; ?>
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-12 col-md-4">
				<!--Sidebar-->
				<?php $this->load->view('partials/_sidebar'); ?>
			</div><!--/col-->
		</div>
	</div>
</section>
<!-- /.Section: main -->
<script>
    $(".fb-comments").attr("data-href", window.location.href);
</script>
<?php
if ($general_settings->facebook_comment == 1) {
	echo $general_settings->facebook_comment;
} ?>
