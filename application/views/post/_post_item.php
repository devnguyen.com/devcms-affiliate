<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if ($layout == "layout_2" || $layout == "layout_5"): ?>
	<!--Post list item-->
	<div class="post-item-horizontal">
		<div class="item-image">
			<a href="<?php echo generate_category_url($item->parent_category_slug, $item->category_slug); ?>">
                <span class="label-post-category">
					<?php echo html_escape($item->category_name); ?>
                </span>
			</a>
			<a href="<?php echo generate_post_url($item); ?>">
				<?php $this->load->view("post/_post_image", ['post_item' => $item, 'type' => 'image_slider']); ?>
			</a>
		</div>

		<div class="item-content">
			<h3 class="title">
				<a href="<?php echo generate_post_url($item); ?>">
					<?php echo html_escape(character_limiter($item->title, 55, '...')); ?>
				</a>
			</h3>
			<?php $this->load->view("post/_post_meta", ['item' => $item]); ?>
			<div class="summary" style="border: 2px dashed #d52e26; width: 100%; padding: 10px; margin-bottom: 5px; margin-top: 0 !important;">
				<?php
					$id				= $item->id;
					$coupon_date 	= unserialize($item->coupon_options)['coupon_date'];
					$coupon_date 	= DateTime::createFromFormat('d/m/Y', $coupon_date);
					$days_left		= (strtotime($coupon_date->format('Y-m-d')) - strtotime(date('Y-m-d'))) / 86400;
					$days_left		= ($days_left <= 0) ? 'ĐÃ HẾT HẠN' : (($days_left == 1) ? 'HẾT HẠN HÔM NAY' : unserialize($item->coupon_options)['coupon_date'] . ' (CÒN '. $days_left .' NGÀY)');
					$coupon_code	= unserialize($item->coupon_options)['coupon_code'];
					$coupon_note	= unserialize($item->coupon_options)['coupon_note'];
					$affiliate_link	= unserialize($item->coupon_options)['affiliate_link'];
					$coupon_action	= ($coupon_code != '') ? '<a href="#coupon-form-copy-'. $id .'" class="popup-modal get-coupon-button"><i class="fa fa-scissors" aria-hidden="true"></i> '. mb_strtoupper(trans('coupon_get')) .'</a>' : '<a href="#deal-popup-confirm-'. $id .'" class="popup-modal get-coupon-button"><i class="fa fa-external-link" aria-hidden="true"></i> '. mb_strtoupper(trans('deal_get')) .'</a>';
				?>
				<!-- Popup coupon note -->
				<div id="condition-modal-<?php echo $id; ?>" class="mfp-hide white-popup-block">
					<h4 class="text-center">NỘI DUNG KHUYẾN MẠI</h4>
					<p><?php echo nl2br($coupon_note); ?></p>
					<p class="text-center"><a class="popup-modal-dismiss" href="#" style="color: #d52e26; font-weight: bold;"><i class="fa fa-times" aria-hidden="true"></i> ĐÓNG LẠI</a></p>
				</div>
				<!-- Form popup copy coupon -->
				<div id="coupon-form-copy-<?php echo $id; ?>" class="white-popup-block mfp-hide">
					<h4 class="text-center">MÃ GIẢM GIÁ</h4>
					<div class="form-inline text-center">
						<p></p>
						<p></p>
						<div class="input-group">
							<input type="text" class="form-control coupon_input_code" style="border: 2px dashed #d52e26;" id="coupon_<?php echo $id; ?>" value="<?php echo $coupon_code; ?>">
						</div>
						<button class="get-coupon-button" onclick="copyCoupon('coupon_<?php echo $id; ?>')">COPY MÃ <i class="fa fa-clone" aria-hidden="true"></i></button>
					</div>
					<p></p>
					<p id="alert-copy" class="text-center" style="color: #0874B5;"><b>COPY MÃ</b> VÀ <b>SỬ DỤNG NGAY</b> ĐỂ ĐƯỢC HƯỞNG ƯU ĐÃI</p>
					<p></p>
					<p class="text-center">
						<a class="button-link get-coupon-button" target="_blank" href="<?php echo $affiliate_link; ?>" style="color: #fff;"><i class="fa fa-shopping-cart" aria-hidden="true"></i> SỬ DỤNG NGAY</a>
					</p>
				</div>
				<!-- Deal popup confirm -->
				<div id="deal-popup-confirm-<?php echo $id; ?>" class="white-popup-block mfp-hide">
					<h4 class="text-center">NHẬN ƯU ĐÃI CỰC KHỦNG</h4>
					<div class="form-inline text-center">
						<p id="alert-copy" class="text-center" style="color: #0874B5;">Khuyến mại này chỉ có hiệu lực khi bạn chọn XÁC NHẬN dưới đây:</p><p></p>
					</div>
					<p></p>
					<p class="text-center">
						<a class="button-link get-coupon-button" target="_blank" href="<?php echo $affiliate_link; ?>" style="color: #fff;">XÁC NHẬN ƯU ĐÃI <i class="fa fa-external-link" aria-hidden="true"></i></a>
					</p>
				</div>
				<!-- Form popup report coupon -->
				<div id="coupon-form-report-<?php echo $id; ?>" class="white-popup-block mfp-hide">
					<h5 class="text-center">BÁO LỖI MÃ GIẢM GIÁ NÀY KHÔNG SỬ DỤNG ĐƯỢC</h5>
					<div class="text-center">
						<p></p>
						<input type="text" class="form-control" id="report_name" placeholder="Họ tên bạn" value="">
						<p></p>
						<input type="text" class="form-control" id="report_phone" placeholder="Số điện thoại" value="">
						<p></p>
						<textarea type="text" class="form-control" id="report_messenge" placeholder="Tin nhắn" value=""></textarea>
						<p></p>
						<button class="get-coupon-button">GỬI BÁO LỖI</button>
					</div>
					<p></p>
					<p id="alert-report" class="text-center" style="color: #0874B5;"></p>
					<p></p>
					<p class="text-center"><a class="popup-modal-dismiss" href="#" style="color: #d52e26; font-weight: bold;"><i class="fa fa-times" aria-hidden="true"></i> ĐÓNG LẠI</a></p>
				</div>
			
				<div class="coupon_date" style="color: #d52e26; font-weight: 600;">
					<p><?php echo mb_strtoupper(trans('coupon_expired')) . ': <span style="color: #05295F">'. $days_left .'</span> <a href="#condition-modal-'. $id .'" class="popup-modal pull-right" style="color: #0874B5;">NỘI DUNG <i class="icon-arrow-down"></i></a>'; ?></p>
				</div>
				<div class="coupon_type">
					<p style="color: #d52e26;"><?php echo trans('category') . ': <a href="'. generate_category_url($item->parent_category_slug, NULL) .'" style="color: #05295F">'. $item->parent_category_name .'</a> '; ?></p>
				</div>
				<div class="kind_of_commodity">
					<p style="color: #d52e26;"><?php echo trans('kind_of_commodity') . ': <a href="'. generate_category_url($item->parent_category_slug, $item->category_slug) .'" style="color: #05295F">'. $item->category_name .'</a> '; ?></p>
				</div>
				<div class="text-center">
					<?php echo $coupon_action; ?>
					<a href="#coupon-form-report-<?php echo $id; ?>" class="popup-modal get-coupon-button" style="background-color: #0874B5 !important;"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> <?php echo mb_strtoupper(trans('error_report')); ?></a>
				</div>
			</div>
		</div>
	</div>

<?php elseif ($layout == "layout_3" || $layout == "layout_6"): ?>

	<div class="col-sm-6 col-xs-12 item-boxed-cnt">
		<!--Post list item-->
		<div class="col-xs-12 post-item-boxed p0">
			<div class="item-image">
				<a href="<?php echo generate_category_url($item->parent_category_slug, $item->category_slug); ?>">
                    <span class="label-post-category">
                   	<?php echo html_escape($item->category_name); ?>
                    </span>
				</a>

				<a href="<?php echo generate_post_url($item); ?>">
					<?php $this->load->view("post/_post_image", ['post_item' => $item, 'type' => 'image_slider']); ?>
				</a>
			</div>

			<div class="item-content">
				<h3 class="title">
					<a href="<?php echo generate_post_url($item); ?>">
						<?php echo html_escape(character_limiter($item->title, 50, '...')); ?>
					</a>
				</h3>
				<?php $this->load->view("post/_post_meta", ['item' => $item]); ?>
				<p class="summary">
					<!-- Code mã giảm giá khác ở đây -->
				</p>

				<div class="post-buttons">
					<a href="<?php echo generate_post_url($item); ?>" class="pull-right read-more">
						<?php echo html_escape(trans("readmore")); ?>
						<i class="icon-arrow-right read-more-i" aria-hidden="true"></i>
					</a>
				</div>
			</div>
		</div>
	</div>

<?php else: ?>

	<!--post list item-->
	<div class="col-sm-12 post-item">
		<div class="row">
			<div class="post-image">
				<a href="<?php echo generate_post_url($item); ?>">
					<?php $this->load->view("post/_post_image", ['post_item' => $item, 'type' => 'image_mid']); ?>
				</a>
			</div>

			<div class="post-footer">
				<div class="text-center">
					<!--if related category exists-->
					<p class="default-post-label-category">
						<a href="<?php echo generate_category_url($item->parent_category_slug, $item->category_slug); ?>">
                            <span class="label-post-category">
                          	<?php echo html_escape($item->category_name); ?>
                            </span>
						</a>
					</p>
					<h3 class="title">
						<a href="<?php echo generate_post_url($item); ?>">
							<?php echo html_escape($item->title); ?>
						</a>
					</h3>
					<?php $this->load->view("post/_post_meta", ['item' => $item]); ?>
				</div>
				<p class="summary text-center">
					<?php echo html_escape($item->summary); ?>
				</p>

				<div class="post-buttons">
					<a href="<?php echo generate_post_url($item); ?>" class="pull-right read-more">
						<?php echo html_escape(trans("readmore")); ?>
						<i class="icon-arrow-right read-more-i" aria-hidden="true"></i>
					</a>
				</div>

			</div><!-- /.post footer -->
		</div><!-- /.row -->
	</div>

<?php endif; ?>




